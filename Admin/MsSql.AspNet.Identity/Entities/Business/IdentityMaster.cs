﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manager.DataLayer.Entities
{
    [Serializable]
    public class IdentityMaster : CommonIdentity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        [JsonIgnore]
        public int CreatedBy { get; set; }

        [JsonIgnore]
        public DateTime? CreatedDate { get; set; }

        [JsonIgnore]
        public DateTime? LastUpdated { get; set; }

        [JsonIgnore]
        public int LastUpdatedBy { get; set; }

        public int Status { get; set; }       
    }

    public class IdentityPolicy : IdentityMaster
    {
        //Extends information here
    }

    //public class IdentityUnit : IdentityMaster
    //{
    //    //Extends information here
    //}

    [Serializable]
    public class IdentityPropertyCategory : IdentityMaster
    {
        //Extends information here
        public int[] SelectedProperties { get; set; }
        public List<IdentityProperty> Properties { get; set; }
        public IdentityPropertyCategory()
        {
            Properties = new List<IdentityProperty>();
        }

        public bool HasChildren()
        {
            return (Properties.Count() > 0);
        }
    }

    public class IdentityGroupProperty : IdentityMaster
    {
        //Extends information here
        public string Icon { get; set; }

        public string Description { get; set; }

        public List<IdentityGroupPropertyLang> LangList { get; set; }

        public IdentityGroupProperty()
        {
            LangList = new List<IdentityGroupPropertyLang>();
        }
    }

    public class IdentityGroupPropertyLang
    {
        public int Id { get; set; }
        public string LangCode { get; set; }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class IdentityProperty : IdentityMaster
    {
        public string Icon { get; set; }

        public List<IdentityPropertyLang> LangList { get; set; }

        public int PropertyCategoryId { get; set; }

        public IdentityProperty()
        {
            LangList = new List<IdentityPropertyLang>();
        }
    }

    [Serializable]
    public class IdentityPropertyLang : IdentityMaster
    {
        //Extends information here
        public int PropertyId { get; set; }
        public string LangCode { get; set; }
    }
    
    public class IdentityPriceType : IdentityMaster
    {
        //Extends information here
        public string Icon { get; set; }
        public string Description { get; set; }
        public List<IdentityPriceTypeLang> LangList { get; set; }

        public IdentityPriceType()
        {
            LangList = new List<IdentityPriceTypeLang>();
        }
    }

    public class IdentityPriceTypeLang
    {
        public int Id { get; set; }
        public string LangCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PriceTypeId { get; set; }
    }

    public class IdentityCurrency : IdentityMaster
    {
        //Extends information here
        public string Icon { get; set; }
        public int SortOrder { get; set; }
    }

    public class IdentityUnit : IdentityMaster
    {
        //Extends information here
        public string Icon { get; set; }
        public int SortOrder { get; set; }
    }
    public class IdentityProvider : IdentityMaster
    {
        //Extends information here
        public int SortOrder { get; set; }

        public string Address { get; set; }
        public string Email { get; set; }

        public string Phone { get; set; }
    }
    public class IdentityCredit : IdentityMaster
    {
        //Extends information here
        public string Icon { get; set; }
    }
    public class IdentityPayment : IdentityMaster
    {
        //Extends information here
        public string Icon { get; set; }
    }

    public class IdentityDevice : IdentityMaster
    {
        //Extends information here
    }

    public class IdentityHTDefaultSetting : IdentityMaster
    {
        //Extends information here
        public int EnumValue { get; set; }
        public int MaxLength { get; set; }
        public int StartPosition { get; set; }
        public int NumberOfCharacters { get; set; }
    }

    public class IdentityAllergy : IdentityMaster
    {
        public int Priority { get; set; }
    }

    public class IdentityMaterialCategory : IdentityMaster
    {
        
    }

    public class IdentityMaterialType : IdentityMaster
    {
        public bool IsMixed { get; set; }
    }

    public class IdentityPreservation : IdentityMaster
    {

    }

    public class IdentityNutrition : IdentityMaster
    {

    }

    public class IdentityTaste : IdentityMaster
    {

    }

    public class IdentityTax : IdentityMaster
    {
        public decimal Value { get; set; }
    }

    public class IdentityColor : IdentityMaster
    {
        public string Value { get; set; }
    }

    public class IdentityStudent : IdentityMaster
    {
        public string Name { get; set; }

        public DateTime? DateBirth { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public int Gender { get; set; }

        public int Status { get; set; }

        //Attendance
        public string Comment { get; set; }

        public string Note { get; set; }

        public int Attendance { get; set; }

    }

    public class IdentityCourse : IdentityMaster
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }

        //Info CourseSub
        public List<IdentityCourseSub> CourseSubs { get; set; }
    }

    public class IdentityCourseSub : IdentityMaster
    {
        public int CourseId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }
       
    }

    public class IdentityTeacher : IdentityMaster
    {
        public string Name { get; set; }

        public DateTime? DateBirth { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public int Gender { get; set; }

        public string Color { get; set; }

        public int Status { get; set; }

        public string UserId { get; set; }

    }
    public class IdentityClass : IdentityMaster
    {
        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int CourseId { get; set; }

        public int CourseSubId { get; set; }

        public TimeSpan? OpenTime { get; set; }

        public TimeSpan? CloseTime { get; set; }

        public string Weekdays { get; set; }

        public int Status { get; set; }

        public IdentityCourse Course { get; set; }

        public IdentityCourseSub CourseSub { get; set; }

        public List<IdentityTeacher> ListTeachers { get; set; }

        public List<IdentityClassTeacher> ClassTeachers { get; set; }

        public List<IdentityClassStudent> ClassStudent { get; set; }

        public List<IdentityClassSchedule> ClassSchedule { get; set; }
    }

    public class IdentityClassTeacher : IdentityMaster
    {
        public int ClassId { get; set; }

        public int TeacherId { get; set; }

        public DateTime? StartTeachingDate { get; set; }

        public DateTime? EndTeachingDate { get; set; }

        public string CodeColor { get; set; }

        public int Status { get; set; }

        public IdentityTeacher Teacher { get; set; }
    }

    public class IdentityClassSchedule : IdentityMaster
    {
        public int ClassId { get; set; }

        public int TeacherId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Comment { get; set; }

        public string Note { get; set; }

        public DateTime? TimeApply { get; set; }
    }

    public class IdentityClassStudent : IdentityMaster
    {
        public int ClassId { get; set; }

        public int  StudentId { get; set; }

        public int  TotalNumLession { get; set; }

        public int  Status { get; set; }
    }

    public class IdentityDaysOfWeek : IdentityMaster
    {
        public int Value { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public int Status { get; set; }
    }

    public class IdentityAttendance : IdentityMaster
    {
        public int ClassScheduleId { get; set; }

        public int StudentId { get; set; }

        public string Comment { get; set; }

        public string Note { get; set; }

        public int Status { get; set; }

        public DateTime? StartTime { get; set; } 

        public DateTime? EndTime { get; set; } 
    }
}
