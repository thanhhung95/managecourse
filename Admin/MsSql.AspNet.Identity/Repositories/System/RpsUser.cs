﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Manager.SharedLibs;

namespace Manager.DataLayer.Repositories
{
    public class RpsUser
    {
        private readonly string _connectionString;

        public RpsUser(string connectionString)
        {
            _connectionString = connectionString;
        }

        public RpsUser()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region --- User ----

        public List<IdentityUser> GetByPage(dynamic filter)
        {
            int offset = (filter.page_index - 1) * filter.page_size;
            var parms = new Dictionary<string, object>
            {
                {"@keyword", filter.keyword},
                {"@status", filter.status },
                {"@offset", offset},
                {"@page_size", filter.page_size}
            };

            var sqlCmd = @"User_GetByPage";

            List<IdentityUser> myList = new List<IdentityUser>();

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parms))
                    {
                        while (reader.Read())
                        {
                            //Get common info
                            var entity = ExtractUserItem(reader);

                            myList.Add(entity);
                        }                        
                    }
                }

            }
            catch (Exception ex)
            {
                var strError = "Failed to User_GetByPage. Error: " + ex.Message;
                throw new CustomSQLException(strError);
            }

            return myList;
        }

        public IdentityUser GetById(int id)
        {
            var parms = new Dictionary<string, object>
            {
                {"@id", id},
            };

            var sqlCmd = @"User_GetById";

            IdentityUser info = null;

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parms))
                    {
                        if (reader.Read())
                        {
                            //Get common info
                            info = ExtractUserItem(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = "Failed to GetById. Error: " + ex.Message;
                throw new CustomSQLException(strError);
            }

            return info;
        }

        public IdentityUser GetById(string id)
        {
            var parms = new Dictionary<string, object>
            {
                {"@UserId", id},
            };

            var sqlCmd = @"User_GetById";

            IdentityUser info = null;

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parms))
                    {
                        if (reader.Read())
                        {
                            //Get common info
                            info = ExtractUserItem(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = "Failed to GetById. Error: " + ex.Message;
                throw new CustomSQLException(strError);
            }

            return info;
        }

        public IdentityUser GetDetailById(int id)
        {
            var parms = new Dictionary<string, object>
            {
                {"@Id", id}
            };

            var sqlCmd = @"User_GetDetailById";

            IdentityUser info = null;

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parms))
                    {
                        if (reader.Read())
                        {
                            //Get common info
                            info = ExtractUserItem(reader);
                        }                       
                    }
                }

            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return info;
        }

        private List<IdentityUser> ParsingUserData(IDataReader reader)
        {
            List<IdentityUser> listData = new List<IdentityUser>();
            while (reader.Read())
            {
               // Get common info
                var entity = ExtractUserItem(reader);

                listData.Add(entity);
            }

            return listData;
        }

        private IdentityUser ExtractUserItem(IDataReader reader)
        {
            var user = new IdentityUser();

            //Seperate properties
            user.Id = reader["Id"].ToString();
            user.StaffId = Utils.ConvertToInt32(reader["StaffId"]);
            user.ParentId = Utils.ConvertToInt32(reader["ParentId"]);
            user.Email = reader["Email"].ToString();            
            user.PhoneNumber = reader["PhoneNumber"].ToString();
            user.UserName = reader["UserName"].ToString();
            user.FullName = reader["FullName"].ToString();
            user.PasswordHash = reader["PasswordHash"].ToString();
            user.TwoFactorEnabled = Utils.ConvertToBoolean(reader["TwoFactorEnabled"]);
            user.LockoutEnabled = Utils.ConvertToBoolean(reader["LockoutEnabled"]);
            user.EmailConfirmed = Utils.ConvertToBoolean(reader["EmailConfirmed"]);
            user.PhoneNumberConfirmed = Utils.ConvertToBoolean(reader["PhoneNumberConfirmed"]);
            user.LockoutEndDateUtc = reader["LockoutEndDateUtc"] == DBNull.Value ? null : (DateTime?)reader["LockoutEndDateUtc"];

            if (reader.HasColumn("TotalCount"))
                user.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

            return user;
        }

        private IdentityStatisticsUserByYear ParsingStatisticsByYear(IDataReader reader)
        {
            //Get common information
            var record = new IdentityStatisticsUserByYear();
            record.year = Utils.ConvertToInt32(reader["year"]);
            record.month_1 = Utils.ConvertToInt32(reader["month_1"]);
            record.month_2 = Utils.ConvertToInt32(reader["month_2"]);
            record.month_3 = Utils.ConvertToInt32(reader["month_3"]);
            record.month_4 = Utils.ConvertToInt32(reader["month_4"]);
            record.month_5 = Utils.ConvertToInt32(reader["month_5"]);
            record.month_6 = Utils.ConvertToInt32(reader["month_6"]);
            record.month_7 = Utils.ConvertToInt32(reader["month_7"]);
            record.month_8 = Utils.ConvertToInt32(reader["month_8"]);
            record.month_9 = Utils.ConvertToInt32(reader["month_9"]);
            record.month_10 = Utils.ConvertToInt32(reader["month_10"]);
            record.month_11 = Utils.ConvertToInt32(reader["month_11"]);
            record.month_12 = Utils.ConvertToInt32(reader["month_12"]);
            return record;
        }

        public IdentityStatisticsUserByYear ActivedUserByYear(dynamic filter)
        {
            //Common syntax
            var conn = new SqlConnection(_connectionString);
            var sqlCmd = @"Statistics_ActivedUserByYear";

            IdentityStatisticsUserByYear data = new IdentityStatisticsUserByYear();

            //For parms
            var parms = new Dictionary<string, object>
            {
                {"@agency_id", filter.agency_id },
                {"@year", filter.year }
            };

            try
            {
                using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parms))
                {
                    if (reader.Read())
                    {
                        data = ParsingStatisticsByYear(reader);
                    }
                    if (reader.NextResult())
                    {
                        if (reader.Read())
                        {
                            if (data != null)
                            {
                                data.AgencyRegisteredDate = reader["CreatedDateUtc"] == DBNull.Value ? null : (DateTime?)reader["CreatedDateUtc"];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return data;
        }

        public int Update(IdentityUser info)
        {
            var returnId = 0;
            var parms = new Dictionary<string, object>
            {
                {"@Id", info.Id},
                {"@Email", info.Email},
                {"@UserName", info.UserName},
                {"@FullName", info.FullName},
                {"@LockoutEnabled", info.LockoutEnabled},
                {"@LockoutEndDateUtc", info.LockoutEndDateUtc},
                {"@Status", info.Status}
            };

            var sqlCmd = @"User_Update";

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parms);
                    returnId = Utils.ConvertToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = "Failed to GetById. Error: " + ex.Message;
                throw new CustomSQLException(strError);
            }

            return returnId;
        }

        public bool UpdateAvatar(IdentityUser identity)
        {
            //Common syntax
            var conn = new SqlConnection(_connectionString);
            var sqlCmd = @"User_UpdateAvatar";

            IdentityStatisticsUserByYear data = new IdentityStatisticsUserByYear();

            //For parms
            var parms = new Dictionary<string, object>
            {
                {"@StaffId", identity.StaffId },
                {"@Avatar", identity.Avatar }
            };

            try
            {
                MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parms);
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public bool AddToRoles(string userId, List<IdentityRole> roles)
        {         
            var sqlCmd = @"User_AddToRole";
            var sqlDelCmd = @"User_DeleteRoles";

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    if (roles.HasData())
                    {                        
                        var delParms = new Dictionary<string, object>
                        {
                            {"@UserId", userId}
                        };

                        //Delete old roles
                        MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlDelCmd, delParms);

                        foreach (var item in roles)
                        {
                            var p = new Dictionary<string, object>
                            {
                                {"@UserId", userId},
                                {"@RoleId", item.Id},
                            };

                            //Add role
                            MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, p);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = "Failed to User_AddToRole. Error: " + ex.Message;
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public List<IdentityUser> GetUsersByPermission(int agencyId, string actionName, string accessName)
        {          
            var parms = new Dictionary<string, object>
            {
                {"@AgencyId", agencyId},
                {"@ActionName", actionName},
                {"@AccessName", accessName}
            };

            var sqlCmd = @"User_GetListByPermission";

            List<IdentityUser> myList = new List<IdentityUser>();

            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parms))
                    {
                        //All parents
                        while (reader.Read())
                        {
                            var r = new IdentityUser();
                            r.Id = reader["Id"].ToString();
                            r.StaffId = Utils.ConvertToInt32(reader["StaffId"]);
                            r.ParentId = Utils.ConvertToInt32(reader["ParentId"]);

                            myList.Add(r);
                        }

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                var r = new IdentityUser();
                                r.Id = reader["Id"].ToString();
                                r.StaffId = Utils.ConvertToInt32(reader["StaffId"]);
                                r.ParentId = Utils.ConvertToInt32(reader["ParentId"]);

                                myList.Add(r);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return myList;
        }

        #endregion
    }
}
