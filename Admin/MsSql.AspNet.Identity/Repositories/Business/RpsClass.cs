﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsClass
    {
        private readonly string _conStr;

        public RpsClass(string conStr)
        {
            _conStr = conStr;
        }

        public RpsClass()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public List<IdentityClass> GetByPage(IdentityClass filter, int currentPage, int pageSize)
        {
            //Common syntax           
            var sqlCmd = @"Class_GetByPage";
            List<IdentityClass> listData = null;

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Status", filter.Status },
                {"@Offset", offset},
                {"@PageSize", pageSize},
            };
            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityClass>();
                        while (reader.Read())
                        {
                            var info = new IdentityClass();

                            info.Id = Utils.ConvertToInt32(reader["Id"]);
                            info.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public int Insert(IdentityClass identity)
        {
            //Common syntax           
            var sqlCmd = @"Class_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Name", identity.Name},
                {"@StartDate", identity.StartDate},
                {"@EndDate", identity.EndDate },
                {"@CourseId", identity.CourseId},
                {"@CourseSubId", identity.CourseSubId},
                {"@OpenTime", identity.OpenTime},
                {"@Weekdays", identity.Weekdays},
                {"@CloseTime", identity.CloseTime},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityClass identity)
        {
            //Common syntax
            var sqlCmd = @"Class_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", identity.Id},
                {"@Name", identity.Name},
                {"@StartDate", identity.StartDate},
                {"@EndDate", identity.EndDate },
                {"@CourseId", identity.CourseId},
                {"@CourseSubId", identity.CourseSubId},
                {"@OpenTime", identity.OpenTime},
                {"@CloseTime", identity.CloseTime},
                {"@Weekdays", identity.Weekdays},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public IdentityClass GetById(int Id)
        {
            var info = new IdentityClass();
            var sqlCmd = @"Class_GetById";

            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        public bool Delete(int Id)
        {
            //Common syntax            
            var sqlCmd = @"Class_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        private IdentityClass ExtractData(IDataReader reader)
        {
            var record = new IdentityClass();

            record = ExtractClassData(reader);

            if (record != null && reader.NextResult())
            {
                record.ClassTeachers = new List<IdentityClassTeacher>();
                while (reader.Read())
                {
                    var infoClassTeacher = ExtractClassTeacherData(reader);

                    if (infoClassTeacher != null)
                    {
                        record.ClassTeachers.Add(infoClassTeacher);
                    }
                }
            }

            if (record != null && reader.NextResult())
            {
                record.ClassStudent = new List<IdentityClassStudent>();
                while (reader.Read())
                {
                    var infoClassStudent = ExtractClassStudentData(reader);

                    if (infoClassStudent != null)
                    {
                        record.ClassStudent.Add(infoClassStudent);
                    }
                }
            }

            if (record != null && reader.NextResult())
            {
                record.ClassSchedule = new List<IdentityClassSchedule>();
                while (reader.Read())
                {
                    var infoClassSchedule = ExtractClassScheduleData(reader);

                    if (infoClassSchedule != null)
                    {
                        record.ClassSchedule.Add(infoClassSchedule);
                    }

                }
            }
            return record;
        }

        private IdentityClass ExtractClassData(IDataReader reader)
        {
            var record = new IdentityClass();

            //Seperate properties
            record.Id           = Utils.ConvertToInt32(reader["Id"]);
            record.Name         = reader["Name"].ToString();
            record.StartDate    = reader["StartDate"] == DBNull.Value ? null : (DateTime?)reader["StartDate"];
            record.EndDate      = reader["EndDate"] == DBNull.Value ? null : (DateTime?)reader["EndDate"];
            record.CourseId     = Utils.ConvertToInt32(reader["CourseId"]);
            record.CourseSubId  = Utils.ConvertToInt32(reader["CourseSubId"]);
            record.OpenTime     = reader["OpenTime"] == DBNull.Value ? null : (TimeSpan?)reader["OpenTime"];
            record.CloseTime    = reader["CloseTime"] == DBNull.Value ? null : (TimeSpan?)reader["CloseTime"];
            record.Weekdays     = reader["Weekdays"].ToString();
            record.Status       = Utils.ConvertToInt32(reader["Status"]);

            return record;
        }

        private IdentityClassTeacher ExtractClassTeacherData(IDataReader reader)
        {
            var record = new IdentityClassTeacher();

            // Seperate properties
            record.Id           = Utils.ConvertToInt32(reader["Id"]);
            record.TeacherId    = Utils.ConvertToInt32(reader["TeacherId"]);

            return record;
        }

        private IdentityClassStudent ExtractClassStudentData(IDataReader reader)
        {
            var record = new IdentityClassStudent();

            // Seperate properties
            record.Id           = Utils.ConvertToInt32(reader["Id"]);
            record.StudentId    = Utils.ConvertToInt32(reader["StudentId"]);

            return record;
        }

        private IdentityClassSchedule ExtractClassScheduleData(IDataReader reader)
        {
            var record = new IdentityClassSchedule();

            //Seperate properties
            record.Id           = Utils.ConvertToInt32(reader["Id"]);
            return record;
        }
        #endregion
    }
}
