﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsClassSchedule
    {
        private readonly string _conStr;

        public RpsClassSchedule(string conStr)
        {
            _conStr = conStr;
        }

        public RpsClassSchedule()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public int Insert(IdentityClassSchedule identity)
        {
            //Common syntax           
            var sqlCmd = @"ClassSchedule_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassId", identity.ClassId},
                {"@TeacherId", identity.TeacherId},
                {"@StartTime", identity.StartTime },
                {"@EndTime", identity.EndTime},
                {"@Comment", identity.Comment},
                {"@Note", identity.Note},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityClassSchedule identity)
        {
            //Common syntax
            var sqlCmd = @"ClassSchedule_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", identity.Id},
                {"@ClassId", identity.ClassId},
                {"@TeacherId", identity.TeacherId},
                {"@StartTime", identity.StartTime},
                {"@EndTime", identity.EndTime},
                {"@Comment", identity.Comment},
                {"@Note", identity.Note},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public bool Delete(int Id)
        {
            //Common syntax            
            var sqlCmd = @"ClassSchedule_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public bool DeleteByClass(int ClassId)
        {
            //Common syntax            
            var sqlCmd = @"ClassSchedule_DeleteByClass";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassId", ClassId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public List<IdentityClassSchedule> GetListByFilter(IdentityClassSchedule filter)
        {
            //Common syntax            
            var sqlCmd = @"ClassSchedule_GetListByFilter";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@TeacherId", filter.TeacherId },
            };

            List<IdentityClassSchedule> listData = new List<IdentityClassSchedule>();
            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        while (reader.Read())
                        {
                            var info = new IdentityClassSchedule();
                            info.Id = Utils.ConvertToInt32(reader["Id"]);

                            listData.Add(info); ;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public IdentityClassSchedule GetById(int Id)
        {
            var info = new IdentityClassSchedule();
            var sqlCmd = @"ClassSchedule_GetById";

            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        public IdentityClassSchedule DuplicateTeaching(IdentityClassSchedule identity)
        {
            var info = new IdentityClassSchedule();
            var sqlCmd = @"ClassSchedule_DuplicateTeaching";

            var parameters = new Dictionary<string, object>
            {
                {"@TeacherId", identity.TeacherId},
                {"@ClassId", identity.ClassId},
                {"@StartTime", identity.StartTime},
                {"@EndTime", identity.EndTime},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        private IdentityClassSchedule ExtractData(IDataReader reader)
        {
            var record = new IdentityClassSchedule();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.ClassId = Utils.ConvertToInt32(reader["ClassId"]);
            record.TeacherId = Utils.ConvertToInt32(reader["TeacherId"]);
            record.StartTime = reader["StartTime"] == DBNull.Value ? null : (DateTime?)reader["StartTime"];
            record.EndTime = reader["EndTime"] == DBNull.Value ? null : (DateTime?)reader["EndTime"];
            record.Comment = reader["Comment"].ToString();
            record.Note = reader["Note"].ToString();

            return record;
        }

        #endregion
    }
}
