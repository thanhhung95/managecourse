﻿using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsDaysOfWeek
    {
        private readonly string _conStr;

        public RpsDaysOfWeek(string conStr)
        {
            _conStr = conStr;
        }

        public RpsDaysOfWeek()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public List<IdentityDaysOfWeek> GetList()
        {
            var listData = new List<IdentityDaysOfWeek>();
            var sqlCmd = @"DaysOfWeek_GetList";

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, null))
                    {
                        while (reader.Read())
                        {
                            var info = ExtractData(reader);

                            listData.Add(info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return listData;
        }

        private IdentityDaysOfWeek ExtractData(IDataReader reader)
        {
            var record = new IdentityDaysOfWeek();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.Value = Utils.ConvertToInt32(reader["Value"]);
            record.Code = reader["Code"].ToString();
            record.Name = reader["Name"].ToString();
            record.Status = Utils.ConvertToInt32(reader["Status"]);

            return record;
        }
        #endregion
    }
}
