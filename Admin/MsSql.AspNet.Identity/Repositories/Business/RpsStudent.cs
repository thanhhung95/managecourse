﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsStudent
    {
        private readonly string _conStr;

        public RpsStudent(string conStr)
        {
            _conStr = conStr;
        }

        public RpsStudent()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public List<IdentityStudent> GetByPage(IdentityStudent filter, int currentPage, int pageSize)
        {           
            //Common syntax           
            var sqlCmd = @"Student_GetByPage";
            List<IdentityStudent> listData = null;

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Status", filter.Status },
                {"@Offset", offset},
                {"@PageSize", pageSize},
            };

            try
            {
                using(var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityStudent>();
                        while (reader.Read())
                        {
                            var infoStudent = new IdentityStudent();
                            infoStudent.Id = Utils.ConvertToInt32(reader["Id"]);
                            infoStudent.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(infoStudent);
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public int Insert(IdentityStudent identity)
        {
            //Common syntax           
            var sqlCmd = @"Student_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Name", identity.Name},
                {"@DateBirth", identity.DateBirth },
                {"@Address", identity.Address},
                {"@Email", identity.Email},
                {"@Phone", identity.Phone},
                {"@Gender", identity.Gender},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityStudent identity)
        {
            //Common syntax
            var sqlCmd = @"Student_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", identity.Id},
                {"@Name", identity.Name},
                {"@DateBirth", identity.DateBirth },
                {"@Address", identity.Address},
                {"@Email", identity.Email},
                {"@Phone", identity.Phone},
                {"@Gender", identity.Gender},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public IdentityStudent GetById(int Id)
        {
            var info = new IdentityStudent();
            var sqlCmd = @"Student_GetById";

            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        public List<IdentityStudent> GetList()
        {
            //Common syntax            
            var sqlCmd = @"Student_GetList";

            List<IdentityStudent> listData = new List<IdentityStudent>();
            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, null))
                    {

                        while (reader.Read())
                        {
                            var info = ExtractData(reader);

                            listData.Add(info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public bool Delete(int Id)
        {
            //Common syntax            
            var sqlCmd = @"Student_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }
        public List<IdentityStudent> GetListStudentNotInClass(IdentityStudent filter, int currentPage, int pageSize, int classId)
        {
            //Common syntax            
            var sqlCmd = @"Student_GetListStudentNotInClass";
            List<IdentityStudent> listData = new List<IdentityStudent>();

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Offset", offset},
                {"@PageSize", pageSize},
                {"@ClassId", classId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityStudent>();
                        while (reader.Read())
                        {
                            var infoStudent = new IdentityStudent();
                            infoStudent.Id = Utils.ConvertToInt32(reader["Id"]);
                            infoStudent.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(infoStudent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public List<IdentityStudent> GetListStudentInClass(IdentityStudent filter, int currentPage, int pageSize, int classId)
        {
            //Common syntax            
            var sqlCmd = @"Student_GetListStudentInClass";
            List<IdentityStudent> listData = new List<IdentityStudent>();

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Offset", offset},
                {"@PageSize", pageSize},
                {"@ClassId", classId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityStudent>();
                        while (reader.Read())
                        {
                            var infoStudent = new IdentityStudent();
                            infoStudent.Id = Utils.ConvertToInt32(reader["Id"]);
                            infoStudent.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(infoStudent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public List<IdentityStudent> GetListStudentByClassSchedule(IdentityStudent filter, int currentPage, int pageSize, int classId,int classScheduleId)
        {
            //Common syntax            
            var sqlCmd = @"Student_GetListStudentByClassSchedule";
            List<IdentityStudent> listData = new List<IdentityStudent>();

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Offset", offset},
                {"@PageSize", pageSize},
                {"@ClassId", classId},
                {"@ClassScheduleId", classScheduleId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityStudent>();
                        while (reader.Read())
                        {
                            var info = new IdentityStudent();

                            //Seperate properties
                            info.Id = Utils.ConvertToInt32(reader["Id"]);
                            info.Name = reader["Name"].ToString();
                            info.DateBirth = reader["DateBirth"] == DBNull.Value ? null : (DateTime?)reader["DateBirth"];
                            info.Address = reader["Address"].ToString();
                            info.Email = reader["Email"].ToString();
                            info.Phone = reader["Phone"].ToString();
                            info.Gender = Utils.ConvertToInt32(reader["Gender"]);
                            info.Status = Utils.ConvertToInt32(reader["Status"]);

                            //Attendance
                            info.Comment = reader["Comment"].ToString();
                            info.Note = reader["Note"].ToString();
                            info.Attendance = Utils.ConvertToInt32(reader["Attendance"]);
                            info.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        private IdentityStudent ExtractData(IDataReader reader)
        {
            var record = new IdentityStudent();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.Name = reader["Name"].ToString();
            record.DateBirth = reader["DateBirth"] == DBNull.Value ? null : (DateTime?)reader["DateBirth"];
            record.Address = reader["Address"].ToString();
            record.Email = reader["Email"].ToString();
            record.Phone = reader["Phone"].ToString();
            record.Gender = Utils.ConvertToInt32(reader["Gender"]);
            record.Status = Utils.ConvertToInt32(reader["Status"]);

            return record;
        }
        #endregion
    }
}
