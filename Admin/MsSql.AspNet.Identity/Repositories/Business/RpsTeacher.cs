﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsTeacher
    {
        private readonly string _conStr;

        public RpsTeacher(string conStr)
        {
            _conStr = conStr;
        }

        public RpsTeacher()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public List<IdentityTeacher> GetByPage(IdentityTeacher filter, int currentPage, int pageSize)
        {
            //Common syntax           
            var sqlCmd = @"Teacher_GetByPage";
            List<IdentityTeacher> listData = null;

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Status", filter.Status },
                {"@Offset", offset},
                {"@PageSize", pageSize},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityTeacher>();

                        while (reader.Read())
                        {
                            var infoTeacher = new IdentityTeacher();

                            infoTeacher.Id = Utils.ConvertToInt32(reader["Id"]);
                            infoTeacher.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(infoTeacher);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public int Insert(IdentityTeacher identity)
        {
            //Common syntax           
            var sqlCmd = @"Teacher_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Name", identity.Name},
                {"@DateBirth", identity.DateBirth },
                {"@Address", identity.Address},
                {"@Email", identity.Email},
                {"@Phone", identity.Phone},
                {"@Gender", identity.Gender},
                {"@Color", identity.Color},
                {"@Status", identity.Status},
                {"@UserId", identity.UserId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityTeacher identity)
        {
            //Common syntax
            var sqlCmd = @"Teacher_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", identity.Id},
                {"@Name", identity.Name},
                {"@DateBirth", identity.DateBirth },
                {"@Address", identity.Address},
                {"@Email", identity.Email},
                {"@Phone", identity.Phone},
                {"@Gender", identity.Gender},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public IdentityTeacher GetById(int Id)
        {
            var info = new IdentityTeacher();
            var sqlCmd = @"Teacher_GetById";

            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        public bool Delete(int Id)
        {
            //Common syntax            
            var sqlCmd = @"Teacher_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public List<IdentityTeacher> GetList()
        {
            //Common syntax            
            var sqlCmd = @"Teacher_GetList";

            List<IdentityTeacher> listData = new List<IdentityTeacher>();
            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, null))
                    {

                        while (reader.Read())
                        {
                            var info = ExtractData(reader);

                            listData.Add(info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        private IdentityTeacher ExtractData(IDataReader reader)
        {
            var record = new IdentityTeacher();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.Name = reader["Name"].ToString();
            record.DateBirth = reader["DateBirth"] == DBNull.Value ? null : (DateTime?)reader["DateBirth"];
            record.Address = reader["Address"].ToString();
            record.Email = reader["Email"].ToString();
            record.Phone = reader["Phone"].ToString();
            record.Gender = Utils.ConvertToInt32(reader["Gender"]);
            record.Color = reader["Color"].ToString();
            record.Status = Utils.ConvertToInt32(reader["Status"]);
            record.UserId = reader["UserId"].ToString();

            return record;
        }

        #endregion
    }
}
