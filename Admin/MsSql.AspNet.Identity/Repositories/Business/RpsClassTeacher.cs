﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsClassTeacher
    {
        private readonly string _conStr;

        public RpsClassTeacher(string conStr)
        {
            _conStr = conStr;
        }

        public RpsClassTeacher()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public int Insert(IdentityClassTeacher identity)
        {
            //Common syntax           
            var sqlCmd = @"ClassTeacher_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassId", identity.ClassId},
                {"@TeacherId", identity.TeacherId },
                {"@StartTeachingDate", identity.StartTeachingDate },
                {"@EndTeachingDate", identity.EndTeachingDate },
                {"@CodeColor", identity.CodeColor },
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityClassTeacher identity)
        {
            //Common syntax
            var sqlCmd = @"ClassTeacher_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", identity.Id},
                {"@ClassId", identity.ClassId},
                {"@TeacherId", identity.TeacherId },
                {"@StartTeachingDate", identity.StartTeachingDate },
                {"@EndTeachingDate", identity.EndTeachingDate },
                {"@CodeColor", identity.CodeColor },
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public IdentityClassTeacher GetById(int Id)
        {
            var info = new IdentityClassTeacher();
            var sqlCmd = @"ClassTeacher_GetById";

            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        public bool Delete(int Id)
        {
            //Common syntax            
            var sqlCmd = @"ClassTeacher_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        private IdentityClassTeacher ExtractData(IDataReader reader)
        {
            var record = new IdentityClassTeacher();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.ClassId = Utils.ConvertToInt32(reader["ClassId"]);
            record.TeacherId = Utils.ConvertToInt32(reader["TeacherId"]);
            record.StartTeachingDate = reader["StartTeachingDate"] == DBNull.Value ? null : (DateTime?)reader["StartTeachingDate"];
            record.EndTeachingDate = reader["EndTeachingDate"] == DBNull.Value ? null : (DateTime?)reader["EndTeachingDate"];
            record.CodeColor = (reader["CodeColor"]).ToString();
            record.Status = Utils.ConvertToInt32(reader["Status"]);

            return record;
        }
        #endregion
    }
}
