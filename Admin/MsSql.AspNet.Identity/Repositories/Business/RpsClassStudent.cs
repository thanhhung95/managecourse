﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Manager.DataLayer.Repositories
{
    public class RpsClassStudent
    {
        private readonly string _conStr;

        public RpsClassStudent(string conStr)
        {
            _conStr = conStr;
        }

        public RpsClassStudent()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public int Insert(IdentityClassStudent identity)
        {
            //Common syntax           
            var sqlCmd = @"ClassStudent_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassId", identity.ClassId},
                {"@StudentId", identity.StudentId },
                {"@TotalNumLession", identity.TotalNumLession},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public int InsertList(string listId, int classId)
        {
            //Common syntax           
            var sqlCmd = @"ClassStudent_InsertList";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ListId", listId},
                {"@ClassId", classId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Delete(IdentityClassStudent identity)
        {
            //Common syntax            
            var sqlCmd = @"ClassStudent_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassId", identity.ClassId},
                {"@StudentId", identity.StudentId},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public bool DeleteList(string listId, int classId)
        {

            //Common syntax           
            var sqlCmd = @"ClassStudent_DeleteByList";

            try
            {
                //For parameters
                var parameters = new Dictionary<string, object>
                {
                    {"@ListId", listId},
                    {"@ClassId", classId},
                };

                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public List<IdentityClassStudent> GetListClassByStudent(int studentId)
        {

            //Common syntax           
            var sqlCmd = @"ClassStudent_GetListClassByStudent";
            List<IdentityClassStudent> listData = new List<IdentityClassStudent>();

            try
            {
                //For parameters
                var parameters = new Dictionary<string, object>
                {
                    {"@StudentId", studentId}
                };

                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        /*listData = new List<IdentityClassStudent>();*/

                        while (reader.Read())
                        {
                            var info= new IdentityClassStudent();
                            info.Id = Utils.ConvertToInt32(reader["Id"]);
                            info.ClassId = Utils.ConvertToInt32(reader["ClassId"]);
                            info.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(info);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        private IdentityClassStudent ExtractStudentData(IDataReader reader)
        {
            var record = new IdentityClassStudent();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.ClassId = Utils.ConvertToInt32(reader["ClassId"]);
            record.StudentId = Utils.ConvertToInt32(reader["StudentId"]);
            record.TotalNumLession = Utils.ConvertToInt32(reader["TotalNumLession"]);
            record.Status = Utils.ConvertToInt32(reader["Status"]);

            return record;
        }
        #endregion
    }
}
