﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsCourse
    {
        private readonly string _conStr;

        public RpsCourse(string conStr)
        {
            _conStr = conStr;
        }

        public RpsCourse()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public List<IdentityCourse> GetByPage(IdentityCourse filter, int currentPage, int pageSize)
        {
            //Common syntax           
            var sqlCmd = @"Course_GetByPage";
            List<IdentityCourse> listData = null;

            //For paging 
            int offset = (currentPage - 1) * pageSize;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Keyword", filter.Keyword },
                {"@Status", filter.Status },
                {"@Offset", offset},
                {"@PageSize", pageSize},
            };

            try
            {
                using(var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        listData = new List<IdentityCourse>();
                        while (reader.Read())
                        {
                            var infoCourse = new IdentityCourse();
                            infoCourse.Id = Utils.ConvertToInt32(reader["Id"]);
                            infoCourse.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(infoCourse);
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public int Insert(IdentityCourse identity)
        {
            //Common syntax           
            var sqlCmd = @"Course_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Name", identity.Name},
                {"@Description", identity.Description},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityCourse identity)
        {
            //Common syntax
            var sqlCmd = @"Course_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", identity.Id},
                {"@Name", identity.Name},
                {"@Description", identity.Description },
                {"@Status", identity.Status}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }

        public bool Delete(int Id)
        {
            //Common syntax            
            var sqlCmd = @"Course_Delete";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }
 
        public List<IdentityCourse> GetList()
        {
            //Common syntax            
            var sqlCmd = @"Course_GetList";

            List<IdentityCourse> listData = new List<IdentityCourse>();
            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, null))
                    {
                        while (reader.Read())
                        {
                            var info = new IdentityCourse();
                            info.Id = Utils.ConvertToInt32(reader["Id"]);
                            info.Name = reader["Name"].ToString();
                            info.Description = reader["Description"].ToString();
                            info.Status = Utils.ConvertToInt32(reader["Status"]);

                            listData.Add(info);
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return listData;
        }

        public IdentityCourse GetById(int Id)
        {
            var info = new IdentityCourse();
            var sqlCmd = @"Course_GetById";

            var parameters = new Dictionary<string, object>
            {
                {"@Id", Id}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        if (reader.Read())
                        {
                            info = ExtractData(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return info;
        }

        private IdentityCourse ExtractData(IDataReader reader)
        {
            var record = new IdentityCourse();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.Name = reader["Name"].ToString();
            record.Description = reader["Description"].ToString();
            record.Status = Utils.ConvertToInt32(reader["Status"]);

            if (record != null && reader.NextResult())
            {
                record.CourseSubs = new List<IdentityCourseSub>();

                while (reader.Read())
                {
                    var CourseSub = new IdentityCourseSub();

                    CourseSub.Id = Utils.ConvertToInt32(reader["Id"]);

                    if (CourseSub != null)
                    {
                        record.CourseSubs.Add(CourseSub);
                    }
                }
            }

            return record;
        }
        #endregion
    }
}
