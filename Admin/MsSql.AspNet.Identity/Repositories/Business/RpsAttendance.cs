﻿
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Manager.DataLayer.Repositories
{
    public class RpsAttendance
    {
        private readonly string _conStr;

        public RpsAttendance(string conStr)
        {
            _conStr = conStr;
        }

        public RpsAttendance()
        {
            _conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        #region  Common

        public int Insert(IdentityAttendance identity)
        {
            //Common syntax           
            var sqlCmd = @"Attendance_Insert";
            var newId = 0;

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassScheduleId", identity.ClassScheduleId },
                {"@StudentId", identity.StudentId},
                {"@Comment", identity.Comment},
                {"@Note", identity.Note},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    var returnObj = MsSqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCmd, parameters);

                    newId = Convert.ToInt32(returnObj);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return newId;
        }

        public bool Update(IdentityAttendance identity)
        {
            //Common syntax
            var sqlCmd = @"Attendance_Update";

            //For parameters
            var parameters = new Dictionary<string, object>
            {
                {"@ClassScheduleId", identity.ClassScheduleId },
                {"@StudentId", identity.StudentId},
                {"@Comment", identity.Comment},
                {"@Note", identity.Note},
                {"@Status", identity.Status},
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    MsSqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCmd, parameters);
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }

            return true;
        }


        public List<IdentityAttendance> GetListByStudent(int classId, int studentId, int status)
        {
            var listData = new List<IdentityAttendance>();
            var sqlCmd = @"Attendance_GetListByStudent";

            var parameters = new Dictionary<string, object>
            {
                {"@ClassId", classId},
                {"@StudentId", studentId},
                {"@Status", status}
            };

            try
            {
                using (var conn = new SqlConnection(_conStr))
                {
                    using (var reader = MsSqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, sqlCmd, parameters))
                    {
                        while (reader.Read())
                        {
                            var record = ExtractData(reader);

                            //Extends information
                            if (reader.HasColumn("TotalCount"))
                                record.TotalCount = Utils.ConvertToInt32(reader["TotalCount"]);

                            listData.Add(record);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed to execute {0}. Error: {1}", sqlCmd, ex.Message);
                throw new CustomSQLException(strError);
            }
            return listData;
        }

        private IdentityAttendance ExtractData(IDataReader reader)
        {
            var record = new IdentityAttendance();

            //Seperate properties
            record.Id = Utils.ConvertToInt32(reader["Id"]);
            record.ClassScheduleId = Utils.ConvertToInt32(reader["ClassScheduleId"]);
            record.StudentId = Utils.ConvertToInt32(reader["StudentId"]);
            record.Comment = reader["Comment"].ToString();
            record.Note = reader["Note"].ToString();
            record.Status = Utils.ConvertToInt32(reader["Status"]);

            record.StartTime = reader["StartTime"] == DBNull.Value ? null : (DateTime?)reader["StartTime"];
            record.EndTime = reader["EndTime"] == DBNull.Value ? null : (DateTime?)reader["EndTime"];

            return record;
        }

        #endregion
    }
}
