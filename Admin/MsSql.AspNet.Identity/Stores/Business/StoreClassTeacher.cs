﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreClassTeacher
    {
        int Insert(IdentityClassTeacher identity);

        bool Update(IdentityClassTeacher identity);

        bool Delete(int id);

        IdentityClassTeacher GetById(int id);
    }

    public class StoreClassTeacher : IStoreClassTeacher
    {
        private readonly string _connectionString;
        private RpsClassTeacher r;



        public StoreClassTeacher() : this("DefaultConnection")
        {

        }

        public StoreClassTeacher(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsClassTeacher(_connectionString);
        }

        #region  Common

        public int Insert(IdentityClassTeacher identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityClassTeacher identity)
        {
            return r.Update(identity);
        }

        public bool Delete(int id)
        {
            return r.Delete(id);
        }

        public IdentityClassTeacher GetById(int id)
        {
            return r.GetById(id);
        }

        #endregion
    }
}
