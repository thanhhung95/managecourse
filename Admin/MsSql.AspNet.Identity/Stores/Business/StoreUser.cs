﻿using System.Configuration;
using System.Collections.Generic;
using Manager.DataLayer.Repositories;

namespace Manager.DataLayer.Stores
{
    public class StoreUser : IStoreUser
    {
        private readonly string _connectionString;
        private RpsUser m;

        public StoreUser()
            : this("DefaultConnection")
        {

        }

        public StoreUser(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            m = new RpsUser(_connectionString);
        }

        public IdentityUser GetUserById(int userId)
        {
            return m.GetDetailById(userId);
        }

        public List<IdentityUser> GetByPage(dynamic filter)
        {
            return m.GetByPage(filter);
        }

        public IdentityStatisticsUserByYear ActivedUserByYear(dynamic filter)
        {
            return m.ActivedUserByYear(filter);
        }

        public bool UpdateAvatar(IdentityUser identity)
        {
            return m.UpdateAvatar(identity);
        }

        public List<IdentityUser> GetUsersByPermission(int agencyId, string actionName, string accessName)
        {
            return m.GetUsersByPermission(agencyId, actionName, accessName);
        }
    }

    public interface IStoreUser
    {
        IdentityUser GetUserById(int userId);
        List<IdentityUser> GetByPage(dynamic filter);
        IdentityStatisticsUserByYear ActivedUserByYear(dynamic filter);
        bool UpdateAvatar(IdentityUser identity);
        List<IdentityUser> GetUsersByPermission(int agencyId, string actionName, string accessName);
    }
}
