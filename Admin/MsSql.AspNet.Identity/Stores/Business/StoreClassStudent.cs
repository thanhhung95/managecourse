﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreClassStudent
    {
        int Insert(IdentityClassStudent identity);

        int InsertList(string listId, int classId);

        bool Delete(IdentityClassStudent identity);

        bool DeleteList(string listId, int classId);

        List<IdentityClassStudent> GetListClassByStudent(int studentId);

    }

    public class StoreClassStudent : IStoreClassStudent
    {
        private readonly string _connectionString;
        private RpsClassStudent r;



        public StoreClassStudent() : this("DefaultConnection")
        {

        }

        public StoreClassStudent(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsClassStudent(_connectionString);
        }

        #region  Common

        public int Insert(IdentityClassStudent identity)
        {
            return r.Insert(identity);
        }

        public int InsertList(string listId, int classId)
        {
            return r.InsertList(listId, classId);
        }

        public bool Delete(IdentityClassStudent identity)
        {
            return r.Delete(identity);
        }

        public bool DeleteList(string listId, int classId)
        {
            return r.DeleteList(listId, classId);
        }

        public List<IdentityClassStudent> GetListClassByStudent(int studentId)
        {
            return r.GetListClassByStudent(studentId);
        }

        #endregion
    }
}