﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreStudent
    {
        
        int Insert(IdentityStudent identity);

        bool Update(IdentityStudent identity);

        bool Delete(int id);

        List<IdentityStudent> GetByPage(IdentityStudent filter, int currentPage, int pageSize);

        IdentityStudent GetById(int id);

        List<IdentityStudent> GetListStudentNotInClass(IdentityStudent filter, int currentPage, int pageSize, int classId);

        List<IdentityStudent> GetListStudentInClass(IdentityStudent filter, int currentPage, int pageSize, int classId);

        List<IdentityStudent> GetListStudentByClassSchedule(IdentityStudent filter, int currentPage, int pageSize, int classId, int classScheduleId);

        List<IdentityStudent> GetList();
    }

    public class StoreStudent : IStoreStudent
    {
        private readonly string _connectionString;
        private RpsStudent r;



        public StoreStudent() : this("DefaultConnection")
        {

        }

        public StoreStudent(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsStudent(_connectionString);
        }


        #region  Common

        public List<IdentityStudent> GetByPage(IdentityStudent filter, int currentPage, int pageSize)
        {
            return r.GetByPage(filter, currentPage, pageSize);
        }

        public int Insert(IdentityStudent identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityStudent identity)
        {
            return r.Update(identity);
        }

        public IdentityStudent GetById(int Id)
        {
            return r.GetById(Id);
        }

        public bool Delete(int Id)
        {
            return r.Delete(Id);
        }
      
        public List<IdentityStudent> GetListStudentNotInClass(IdentityStudent filter, int currentPage, int pageSize, int classId)
        {
            return r.GetListStudentNotInClass(filter, currentPage, pageSize, classId);
        }

        public List<IdentityStudent> GetListStudentInClass(IdentityStudent filter, int currentPage, int pageSize, int classId)
        {
            return r.GetListStudentInClass(filter, currentPage, pageSize, classId);
        }

        public List<IdentityStudent> GetList()
        {
            return r.GetList();
        }

        public List<IdentityStudent> GetListStudentByClassSchedule(IdentityStudent filter, int currentPage, int pageSize, int classId, int classScheduleId)
        {
            return r.GetListStudentByClassSchedule(filter, currentPage, pageSize, classId, classScheduleId);
        }

        #endregion
    }
}
