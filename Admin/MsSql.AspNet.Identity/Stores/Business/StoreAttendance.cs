﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreAttendance
    {
        
        int Insert(IdentityAttendance identity);

        bool Update(IdentityAttendance identity);

        List<IdentityAttendance> GetListByStudent(int classId, int studentId, int status);
    }

    public class StoreAttendance : IStoreAttendance
    {
        private readonly string _connectionString;
        private RpsAttendance r;

        public StoreAttendance() : this("DefaultConnection")
        {

        }

        public StoreAttendance(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsAttendance(_connectionString);
        }
        
        #region  Common

        public int Insert(IdentityAttendance identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityAttendance identity)
        {
            return r.Update(identity);
        }

        public List<IdentityAttendance> GetListByStudent(int classId, int studentId, int status)
        {
            return r.GetListByStudent(classId, studentId, status);
        }

        #endregion
    }
}
