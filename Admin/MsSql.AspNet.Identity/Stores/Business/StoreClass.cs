﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreClass
    {
        
        int Insert(IdentityClass identity);

        bool Update(IdentityClass identity);

        bool Delete(int id);

        List<IdentityClass> GetByPage(IdentityClass filter, int currentPage, int pageSize);

        IdentityClass GetById(int id);

    }

    public class StoreClass : IStoreClass
    {
        private readonly string _connectionString;
        private RpsClass r;



        public StoreClass() : this("DefaultConnection")
        {

        }

        public StoreClass(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsClass(_connectionString);
        }


        #region  Common

        public List<IdentityClass> GetByPage(IdentityClass filter, int currentPage, int pageSize)
        {
            return r.GetByPage(filter, currentPage, pageSize);
        }

        public int Insert(IdentityClass identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityClass identity)
        {
            return r.Update(identity);
        }

        public IdentityClass GetById(int Id)
        {
            return r.GetById(Id);
        }

        public bool Delete(int Id)
        {
            return r.Delete(Id);
        }

        #endregion
    }
}
