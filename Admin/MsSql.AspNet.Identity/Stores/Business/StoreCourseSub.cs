﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreCourseSub
    {

        int Insert(IdentityCourseSub identity);

        bool Update(IdentityCourseSub identity);

        bool Delete(int id);

        IdentityCourseSub GetById(int id);
    }

    public class StoreCourseSub : IStoreCourseSub
    {
        private readonly string _connectionString;
        private RpsCourseSub r;

        public StoreCourseSub(): this("DefaultConnection")
        {

        }

        public StoreCourseSub(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsCourseSub(_connectionString);
        }

        #region  Common

        public int Insert(IdentityCourseSub identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityCourseSub identity)
        {
            return r.Update(identity);
        }

        public bool Delete(int Id)
        {
            return r.Delete(Id);
        }

        public IdentityCourseSub GetById(int Id)
        {
            return r.GetById(Id);
        }
        #endregion
    }
}
