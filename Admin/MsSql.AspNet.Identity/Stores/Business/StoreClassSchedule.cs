﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreClassSchedule
    {
        int Insert(IdentityClassSchedule identity);

        bool Update(IdentityClassSchedule identity);
        
        bool Delete(int id);

        bool DeleteByClass(int ClassId);

        List<IdentityClassSchedule> GetListByFilter(IdentityClassSchedule filter);

        IdentityClassSchedule GetById(int id);

        IdentityClassSchedule DuplicateTeaching(IdentityClassSchedule identity);
    }

    public class StoreClassSchedule : IStoreClassSchedule
    {
        private readonly string _connectionString;
        private RpsClassSchedule r;



        public StoreClassSchedule() : this("DefaultConnection")
        {

        }

        public StoreClassSchedule(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsClassSchedule(_connectionString);
        }

        #region  Common

        public int Insert(IdentityClassSchedule identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityClassSchedule identity)
        {
            return r.Update(identity);
        }

        public bool Delete(int id)
        {
            return r.Delete(id);
        }

        public List<IdentityClassSchedule> GetListByFilter(IdentityClassSchedule filter)
        {
            return r.GetListByFilter(filter);
        }

        public IdentityClassSchedule GetById(int id)
        {
            return r.GetById(id);
        }

        public bool DeleteByClass(int ClassId)
        {
            return r.DeleteByClass(ClassId);
        }

        public IdentityClassSchedule DuplicateTeaching(IdentityClassSchedule identity)
        {
            return r.DuplicateTeaching(identity);
        }

        #endregion
    }
}
