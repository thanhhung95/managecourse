﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreTeacher
    {
        int Insert(IdentityTeacher identity);

        bool Update(IdentityTeacher identity);

        bool Delete(int id);

        List<IdentityTeacher> GetByPage(IdentityTeacher filter, int currentPage, int pageSize);

        IdentityTeacher GetById(int id);

        List<IdentityTeacher> GetList();
    }

    public class StoreTeacher : IStoreTeacher
    {
        private readonly string _connectionString;
        private RpsTeacher r;



        public StoreTeacher() : this("DefaultConnection")
        {

        }

        public StoreTeacher(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsTeacher(_connectionString);
        }

        #region  Common

        public List<IdentityTeacher> GetByPage(IdentityTeacher filter, int currentPage, int pageSize)
        {
            return r.GetByPage(filter, currentPage, pageSize);
        }

        public int Insert(IdentityTeacher identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityTeacher identity)
        {
            return r.Update(identity);
        }

        public bool Delete(int id)
        {
            return r.Delete(id);
        }
        public IdentityTeacher GetById(int id)
        {
            return r.GetById(id);
        }

        public List<IdentityTeacher> GetList()
        {
            return r.GetList();
        }

        #endregion
    }
}
