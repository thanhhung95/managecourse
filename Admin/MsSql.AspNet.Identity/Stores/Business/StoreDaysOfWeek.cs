﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreDaysOfWeek
    {
        List<IdentityDaysOfWeek> GetList();
    }

    public class StoreDaysOfWeek : IStoreDaysOfWeek
    {
        private readonly string _connectionString;
        private RpsDaysOfWeek r;



        public StoreDaysOfWeek() : this("DefaultConnection")
        {

        }

        public StoreDaysOfWeek(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsDaysOfWeek(_connectionString);
        }

        #region  Common
        public List<IdentityDaysOfWeek> GetList()
        {
            return r.GetList();
        }
        #endregion
    }
}
