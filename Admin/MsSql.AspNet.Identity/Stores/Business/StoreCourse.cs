﻿using System;
using System.Collections.Generic;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Repositories;
using System.Configuration;

namespace Manager.DataLayer.Stores
{
    public interface IStoreCourse
    {
        List<IdentityCourse> GetByPage(IdentityCourse filter, int currentPage, int pageSize);

        int Insert(IdentityCourse identity);

        bool Update(IdentityCourse identity);

        bool Delete(int id);

        List<IdentityCourse> GetList();

        IdentityCourse GetById(int id);
    }

    public class StoreCourse : IStoreCourse
    {
        private readonly string _connectionString;
        private RpsCourse r;

        public StoreCourse(): this("DefaultConnection")
        {

        }

        public StoreCourse(string connectionStringName)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            r = new RpsCourse(_connectionString);
        }

        #region  Common

        public List<IdentityCourse> GetByPage(IdentityCourse filter, int currentPage, int pageSize)
        {
            return r.GetByPage(filter, currentPage, pageSize);
        }

        public int Insert(IdentityCourse identity)
        {
            return r.Insert(identity);
        }

        public bool Update(IdentityCourse identity)
        {
            return r.Update(identity);
        }

        public bool Delete(int Id)
        {
            return r.Delete(Id);
        }
       
        public List<IdentityCourse> GetList()
        {
            return r.GetList();
        }

        public IdentityCourse GetById(int Id)
        {
            return r.GetById(Id);
        }
        #endregion
    }
}
