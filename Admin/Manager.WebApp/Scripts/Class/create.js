﻿
$(document).ready(function () {
    $('.select2').select2();

    //function ajax get CoursSub
    var GetCourseSub = function (id, subId = null) {

        var url = '/Class/GetSubByCourse/' + id;

        if (id != 0) {
            $.aGet(url, {}, function (result) {

                var html = '<option value="">選んでください</option>';

                if (result.success == true && result.data.length > 0) {
                    $.each(result.data, function (i, item) {
                        html += ('<option value="' + item.Id + '">' + item.Name + '</option>');
                    });
                    $("#course-sub").prop('disabled', false);
                    $("#course-sub").html(html);
                }
                else {
                    $("#course-sub").prop('disabled', true);
                    $("#course-sub").html(html);
                }
            }, "json");
        }
    }

    //event on change course
    $('#course').on('change', function () {

        var id = $(this).val();
        GetCourseSub(id);
    });

    //----------------Valid Form------------------
    var dupTeaching = true;

    //element
    var name = $("input[name=Name]");
    var course = $("select[name=CourseId]");
    var teacher = $("select[name=TeacherId]");
    var startDate = $("input[name=StartDate]");
    var endDate = $("input[name=EndDate]");
    var openTime = $("#OpenTime");
    var closeTime = $("#CloseTime");
    var day = $("#Day");

    //check submit form
    $(document).on('submit', "#form-class", function (e) {

        var isValid = true;

        if (dupTeaching == false) {
            isValid = false;
        }

        if (name.val() == "") {
            isValid = false;
            $('.msg-name').removeClass('hidden');
        }
        else {
            $('.msg-name').addClass('hidden');
        }

        if (course.val() == "") {
            isValid = false;
            $('.msg-course').removeClass('hidden');
        }
        else {
            $('.msg-course').addClass('hidden');
        }

        if (teacher.val() == "") {
            isValid = false;
            $('.msg-teacher').removeClass('hidden');
        }
        else {
            $('.msg-teacher').addClass('hidden');
        }

        if (startDate.val() == "") {
            isValid = false;
            $('.msg-startdate').removeClass('hidden');
        }
        else {
            $('.msg-startdate').addClass('hidden');
        }

        if (endDate.val() == "") {
            isValid = false;
            $('.msg-enddate').removeClass('hidden');
        }
        else {
            $('.msg-enddate').addClass('hidden');
        }

        if (openTime.val() == "") {
            isValid = false;
            $('.msg-opentime').removeClass('hidden');
        }
        else {
            $('.msg-opentime').addClass('hidden');
        }

        if (closeTime.val() == "") {
            isValid = false;
            $('.msg-closetime').removeClass('hidden');
        }
        else {
            $('.msg-closetime').addClass('hidden');
        }

        if (day.val() == "") {
            isValid = false;
            $('.msg-day').removeClass('hidden');
        }
        else {
            $('.msg-day').addClass('hidden');
        }

        $(".rpt-item-container").each(function () {
            var el = $(this);
            var teacherId = el.find('.teacher-id').val();
            var stTDate = el.find('.start-teaching-date').val();
            var enTDate = el.find('.end-teaching-date').val();

            if (teacherId == "" || stTDate == "" || enTDate == "") {
                isValid = false;
                $.showWarningMessage('アラート !', "先生名のフィールドが必須です");

                teacherId == "" ? el.find('.select2-selection').addClass('border-danger') : "";
                stTDate == "" ? el.find('.start-teaching-date').addClass('border-danger') : "";
                enTDate == "" ? el.find('.end-teaching-date').addClass('border-danger') : "";
            }
        });

        if (isValid == false) {
            e.preventDefault();
        }
    });

    $(document).on('change', '.start-teaching-date', function () {

        var el = $(this);
        var elParent = el.parents('.rpt-item-container');
        var isIdx = elParent.data('idx');

        var isStTDate = el.val();
        var isEnTDate = elParent.find('.end-teaching-date').val();

        var stDate = startDate.val();
        var enDate = endDate.val();

        //check if startDate and EndDate other null
        if (stDate == "" || enDate == "") {
            dupTeaching = false;

            $.showWarningMessage('アラート !', "クラスの開始時間と終了時間が必須です");

            (stDate == "" ? startDate.addClass('border-danger') : startDate.removeClass('border-danger'));
            (enDate == "" ? endDate.addClass('border-danger') : endDate.removeClass('border-danger'));
        }
        //Check if Start Teaching Date outside startDate and endDate 
        else if (isStTDate < stDate || isStTDate > enDate) {
            dupTeaching = false;

            $.showWarningMessage('アラート !', "先生の勤務期間はクラスの期間以内に設定してください");
            el.addClass('border-danger');
        }
        else {
            //remove border when entered correctly
            startDate.removeClass('border-danger');
            endDate.removeClass('border-danger');
            el.removeClass('border-danger');

            // check if the start date is greater than the end date
            if (isStTDate >= isEnTDate && isEnTDate != "") {
                dupTeaching = false;

                $.showWarningMessage('アラート !', "終了時間は開始時間より後の時刻を設定してください");
                el.addClass('border-danger');
            }
            else {

                if (isIdx == 0) {
                    dupTeaching = true;
                }

                $('.rpt-item-container').each(function () {

                    var elContainer = $(this);
                    var idx = elContainer.data('idx');
                    var stTDate = elContainer.find('.start-teaching-date').val();
                    var enTDate = elContainer.find('.end-teaching-date').val();

                    //get locations other than current location and start teaching date, end teaching date other null
                    if (isIdx != idx && stTDate != "" && enTDate != "") {

                        //check if start teaching date current location outside start teaching date , end teaching date other
                        if (isStTDate >= stTDate && isStTDate <= enTDate) {
                            dupTeaching = false;

                            $.showWarningMessage('アラート !', "先生のスケジュールが重複します");
                            el.addClass('border-danger');
                        }
                        else {
                            dupTeaching = true;
                            el.removeClass('border-danger');
                        }
                    }
                });
            }
        }
    });

    $(document).on('change', '.end-teaching-date', function () {

        var el = $(this);
        var elParent = el.parents('.rpt-item-container');
        var isIdx = elParent.data('idx');

        var isEnTDate = el.val();
        var isStTDate = elParent.find('.start-teaching-date').val();

        var stDate = startDate.val();
        var enDate = endDate.val();

        //check startDate EndDate 
        if (stDate == "" || enDate == "") {
            dupTeaching = false;

            $.showWarningMessage('アラート !', "クラスの開始時間と終了時間が必須です");

            (stDate == "" ? startDate.addClass('border-danger') : startDate.removeClass('border-danger'));
            (enDate == "" ? endDate.addClass('border-danger') : endDate.removeClass('border-danger'));
        }
        //Check if End Teaching Date outside startDate and endDate 
        else if (isEnTDate < stDate || isEnTDate > enDate) {
            dupTeaching = false;

            $.showWarningMessage('アラート !', "先生の勤務期間はクラスの期間以内に設定してください");
            el.addClass('border-danger');

        }
        else {
            //remove border when entered correctly
            startDate.removeClass('border-danger');
            endDate.removeClass('border-danger');
            el.removeClass('border-danger');

            // check if the end date is greater than the end date
            if (isEnTDate <= isStTDate && isStTDate != "") {
                dupTeaching = false;

                $.showWarningMessage('アラート !', "終了日は開始日より後の日を設定してください。");
                el.addClass('border-danger');
            }
            else {
                if (isIdx == 0) {
                    dupTeaching = true;
                }

                $('.rpt-item-container').each(function () {

                    var elContainer = $(this);
                    var idx = elContainer.data('idx');
                    var stTDate = elContainer.find('.start-teaching-date').val();
                    var enTDate = elContainer.find('.end-teaching-date').val();

                    //get locations other than current location and start teaching date, end teaching date other null
                    if (isIdx != idx && stTDate != "" && enTDate != "") {

                        //check if start teaching date current location outside start teaching date , end teaching date other
                        if (isEnTDate >= stTDate && isEnTDate <= enTDate) {
                            dupTeaching = false;

                            $.showWarningMessage('アラート !', "先生のスケジュールが重複します");
                            el.addClass('border-danger');
                        }
                        else {
                            dupTeaching = true;
                            el.removeClass('border-danger');
                        }
                    }

                });
            }
        }
    });

    $(document).on('change', '.teacher-id', function () {
        var isVal = $(this).val();
        var elParent = $(this).parents('.rpt-item-container');

        (isVal != 0 ? elParent.find('.select2-selection').removeClass('border-danger') : "");
    })

    $(name).on('keyup', function () {
        if ($(this).val() != "")
            $('.msg-name').addClass('hidden');
    });

    $(course).on('change', function () {
        if ($(this).val() != "")
            $('.msg-course').addClass('hidden');
    });

    $(teacher).on('change', function () {
        if ($(this).val() != "")
            $('.msg-teacher').addClass('hidden');
    });

    $(startDate).on('change', function () {
        var el = $(this);
        var isVal = el.val();
        var enDateVal = endDate.val();

        if (isVal != "") {
            $('.msg-startdate').addClass('hidden');

            if (enDateVal != "" && isVal > enDateVal) {
                dupTeaching = false;

                $.showWarningMessage('アラート !', "開始日は終了日より前の日を設定してください");
                $(this).addClass('border-danger');
            }
            else {
                dupTeaching = true;
                $(this).removeClass('border-danger');
            }
        }
    });

    $(endDate).on('change', function () {
        var el = $(this);
        var isVal = el.val();
        var stDateVal = startDate.val();

        if (isVal != "") {
            $('.msg-enddate').addClass('hidden');

            if (stDateVal != "" && isVal < stDateVal) {
                dupTeaching = false;

                $.showWarningMessage('アラート !', "開始日は終了日より前の日を設定してください");
                $(this).addClass('border-danger');
            }
            else {
                dupTeaching = true;
                $(this).removeClass('border-danger');
            }
        }
    });

    $(openTime).on('change', function () {
        var el = $(this);
        var isVal = el.val();
        var clTimeVal = closeTime.val();

        if (isVal != "") {
            $('.msg-opentime').addClass('hidden');

            if (clTimeVal != "" && isVal > clTimeVal) {
                dupTeaching = false;

                $.showWarningMessage('アラート !', "開始時間は終了時間より前の時刻を設定してください");
                $(this).addClass('border-danger');
            }
            else {
                dupTeaching = true;
                $(this).removeClass('border-danger');
            }
        }
    });

    $(closeTime).on('change', function () {
        var el = $(this);
        var isVal = el.val();
        var opTimeVal = openTime.val();

        if (isVal != "") {
            $('.msg-closetime').addClass('hidden');

            if (opTimeVal != "" && isVal < opTimeVal) {
                dupTeaching = false;

                $.showWarningMessage('アラート !', "終了時間は開始時間より後の時刻を設定してください");
                $(this).addClass('border-danger');
            }
            else {
                dupTeaching = true;
                $(this).removeClass('border-danger');
            }
        }
    });

    $(day).on('change', function () {
        if ($(this).val() != "")
            $('.msg-day').addClass('hidden');
    });
});