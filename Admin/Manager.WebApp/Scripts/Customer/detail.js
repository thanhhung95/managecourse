﻿////$(function () {
////    var ctn = $("#CustomerProductList");
////    if (!ctn.hasClass("loaded")) {
////        ctn.addClass("loaded");

////        CustomerProductGlobal.GetCustomerProductList(1);
////    }
////});


$("body").on("change", '#selectCategoryId', function () {
    var frm = $("#sellProductSearch").serializeArray();
    CustomerProductGlobal.GetCustomerProductList(1, "", frm);
    /*$('#sellProductSearch').submit();*/
});


var CustomerProductGlobal = {
    CustomerProductPageIdx: 1,
    GetCustomerProductList: function (idx, searchExec = "", frmSearch = {}) {
        var ctn = $("#CustomerProductList");
        var myTable = ctn.find(".dynamic-table");
        CustomerProductGlobal.CustomerProductPageIdx = idx;
        if (searchExec === "") {
            ctn.html("");
            ctn.areaLoading();
        } else {
            myTable.html("");
            myTable.areaLoading();
        }

        var frmData = $("#frmCustomerProductList").serializeArray().concat(frmSearch);
        //var frmData = $("#frmCustomerProductList").serializeArray() + frmSearch;
        frmData.push({ name: 'Page', value: idx });
        frmData.push({ name: 'CustomerId', value: $("#CustomerId").val() });
        frmData.push({ name: 'SearchExec', value: searchExec });
        //frmData.push(frmSearch);

        $.aPost("/Customer/GetCustomerSellProductList", frmData, function (result) {
            if (result) {
                if (searchExec === "Y") {
                    myTable.html(result);
                } else {
                    ctn.html(result);
                }

                changeLocalTimezone();
            }
        }, "html");
    },
    GetAgencyPager: function (idx) {
        CustomerProductGlobal.GetOfferList(idx, "Y");
    }
};

$("body").on("click", ".CustomerProductListTab", function () {
    var ctn = $("#CustomerProductList");
    if (!ctn.hasClass("loaded")) {
        ctn.addClass("loaded");

        CustomerProductGlobal.GetCustomerProductList(1);
    }
});

function loadCustomerProductList() {
    var ctn = $("#CustomerProductList");
    if (ctn.hasClass("loaded")) {
        ctn.removeClass("loaded");
        var frm = $("#sellProductSearch").serializeArray();
        CustomerProductGlobal.GetCustomerProductList(1, "", frm);
        ctn.addClass("loaded");
    }
}

var CustomerStoreGlobal = {
    CustomerStorePageIdx: 1,
    GetCustomerStoreList: function (idx, searchExec = "") {
        var ctn = $("#CustomerStoreList");
        var myTable = ctn.find(".dynamic-table");
        CustomerStoreGlobal.CustomerStorePageIdx = idx;
        if (searchExec === "") {
            ctn.html("");
            ctn.areaLoading();
        } else {
            myTable.html("");
            myTable.areaLoading();
        }

        var frmData = $("#frmCustomerStoreList").serializeArray();
        frmData.push({ name: 'Page', value: idx });
        frmData.push({ name: 'CustomerId', value: $("#CustomerId").val() });
        frmData.push({ name: 'SearchExec', value: searchExec });

        $.aPost("/Customer/GetCustomerStoreList", frmData, function (result) {
            if (result) {
                if (searchExec === "Y") {
                    myTable.html(result);
                } else {
                    ctn.html(result);
                }

                changeLocalTimezone();
            }
        }, "html");
    },
    GetAgencyPager: function (idx) {
        CustomerStoreGlobal.GetOfferList(idx, "Y");
    }
};

$("body").on("click", ".CustomerStoreListTab", function () {
    var ctn = $("#CustomerStoreList");
    if (!ctn.hasClass("loaded")) {
        ctn.addClass("loaded");

        CustomerStoreGlobal.GetCustomerStoreList(1);
    }
});

function loadCustomerStoreList() {
    var ctn = $("#CustomerStoreList");
    if (ctn.hasClass("loaded")) {
        ctn.removeClass("loaded");
        CustomerStoreGlobal.GetCustomerStoreList(1);
        ctn.addClass("loaded");
    }
}

var CustomerOrderGlobal = {
    CustomerOrderPageIdx: 1,
    GetCustomerOrderList: function (idx, searchExec = "") {
        var ctn = $("#CustomerOrderList");
        var myTable = ctn.find(".dynamic-table");
        CustomerOrderGlobal.CustomerAccountPageIdx = idx;
        if (searchExec === "") {
            ctn.html("");
            ctn.areaLoading();
        } else {
            myTable.html("");
            myTable.areaLoading();
        }

        var frmData = $("#frmCustomerOrderList").serializeArray();
        frmData.push({ name: 'Page', value: idx });
        frmData.push({ name: 'CustomerId', value: $("#CustomerId").val() });
        frmData.push({ name: 'SearchExec', value: searchExec });

        $.aPost("/Customer/GetCustomerOrderList", frmData, function (result) {
            if (result) {
                if (searchExec === "Y") {
                    myTable.html(result);
                } else {
                    ctn.html(result);
                }

                changeLocalTimezone();
            }
        }, "html");
    },
    GetAgencyPager: function (idx) {
        CustomerOrderGlobal.GetOfferList(idx, "Y");
    }
};

$("body").on("click", ".CustomerOrderListTab", function () {
    var ctn = $("#CustomerOrderList");
    if (!ctn.hasClass("loaded")) {
        ctn.addClass("loaded");

        CustomerOrderGlobal.GetCustomerOrderList(1, "");
    }
});

function loadCustomerOrderList() {
    var ctn = $("#CustomerOrderList");
    if (ctn.hasClass("loaded")) {
        ctn.removeClass("loaded");
        CustomerOrderGlobal.GetCustomerOrderList(1, "");
        ctn.addClass("loaded");
    }
}

var SellProductGlobal = {
    UpdateActiveStatus: function (productId, customerId, isChecked, sellPrice) {
        var frmData = $("#frmCustomerSellProductList").serializeArray();
        frmData.push({ name: 'ActiveStatus', value: isChecked });
        frmData.push({ name: 'CustomerId', value: customerId });
        frmData.push({ name: 'ProductId', value: productId });
        frmData.push({ name: 'SellPrice', value: sellPrice });
        $.aPost("/Customer/UpdateActiveStatus", frmData, function (result) {
            if (result) {
                //ctn.html(result);
                changeLocalTimezone();
            }
        }, "html");
    },
    UpdateActiveStatusAll: function (customerId, activeAll) {
        var frmData = $("#frmCustomerSellProductList").serializeArray();
        frmData.push({ name: 'CustomerId', value: customerId });
        frmData.push({ name: 'ActiveStatusAll', value: activeAll });
        $.aPost("/Customer/UpdateActiveStatusAll_Post", frmData, function (result) {
            if (result.success) {
                $('#myModal').modal('hide');
                $.showSuccessMessage('変更完了 !', '');
                loadCustomerProductList();
            }
        });
    },
};

$(document).on("click", ".active-checkbox", SpamProtection(function () {
    var el = $(this);
    let isChecked = 0;
    let productId = $(this).data('productid');
    let customerId = $(this).data('customerid');
    let sellPrice = $(this).data('sellprice');
    if (el.is(':checked')) {

        isChecked = 1;
    }
    else {
        isChecked = 0;
    }
    SellProductGlobal.UpdateActiveStatus(productId, customerId, isChecked, sellPrice);
    $.showSuccessMessage('変更完了　!', '');
}, 300)
);

$(document).on("click", ".active-checkbox-all", SpamProtection(function () {
    var el = $(this);
    let isChecked = 0;
    let customerId = $(this).data('customerid');
    if (el.is(':checked')) {

        isChecked = 1;
    }
    else {
        isChecked = 0;
    }
    console.log(isChecked)
    var frmData = $("#frmActiveAll").serializeArray();
    frmData.push({ name: 'CustomerId', value: customerId });
    frmData.push({ name: 'ActiveStatusAll', value: isChecked });
    $.aPost("/Customer/UpdateActiveStatusAll", frmData, function (result) {
        if (result.success) {
            $("#myModalContent").html(result.html);
            $('#myModal').modal('show');
        }
        else {
            $.showErrorMessage(result.message, '')
        }
    }, "json", true);
}, 500)
);

$("body").on("click", ".btn-update-status-all", function () {
    var customerId = $("#active_all_customerid").val();
    var activeStatusAll = $("#active_all_status").val();
    SellProductGlobal.UpdateActiveStatusAll(customerId, activeStatusAll);
});

$("body").on("click", ".btn-dismiss-status-all", function () {
    var chkVal = $("#ActiveStatusAll").val();
    if (chkVal == 0) {
        $(".active-checkbox-all").prop("checked", false);
    }
    if (chkVal == 1) {
        $(".active-checkbox-all").prop("checked", true);
    }
});

$(document).on('click', ".order-prop", function (ev) {
    ev.preventDefault();
    //window.location = $(this).data("detail");

    var link = document.createElement('a');
    link.href = "javascript:;";
    link.setAttribute("data-modal", "");
    link.setAttribute("data-href", $(this).data("detail"));
    document.body.appendChild(link);
    link.click();

    return false;
});

$("body").on("click", "#btn-invoice-export", function () {
    $("#frmOrderCheckboxList").resetForm();
    var frmData = $("#frmOrderCheckboxList").serializeArray();
    frmData.push({ name: 'checkboxes', value: checkBoxArr });
    $.aPost("/Customer/GetListInvoice", frmData, function (result) {
        if (result.success) {
            $("#myModalContent").html(result.html);
            $('#myModal').modal('show');
        }
        else {
            $.showErrorMessage(result.message, '')
        }
    }, "json", true);
});


$("body").on("click", ".btn-edit-nohinsho-generate", function () {
    var orderId = $("#orderId").val();
    var listRow = $(".nohinsho-product-list").find(".row-ord-idx");
    var frmData = $("#frmNohinshoGenerate").serializeArray();
    var idx = 0;
    var isVaild = true;
    var noQuantityRowCount = 0;

    listRow.each(function () {
        var ctn = $(this);
        var maxQuantity = ctn.find(`.prd-maxquantity-${idx}`).val();
        var quantity = ctn.find(`.prd-quantity-${idx}`).val();

        if (parseFloat(quantity) == 0) {
            noQuantityRowCount++;
        }

        if (isNaN(quantity) || (parseFloat(quantity) % 1 != 0)) {
            ctn.find(".validate-quantity-product").removeClass("hidden");
            isVaild = false;
        }
        else {
            if (parseFloat(quantity) < 0 || parseFloat(quantity) > parseFloat(maxQuantity)) {
                ctn.find(".validate-quantity-product").removeClass("hidden");
                isVaild = false;
            }
            if (parseFloat(quantity) >= 0 && parseFloat(quantity) <= parseFloat(maxQuantity)) {
                ctn.find(".validate-quantity-product").addClass("hidden");
            }
        }
        idx++;
    });
    if (noQuantityRowCount == listRow.length) {
        $(".validate-total-quantity-product").removeClass("hidden");
        isVaild = false;
    } else {
        $(".validate-total-quantity-product").addClass("hidden");
    }

    if (isVaild) {
        $("#frmNohinshoGenerate").submit();
    }

});

const checkBoxArr = [];
$("body").on("click", ".chk-order", function () {
    if ($(this).is(':checked')) {
        checkBoxArr.push($(this).val());
    }
    else {
        var x = checkBoxArr.indexOf($(this).val());
        checkBoxArr.splice(x, 1);
    }
});

var CustomerOrderListGlobal = {
    CustomerOrderListPageIdx: 1,
    GetCustomerOrderList: function (idx, searchExec = "", frmSearch = {}) {
        var ctn = $("#CustomerOrderList");
        var myTable = ctn.find(".dynamic-table");
        CustomerOrderListGlobal.CustomerOrderListPageIdx = idx;
        if (searchExec === "") {
            ctn.html("");
            ctn.areaLoading();
        } else {
            myTable.html("");
            myTable.areaLoading();
        }

        var frmData = $("#frmCustomerOrderList").serializeArray().concat(frmSearch);
        //var frmData = $("#frmCustomerProductList").serializeArray() + frmSearch;
        frmData.push({ name: 'Page', value: idx });
        frmData.push({ name: 'CustomerId', value: $("#CustomerId").val() });
        frmData.push({ name: 'SearchExec', value: searchExec });
        //frmData.push(frmSearch);

        $.aPost("/Customer/GetCustomerOrderList", frmData, function (result) {
            if (result) {
                if (searchExec === "Y") {
                    myTable.html(result);
                } else {
                    ctn.html(result);
                }

                changeLocalTimezone();
            }
        }, "html");
    },
    GetAgencyPager: function (idx) {
        CustomerOrderListGlobal.GetOfferList(idx, "Y");
    }
};

if (!isMobile) {
    var columns = [];
    var data = [];
    var tblHeight = $("#DynamicTable").height();
    var maxHeight = 500;
    if (tblHeight < 500) {
        maxHeight = undefined;
    }
    $("#DynamicTable").bootstrapTable('destroy').bootstrapTable({
        height: maxHeight,
        columns: columns,
        data: data,
        toolbar: '.toolbar',
        search: false,
        showColumns: true,
        showToggle: true,
        clickToSelect: true,
        fixedColumns: true,
        fixedNumber: 2,
        fixedRightNumber: 1,
        //stickyHeader: true
    });
}



$("body").on("click", ".CustomerOrderListTab", function () {
    var ctn = $("#CustomerOrderList");
    if (!ctn.hasClass("loaded")) {
        ctn.addClass("loaded");

        CustomerOrderListGlobal.GetCustomerOrderList(1);
    }
});

var CustomerAgencyListGlobal = {
    CustomerAgencyListPageIdx: 1,
    GetCustomerAgencyList: function (idx, searchExec = "", frmSearch = {}) {
        var ctn = $("#CustomerAgencyList");
        var myTable = ctn.find(".dynamic-table");
        CustomerAgencyListGlobal.CustomerAgencyListPageIdx = idx;
        if (searchExec === "") {
            ctn.html("");
            ctn.areaLoading();
        } else {
            myTable.html("");
            myTable.areaLoading();
        }

        var frmData = $("#frmCustomerAgencyList").serializeArray().concat(frmSearch);
        frmData.push({ name: 'Page', value: idx });
        frmData.push({ name: 'CustomerId', value: $("#CustomerId").val() });
        frmData.push({ name: 'SearchExec', value: searchExec });
        //frmData.push(frmSearch);

        $.aPost("/Customer/GetCustomerAgencyList", frmData, function (result) {
            if (result) {
                if (searchExec === "Y") {
                    myTable.html(result);
                } else {
                    ctn.html(result);
                }

                changeLocalTimezone();
            }
        }, "html");
    },
    GetAgencyPager: function (idx) {
        CustomerAgencyListGlobal.GetOfferList(idx, "Y");
    }
};

$("body").on("click", ".CustomerAgencyListTab", function () {
    var ctn = $("#CustomerAgencyList");
    if (!ctn.hasClass("loaded")) {
        ctn.addClass("loaded");

        CustomerAgencyListGlobal.GetCustomerAgencyList(1);
    }
});

function loadCustomerAgencyList() {
    var ctn = $("#CustomerAgencyList");
    if (ctn.hasClass("loaded")) {
        ctn.removeClass("loaded");
        CustomerAgencyListGlobal.GetCustomerAgencyList(1);
        ctn.addClass("loaded");
    }
}


