﻿using Manager.WebApp.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Resources;
using System.Web;

namespace Manager.WebApp.Helpers
{
    public enum EnumDaysOfWeek
    {
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6,
        Saturday = 7,
        Sunday = 8,
    }

    public enum EnumGender
    {
        Famale = 1,
        Male = 2,
        Orther = 3,
    }

    public enum EnumMaterialType
    {
        Food = 1,
        Mix = 2,
        Additives = 3,
        Substance = 4,
        Cost = 5
    }

    public enum EnumMaterialMasterType
    {
        Material = 0,
        PackMaterial = 1
    }

    public enum EnumPackMaterialType
    {
        Bag = 1,
        Spoon = 2,        
        FrontLabelCost = 3,
        BackLabelCost = 4,
    }

    public enum EnumPackMaterialCostType
    {
        ProcessingCost = 1
    }

    public enum EnumProductWarehouseImportTemp
    {
        Kyowa = 0,
        Nippon = 1,        
    }

    public enum EnumMaterialOrderStatus
    {
        [Description("未発注")]
        Pending = 0,

        [Description("発注済")]
        Ordered = 1,

        [Description("納期判明")]
        ReceiptDateUpdated = 2,

        [Description("納期連絡済")]
        RecepitDateAnnounced = 3,

        [Description("Trạng thái active")]
        InvoiceChecked = 4
    }

    public enum EnumWarehouseActivityType
    {
        [LocalizedDescription("LB_GOODS_RECEIPT", typeof(ManagerResource))]
        GoodsReceipt = 1,

        [LocalizedDescription("LB_GOODS_ISSUE", typeof(ManagerResource))]
        GoodsIssue = 2,

        [LocalizedDescription("LB_REFLECT_STOCK_TAKE", typeof(ManagerResource))]
        ReflectStockTake = 3
    }

    public enum EnumStatus
    {
        [LocalizedDescription("LB_ACTIVE", typeof(ManagerResource))]
        Activated = 1,

        [LocalizedDescription("LB_LOCKED", typeof(ManagerResource))]
        Locked = 0
    }

    public enum EnumElementType
    {
        Text = 0,
        Dropdown = 1,
        DropdownGroup=2,
    }

    public enum EnumNotificationUserType
    {
        //FA
        Manager = 0,

        Customer = 1,

        Factory = 2,
    }

    public enum EnumNotificationActionTypeForCustomer
    {
        #region Offer

        NewOfferCreated = 1000,
        OfferUpdated = 1001,
        NewOfferVersionCreated = 1002,

        #endregion
    }

    public enum EnumNotificationActionTypeForManager
    {
        #region Offer

        StockPlaceConfirmed = 2000,
        StockPlaceUpdated = 2001

        #endregion
    }

    public enum EnumNotificationActionTypeForFactory
    {
        #region Factory

        NewFactoryOrderCreated = 3000,
        FactoryOrderUpdated = 3001,
        FactoryOrderBeginManufacturing = 3002,

        #endregion
    }

    public enum EnumNotificationTargetType
    {
        System = 0,

        //FA users
        Manager = 1,

        Customer = 2,

        Factory = 3,

        Offer = 100,

        Order = 101,

        Product = 102,

        FactoryOrder = 301
    }

    public enum EnumSearchStatus
    {
        Search = 1,
        SearchAjax = 2,
    }
    public enum EnumProjectStatus
    {
        [LocalizedDescription("LB_LOCKED", typeof(ManagerResource))]
        Locked = 0,

        [LocalizedDescription("LB_ACTIVE", typeof(ManagerResource))]
        Activated = 1       
    }

    public enum EnumSlideType
    {
        [Description("Tĩnh")]
        Static = 1,

        [Description("Linh hoạt")]
        Dynamic = 2
    }

    public enum EnumLinkActionClick
    {
        [Description("Mở tab mới")]
        Blank = 1,

        [Description("Mở trực tiếp")]
        Self = 2
    }

    public enum EnumPostType
    {
        [LocalizedDescription("LB_ARTICLE", typeof(ManagerResource))]
        Article = 1,

        [LocalizedDescription("LB_TOP_BANNER", typeof(ManagerResource))]
        TopBanner = 2,

        [LocalizedDescription("LB_PRODUCT_SERVICE", typeof(ManagerResource))]
        WhatWeDo = 3,

        [LocalizedDescription("LB_NEW_ACTIVITY", typeof(ManagerResource))]
        HowWeWork = 4,

        //[LocalizedDescription("LB_ABOUT_US", typeof(ManagerResource))]
        //AboutUs = 5,

        //[LocalizedDescription("LB_TESIMONIAL", typeof(ManagerResource))]
        //Tesimonial = 6
    }

    public enum EnumShopCategory
    {
        SmallShop = 1,
                
        Agency = 2
    }

    public enum EnumBarCode
    {
        [Description("UNDEFINED")]
        UNDEFINED = 0,

        [Description("BT_SCAN_CODE_ANY")]
        ANY = 999,

        [Description("BT_SCAN_CODE_OCR")]
        ORC = 99,

        [Description("BT_SCAN_CODE_COMPOSI")]
        COMPOSITE = 16,

        [Description("BT_SCAN_CODE_MC")]
        MC = 14,

        [Description("BT_SCAN_CODE_DM")]
        DM = 13,

        [Description("BT_SCAN_CODE_PDF")]
        PDF = 12,

        [Description("BT_SCAN_CODE_QR")]
        QR = 11,

        [Description("BT_SCAN_CODE_GS1_DB")]
        GS1_DB = 10,

        [Description("BT_SCAN_CODE_COOP")]
        COOP = 9,

        [Description("BT_SCAN_CODE_TOF")]
        Composite = 8,

        [Description("BT_SCAN_CODE_C93")]
        C93 = 7,

        [Description("BT_SCAN_CODE_NW7")]
        NW7 = 6,

        [Description("BT_SCAN_CODE_ITF")]
        ITF = 5,

        [Description("BT_SCAN_CODE_C128")]
        C128 = 4,

        [Description("BT_SCAN_CODE_GS1_128")]
        GS1_128 = 3,

        [Description("BT_SCAN_CODE_C39")]
        C39 = 2,

        [Description("BT_SCAN_CODE_JAN")]
        JAN = 1
    }


    public static class EnumCommonCode
    {
        public static int Success = 1;
        public static int Error = -1;
        public static int Error_Info_NotFound = -2;
    }

    public static class EnumListCacheKeys
    {
        public static string Regions = "REGIONS";
        public static string Cities = "CITIES";
        public static string Prefectures = "PREFECTURES";
        public static string Stations = "STATIONS";
        public static string TrainLines = "TRAIN_LINES";
        public static string EmploymentTypes = "EMPLOYMENT_TYPES";
        public static string JapaneseLevels = "JAPANESE_LEVELS";
        public static string CompanySizes = "COMPANY_SIZES";
        public static string SalaryTypes = "SALARY_TYPES";
        public static string Qualifications = "QUALIFICATIONS";
        public static string Majors = "MAJORS";
        public static string ProcessStatus = "PROCESSSTATUS";
        public static string Fields = "FIELDS";
        public static string SubFields = "SUB_FIELDS";
        public static string Industries = "INDUSTRIES";
        public static string SubIndustries = "SUB_INDUSTRIES";
        public static string Agencies = "AGENCIES";
        public static string Tags = "TAGS_{0}";
        public static string CustomerAccounts = "CUSTOMERACCOUNTS_{0}";
        public static string FactoryAccounts = "FACTORYACCOUNTS_{0}";
        public static string ProductWarehouses = "PRODUCTWAREHOUSES";      

        public static string Countries = "COUNTRIES";
        public static string Allergies = "ALLERGIES";
        public static string Units = "UNITS";
        public static string Currencies = "CURRENCIES";
        public static string StockPlaces = "STOCKPLACES";
        public static string MaterialCategories = "MATERIALCATEGORIES";
        public static string MaterialTypes = "MATERIALTYPES";
        public static string Preservations = "PRESERVATIONS";        
        public static string Nutritions = "NUTRITIONS";        
        public static string Taxes = "TAXES";        
        public static string Tastes = "TASTES";
        public static string Colors = "COLORS";
        public static string DaysOfWeeks = "DAYSOFWEEKS";
        public static string Teachers = "TEACHERS";
        public static string Courses = "COURSES";
    }

    public static class EnumFormatInfoCacheKeys
    {
        //Detail info
        public static string City = "CITY_{0}";
        public static string Region = "REGION_{0}";
        public static string Station = "STATION_{0}";
        public static string EmailServers = "EMAIL_SERVERS_{0}";
        public static string EmailSettings = "EMAIL_SETTINGS_{0}_{1}";
        public static string EmailIdsSynchronized = "EMAIL_SYNC_{0}_{1}";

        public static string Student = "STUDENT_{0}";
        public static string Teacher = "TEACHER_{0}";
        public static string Course = "COURSE_{0}";
        public static string CourseSub = "COURSESUB_{0}";
        public static string Class = "CLASS_{0}";
        public static string ClassTeacher = "CLASSTEACHER_{0}";
        public static string ClassSchedule = "CLASSSCHEDULE_{0}";
    }

    public enum EnumJobStatus
    {
        [LocalizedDescription("LB_AWAITING_APPROVAL", typeof(ManagerResource))]
        Draft = 0,

        [LocalizedDescription("LB_ACTIVE", typeof(ManagerResource))]
        Published = 1,

        [LocalizedDescription("LB_CLOSED_JOB", typeof(ManagerResource))]
        Closed = 2,

        [LocalizedDescription("LB_SAVED", typeof(ManagerResource))]
        Saved = 3,

        [LocalizedDescription("LB_EXPRIRED", typeof(ManagerResource))]
        Expired = 4,

        [LocalizedDescription("LB_CANCELED", typeof(ManagerResource))]
        Canceled = 5
    }　
    public enum EnumTranslateStatus
    {
        [LocalizedDescription("LB_NOT_TRANSLATED_YET", typeof(ManagerResource))]
        NotTranslatedYet = 0,

        [LocalizedDescription("LB_TRANSLATED", typeof(ManagerResource))]
        Translated = 1
    }
    public enum EnumSearchType
    {
        JobSeeker = 1,
        Job = 2,
        Company = 3
    }

    public enum EnumEducationStatus
    {
        [LocalizedDescription("EDUCATION_STATUS_STUDYING", typeof(ManagerResource))]
        Studying = 0,

        [LocalizedDescription("EDUCATION_STATUS_GRADUATED", typeof(ManagerResource))]
        Graduated = 1,

        [LocalizedDescription("EDUCATION_STATUS_FLUNKED_OUT", typeof(ManagerResource))]
        FlunkedOut = -1
    }

    public enum EnumCertificateStatus
    {
        [LocalizedDescription("CERTIFICATE_STATUS_NOT_PASSED", typeof(ManagerResource))]
        NotPassed = 0,

        [LocalizedDescription("CERTIFICATE_STATUS_PASSED", typeof(ManagerResource))]
        Passed = 1
    }

    public enum EnumWorkStatus
    {
        [LocalizedDescription("WORK_STATUS_WORKING", typeof(ManagerResource))]
        Working = 0,

        [LocalizedDescription("WORK_STATUS_RESIGN", typeof(ManagerResource))]
        Resign = -1
    }

    public enum EnumCountry
    {
        //[LocalizedDescription("JOB_STATUS_DRAFT", typeof(ManagerResource))]
        Japan = 81,

        VietNam = 84
    }

    public enum EnumApplicationStatus
    {
        [LocalizedDescription("LB_APPLICATION_STATUS_AWAITING", typeof(ManagerResource))]
        Applied = 0,

        [LocalizedDescription("LB_APPLICATION_STATUS_AWAITING_INTERVIEW", typeof(ManagerResource))]
        Interview = 1,

        [LocalizedDescription("LB_APPLICATION_STATUS_IGNORE", typeof(ManagerResource))]
        Ignore = -1,

        [LocalizedDescription("LB_APPLICATION_STATUS_INVITED", typeof(ManagerResource))]
        Invited = 2,

        [LocalizedDescription("LB_APPLICATION_STATUS_CANCELED", typeof(ManagerResource))]
        Cancelled = -2
    }
    public enum EnumTagType
    {
        [LocalizedDescription("LB_KEYWORD_JOB", typeof(ManagerResource))]
        TagJob = 1,

        [LocalizedDescription("LB_KEYWORD_JOBSEEKER", typeof(ManagerResource))]
        TagJobSeeker = 2,
    }

    public enum EnumLanguageSkillLanguages
    {
        [LocalizedDescription("LANGUAGE_SKILL_LANGUAGES_JAPANESE", typeof(ManagerResource))]
        Japanese = 1,

        [LocalizedDescription("LANGUAGE_SKILL_LANGUAGES_ENGLISH", typeof(ManagerResource))]
        English = 2,

        [LocalizedDescription("LANGUAGE_SKILL_LANGUAGES_OTHER", typeof(ManagerResource))]
        Other = 0,
    }

    public enum EnumLanguageSkillLevel
    {
        [LocalizedDescription("LANGUAGE_SKILL_NATIVE_LEVEL", typeof(ManagerResource))]
        NATIVE_LEVEL = 1,

        [LocalizedDescription("LANGUAGE_SKILL_BUSINESS_LEVEL", typeof(ManagerResource))]
        BUSINESS_LEVEL = 2,

        [LocalizedDescription("LANGUAGE_SKILL_BUSINESS_WITH_ASSISTANCE_LEVEL", typeof(ManagerResource))]
        BUSINESS_WITH_ASSISTANCE_LEVEL = 3,

        [LocalizedDescription("LANGUAGE_SKILL_DAILY_LIFE_LEVEL", typeof(ManagerResource))]
        DAILY_LIFE_LEVEL = 4,

        [LocalizedDescription("LANGUAGE_SKILL_DAILY_LIFE_WITH_ASSISTANCE_LEVEL", typeof(ManagerResource))]
        DAILY_LIFE_WITH_ASSISTANCE_LEVEL = 5,
    }

    public enum EnumCandidateStatus
    {
        [LocalizedDescription("LB_APPLICATION_STATUS_AWAITING", typeof(ManagerResource))]
        Applied = 0,

        [LocalizedDescription("LB_APPLICATION_STATUS_AWAITING_INTERVIEW", typeof(ManagerResource))]
        Interview = 1,

        [LocalizedDescription("LB_APPLICATION_STATUS_IGNORE", typeof(ManagerResource))]
        Ignore = -1,

        [LocalizedDescription("LB_APPLICATION_STATUS_CANCELED", typeof(ManagerResource))]
        Cancelled = -2
    }

    public enum EnumApplicationInvitationStatus
    {
        [LocalizedDescription("INVITATION_STATUS_AWAITING", typeof(ManagerResource))]
        Awating = 0,

        [LocalizedDescription("INVITATION_STATUS_ACCEPTED", typeof(ManagerResource))]
        Accepted = 1,

        [LocalizedDescription("INVITATION_STATUS_IGNORED", typeof(ManagerResource))]
        Ignore = -1
    }

    public enum EnumEmploymentCalculateBy
    {
        //[LocalizedDescription("JOB_STATUS_DRAFT", typeof(ManagerResource))]
        Month = 0,

        //[LocalizedDescription("JOB_STATUS_PUBLISHED", typeof(ManagerResource))]
        Hour = 1
    }

    public enum EnumCvCreationMethod
    {
        //[LocalizedDescription("JOB_STATUS_DRAFT", typeof(ManagerResource))]
        CreateNew = 0,

        //[LocalizedDescription("JOB_STATUS_PUBLISHED", typeof(ManagerResource))]
        Clone = 1
    }

    public enum EnumScheduleCategory
    {
        [LocalizedDescription("SCHEDULE_CAT_MEETING", typeof(ManagerResource))]
        Meeting = 0,

        [LocalizedDescription("SCHEDULE_CAT_GUEST", typeof(ManagerResource))]
        Guest = 1,

        [LocalizedDescription("SCHEDULE_CAT_SALES", typeof(ManagerResource))]
        Sales = 2,

        //[LocalizedDescription("SCHEDULE_CAT_INTERVIEW", typeof(ManagerResource))]
        //Interview = 4,

        [LocalizedDescription("SCHEDULE_CAT_OTHERS", typeof(ManagerResource))]
        Others = 3
    }

    public enum EnumNotifActionTypeForAgency
    {
        System = 0,

        #region Application

        Application_Apply = 1000,

        Application_Cancel = 1001,

        #endregion

        #region Invitation

        Invitation_Received = 2000,

        Invitation_Accepted = 2001,

        Invitation_Canceled = 2002

        #endregion
    }

    public enum EnumNotifJob
    {
        #region Public job

        Approval = 3000,

        Accepted = 3001,

        Canceled = 3002

        #endregion
    }    

    public enum EnumEmailSettingTypes
    {
        [LocalizedDescription("LB_EMAIL_OUTGOING", typeof(ManagerResource))]
        OutGoing = 0,

        [LocalizedDescription("LB_EMAIL_INCOMING", typeof(ManagerResource))]
        InComing = 1
    }

    public static class EnumMessageTypes
    {
        public static int Text = 1;
        public static int Image = 2;
        public static int File = 3;
    }

    public enum EnumMessengerObjectType
    {
        JobSeeker = 0,
        Agency = 1,
        Admin = 2
    }

    public enum EnumEmailTargetType
    {
        JobSeeker = 0,
        Company = 1
    }

    public enum EnumOrderStatus
    {
        Pending = 0,
        Confirm = 1,
        ShippingPartial = 2,
        ShippingAll = 3,
        Paid = 4
    }

    public class LocalizedDescriptionAttribute : DescriptionAttribute
    {
        private readonly string _resourceKey;
        private readonly ResourceManager _resource;
        public LocalizedDescriptionAttribute(string resourceKey, Type resourceType)
        {
            _resource = new ResourceManager(resourceType);
            _resourceKey = resourceKey;
        }

        public override string Description
        {
            get
            {
                string displayName = _resource.GetString(_resourceKey);

                return string.IsNullOrEmpty(displayName)
                    ? string.Format("[[{0}]]", _resourceKey)
                    : displayName;
            }
        }
    }

    public static class EnumExtensions
    {
        public static string GetEnumDescription(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }

    #region FactoryOrderStatus

    public enum EnumFactoryOrderStatus
    {
        Pending = 0,

        Confirm = 1,

        BeginManufacturing = 2
    }

    #endregion
}