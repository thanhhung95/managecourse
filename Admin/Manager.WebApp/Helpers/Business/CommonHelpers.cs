﻿using System;
using System.Collections.Generic;
using Autofac;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using Manager.DataLayer;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using StackExchange.Redis.Extensions.Core;
using System.Linq;
using Manager.WebApp.Caching;
using Manager.SharedLibs;
using Newtonsoft.Json;
using Manager.WebApp.Models;
using System.Globalization;

namespace Manager.WebApp.Helpers
{
    public class CommonHelpers
    {
        private static readonly ILog logger = LogProvider.For<CommonHelpers>();

        public static List<DateTime> GetListDaysInCurrentWeek()
        {
            List<DateTime> dates = new List<DateTime>();
            try
            {
                DateTime today = DateTime.Today;
                int currentDayOfWeek = (int)today.DayOfWeek;
                DateTime sunday = today.AddDays(-currentDayOfWeek);
                DateTime monday = sunday.AddDays(1);

                // If we started on Sunday, we should actually have gone *back*
                // 6 days instead of forward 1...
                if (currentDayOfWeek == 0)
                {
                    monday = monday.AddDays(-7);
                }

                dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Failed when get list days in current week because: {0}", ex.ToString()));
            }

            return dates;
        }

        public static string GetCurrentLangageCode()
        {
            var currentLang = "ja-JP";
            try
            {
                currentLang = CultureInfo.CurrentCulture.ToString();
            }
            catch
            {

            }

            return currentLang;
        }

        public static int GetCurrentAgencyId()
        {
            var currentUser = AccountHelper.GetCurrentUser();
            if (currentUser != null)
            {
                if (currentUser.ParentId == 0)
                    return currentUser.StaffId;
                else
                    return currentUser.ParentId;
            }

            return 0;
        }

        public static IdentityUser GetUserByStaffId(int id = 0)
        {
            var myKey = "USER_" + id;
            IdentityUser info = null;

            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<IdentityUser>(myKey, out info);

                if (info == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IIdentityStore>();
                    info = myStore.GetByStaffId(id);

                    if (info != null)
                        //Storage to cache
                        cacheProvider.Set(myKey, info);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetUserById: " + ex.ToString());
            }

            return info;
        }

        public static List<IdentityUser> GetListUser(int parentId = 0)
        {
            var myKey = "USERS_" + parentId;
            List<Manager.DataLayer.IdentityUser> myList = null;

            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<List<Manager.DataLayer.IdentityUser>>(myKey, out myList);
                if (parentId == 0)
                    parentId = GetCurrentAgencyId();

                if (!myList.HasData())
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IIdentityStore>();
                    myList = myStore.GetListUser(parentId);

                    if (myList.HasData())
                        //Storage to cache
                        cacheProvider.Set(myKey, myList);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetListUser: " + ex.ToString());
            }

            return myList;
        }        
    }
}