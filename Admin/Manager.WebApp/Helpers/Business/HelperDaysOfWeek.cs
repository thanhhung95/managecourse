﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;

namespace Manager.WebApp.Helpers
{
    public class HelperDaysOfWeek
    {
        private static readonly ILog logger = LogProvider.For<HelperDaysOfWeek>();

        public static List<IdentityDaysOfWeek> GetList()
        {
            var myKey = string.Format(EnumListCacheKeys.DaysOfWeeks);
            List<IdentityDaysOfWeek> listData = null;
            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<List<IdentityDaysOfWeek>>(myKey, out listData);

                if (listData == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreDaysOfWeek>();
                    listData = myStore.GetList();

                    if (listData != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, listData, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetList: " + ex.ToString());
            }

            return listData;
        }

        public static void ClearCacheList()
        {
            var strError = string.Empty;
            try
            {
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Clear(string.Format(EnumListCacheKeys.DaysOfWeeks));
            }
            catch (Exception ex)
            {
                strError = string.Format("Failed to ClearCacheList:", ex.ToString());
                logger.Error(strError);
            }
        }
    }
}