﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;

namespace Manager.WebApp.Helpers
{
    public class HelperClass
    {
        private static readonly ILog logger = LogProvider.For<HelperClass>();

        public static IdentityClass GetBaseInfo(int id)
        {
            var myKey = string.Format(EnumFormatInfoCacheKeys.Class, id);
            IdentityClass info = null;
            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<IdentityClass>(myKey, out info);

                if (info == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreClass>();
                    info = myStore.GetById(id);

                    if (info != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, info, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetBaseInfo: " + ex.ToString());
            }

            return info;
        }

        public static void ClearCache(int id)
        {
            var strError = string.Empty;
            try
            {
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Clear(string.Format(EnumFormatInfoCacheKeys.Class, id));
            }
            catch (Exception ex)
            {
                strError = string.Format("Failed to ClearCache: {0}", ex.ToString());
                logger.Error(strError);
            }
        }
    }
}