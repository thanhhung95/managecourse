﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;

namespace Manager.WebApp.Helpers
{
    public class HelperCourseSub
    {
        private static readonly ILog logger = LogProvider.For<HelperCourseSub>();

        public static IdentityCourseSub GetBaseInfo(int id)
        {
            var myKey = string.Format(EnumFormatInfoCacheKeys.CourseSub, id);
            IdentityCourseSub info = null;

            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<IdentityCourseSub>(myKey, out info);

                if (info == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreCourseSub>();
                    info = myStore.GetById(id);

                    if (info != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, info, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetBaseInfo: " + ex.ToString());
            }

            return info;
        }

        public static void ClearCache(int id)
        {
            var strError = string.Empty;
            try
            {
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Clear(string.Format(EnumFormatInfoCacheKeys.CourseSub, id));
            }
            catch (Exception ex)
            {
                strError = string.Format("Failed to ClearCache: {0}", ex.ToString());
                logger.Error(strError);
            }
        }
    }
}