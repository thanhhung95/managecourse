﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;

namespace Manager.WebApp.Helpers
{
    public class HelperCurrency
    {
        private static readonly ILog logger = LogProvider.For<HelperCurrency>();

        public static List<IdentityCurrency> GetList()
        {
            var myKey = string.Format(EnumListCacheKeys.Currencies);
            List<IdentityCurrency> list = null;
            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<List<IdentityCurrency>>(myKey, out list);

                if (list == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreCurrency>();
                    list = myStore.GetList();

                    if (list.HasData())
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, list, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetList: " + ex.ToString());
            }

            return list;
        }

        public static void ClearCache()
        {
            var strError = string.Empty;
            try
            {
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Clear(EnumListCacheKeys.Currencies);
            }
            catch (Exception ex)
            {
                strError = string.Format("Failed to ClearCache: {0}", ex.ToString());
                logger.Error(strError);
            }
        }
    }
}