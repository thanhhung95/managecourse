﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;

namespace Manager.WebApp.Helpers
{
    public class HelperCourse
    {
        private static readonly ILog logger = LogProvider.For<HelperCourse>();

        public static IdentityCourse GetBaseInfo(int id)
        {
            var myKey = string.Format(EnumFormatInfoCacheKeys.Course, id);
            IdentityCourse info = null;

            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<IdentityCourse>(myKey, out info);

                if (info == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreCourse>();
                    info = myStore.GetById(id);

                    if (info != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, info, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetBaseInfo: " + ex.ToString());
            }

            return info;
        }

        public static List<IdentityCourse> GetList()
        {
            var myKey = string.Format(EnumListCacheKeys.Courses);
            List<IdentityCourse> listData = null;

            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<List<IdentityCourse>>(myKey, out listData);

                if (listData == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreCourse>();
                    listData = myStore.GetList();

                    if (listData != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, listData, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetBaseInfo: " + ex.ToString());
            }

            return listData;
        }

        public static void ClearCache(int id)
        {
            var strError = string.Empty;
            try
            {
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();

                if (id > 0)
                {
                    cacheProvider.Clear(string.Format(EnumFormatInfoCacheKeys.Course, id));
                    cacheProvider.Clear(string.Format(EnumListCacheKeys.Courses));
                }
                else
                {
                    cacheProvider.Clear(string.Format(EnumListCacheKeys.Courses));
                }
                
            }
            catch (Exception ex)
            {
                strError = string.Format("Failed to ClearCache: {0}", ex.ToString());
                logger.Error(strError);
            }
        }
    }
}