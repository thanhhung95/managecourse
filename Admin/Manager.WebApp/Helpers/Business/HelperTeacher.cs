﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Caching.Providers;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;

namespace Manager.WebApp.Helpers
{
    public class HelperTeacher
    {
        private static readonly ILog logger = LogProvider.For<HelperTeacher>();

        public static IdentityTeacher GetBaseInfo(int id)
        {
            var myKey = string.Format(EnumFormatInfoCacheKeys.Teacher, id);
            IdentityTeacher info = null;
            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<IdentityTeacher>(myKey, out info);

                if (info == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreTeacher>();
                    info = myStore.GetById(id);

                    if (info != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, info, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetBaseInfo: " + ex.ToString());
            }

            return info;
        }

        public static List<IdentityTeacher> GetList()
        {
            var myKey = string.Format(EnumListCacheKeys.Teachers);
            List<IdentityTeacher> listData = null;
            try
            {
                //Check from cache first
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();
                cacheProvider.Get<List<IdentityTeacher>>(myKey, out listData);

                if (listData == null)
                {
                    var myStore = GlobalContainer.IocContainer.Resolve<IStoreTeacher>();
                    listData = myStore.GetList();

                    if (listData != null)
                    {
                        //Storage to cache
                        cacheProvider.Set(myKey, listData, SystemSettings.DefaultCachingTimeInMinutes);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Could not GetList: " + ex.ToString());
            }

            return listData;
        }

        public static void ClearCache(int id)
        {
            var strError = string.Empty;
            try
            {
                var cacheProvider = GlobalContainer.IocContainer.Resolve<ICacheProvider>();

                if (id > 0)
                {
                    cacheProvider.Clear(string.Format(EnumFormatInfoCacheKeys.Teacher, id));
                    cacheProvider.Clear(string.Format(EnumListCacheKeys.Teachers));
                }
                else
                {
                    cacheProvider.Clear(string.Format(EnumListCacheKeys.Teachers));
                }
            }
            catch (Exception ex)
            {
                strError = string.Format("Failed to ClearCache: {0}", ex.ToString());
                logger.Error(strError);
            }
        }

    }
}