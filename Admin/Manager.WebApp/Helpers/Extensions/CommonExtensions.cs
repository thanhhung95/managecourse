﻿using Manager.SharedLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Manager.WebApp.Helpers
{
    public static class CommonExtensions
    {
        public static string ToUTF8(this string rawStr)
        {
            var myStr = rawStr;
            try
            {
                byte[] bytes = Encoding.Default.GetBytes(rawStr);
                myStr = Encoding.UTF8.GetString(bytes);
            }
            catch
            {
                return rawStr;
            }

            return myStr;
        }

        public static IEnumerable<T> OrderBySequence<T, TId>(this IEnumerable<T> source, IEnumerable<TId> order, Func<T, TId> idSelector)
        {
            var lookup = source.ToLookup(idSelector, t => t);
            foreach (var id in order)
            {
                foreach (var t in lookup[id])
                {
                    yield return t;
                }
            }
        }

        public static string ToLocaleString(this decimal number)
        {
            var myStr = string.Empty;
            try
            {
                //number = Utils.RoundUpNumber(number, decimalLength);
                myStr = number.ToString("G29");
            }
            catch
            {
                return myStr;
            }

            return myStr;
        }

        public static string ToLocaleString(this decimal? number)
        {
            var myStr = string.Empty;
            try
            {
                //number = Utils.RoundUpNumber(number, decimalLength);
                if (number.HasValue)
                {
                    myStr = number.Value.ToString("G29");
                }
            }
            catch
            {
                return myStr;
            }

            return myStr;
        }

        public static string ToLocaleString(this int number)
        {
            var myStr = string.Empty;
            try
            {
                //number = Utils.RoundUpNumber(number, decimalLength);
                myStr = number.ToString("G29");
            }
            catch
            {
                return myStr;
            }

            return myStr;
        }

        public static string ToLocaleString(this int? number)
        {
            var myStr = string.Empty;
            try
            {
                //number = Utils.RoundUpNumber(number, decimalLength);
                if (number.HasValue)
                {
                    myStr = number.Value.ToString("G29");
                }
            }
            catch
            {
                return myStr;
            }

            return myStr;
        }

        public static IEnumerable<string> SplitOnLength(this string input, int length)
        {
            int index = 0;
            while (index < input.Length)
            {
                if (index + length < input.Length)
                    yield return input.Substring(index, length);
                else
                    yield return input.Substring(index);

                index += length;
            }
        }
    }

    public static class ApiExtensions
    {
        public static bool HasData(this ApiResponseCommonModel res)
        {
            if (res != null && res.Value != null)
                return true;

            return false;
        }
        
        public static T ConvertData<T>(this ApiResponseCommonModel res)
        {
            try
            {
                if (res.HasData())
                {
                    var myObj = JsonConvert.DeserializeObject<T>(res.Value.ToString());
                    return myObj;
                }                
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return default(T);
        }

        //public static T GetUserCookie<T>(string key)
        //{
        //    string value = string.Empty;
        //    HttpCookie cookie = HttpContext.Current.Request.Cookies[key];

        //    if (cookie != null)
        //    {
        //        // For security purpose, we need to encrypt the value.
        //        //HttpCookie decodedCookie = HttpSecureCookie.Decode(cookie);               
        //        try
        //        {
        //            //var decryptedStr = Base64Decode(cookie.Value);
        //            //var myObj = JsonConvert.DeserializeObject<T>(decryptedStr);
        //            //return myObj;

        //            var myObj = DecodeDataFromToken<T>(cookie.Value);
        //            return myObj;
        //        }
        //        catch
        //        {
        //            ClearCookie(key);
        //            return default(T);
        //        }
        //    }

        //    return default(T);
        //}
    }

    public static class LevenshteinDistance
    {
        /// <summary>
        /// Compute the distance between two strings.
        /// </summary>
        public static int Compute(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }

}