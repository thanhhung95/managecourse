﻿using System.Web;
using System.Web.Optimization;

namespace Manager.WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/bundles/css-bootstraptable").Include(
                "~/Content/Extensions/bootstraptable/bootstrap-table.min.css",
                "~/Content/Extensions/bootstraptable/bootstrap-table-fixed-columns.min.css",
                "~/Content/Extensions/context/jquery.contextMenu.min.css",
                "~/Scripts/Plugins/Fancybox/css/fancybox.min.css"
            ));

            //bundles.Add(new ScriptBundle("~/bundles/emoji").Include(
            //         "~/Content/Extensions/emoji/lib/js/config.js",
            //         "~/Content/Extensions/emoji/lib/js/util.js",
            //         "~/Content/Extensions/emoji/lib/js/jquery.emojiarea.js",
            //         "~/Content/Extensions/emoji/lib/js/emoji-picker.js"
            //         ));

            bundles.Add(new ScriptBundle("~/bundles/js-bootstraptable").Include(
                "~/Content/Extensions/context/jquery.contextMenu.min.js",
                "~/Content/Extensions/context/jquery.ui.position.js",
                "~/Content/Extensions/bootstraptable/bootstrap-table.min.js",
                "~/Content/Extensions/bootstraptable/bootstrap-table-fixed-columns.min.js",
                "~/Scripts/Plugins/Fancybox/js/fancybox.min.js"
            ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
