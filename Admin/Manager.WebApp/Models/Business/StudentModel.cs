﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Manager.WebApp.Resources;
using Manager.DataLayer.Entities;
using System;

namespace Manager.WebApp.Models
{
    public class ManageStudentModel : CommonPagingModel
    {
        public List<IdentityStudent> SearchResults { get; set; }

        public int? Status { get; set; }
    }

    public class StudentUpdateModel : CommonPagingModel
    {
        public int Id { get; set; }

        public int Gender { get; set; }

        public DateTime? DateBirth { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "必須項目です")]
        [EmailAddress(ErrorMessage = "メールアドレスの形式が正しくありません")]
        public string Email { get; set; }

        public string Phone { get; set; }

        [Required(ErrorMessage = "必須項目です")]
        public string Name { get; set; }

        public List<IdentityClass> ListClass { get; set; }
    }

    public class AttendanceModel : CommonPagingModel
    {
        public int Id { get; set; }

        public int ClassScheduleId { get; set; }

        public int StudentId { get; set; }

        public string Comment { get; set; }

        public string Note { get; set; }

        public int? Status { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public int ClassId { get; set; }

        public IdentityClass Class { get; set; }

        public List<IdentityAttendance> ListAttendance { get; set; }

        public bool IsFilter { get; set; }
    }
}