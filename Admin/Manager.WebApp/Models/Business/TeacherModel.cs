﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Manager.WebApp.Resources;
using Manager.DataLayer.Entities;
using System;
using SelectItem = System.Web.Mvc.SelectListItem;

namespace Manager.WebApp.Models
{
    public class ManageTeacherModel : CommonPagingModel
    {
        public List<IdentityTeacher> SearchResults { get; set; }

        //For filtering
        public int? Status { get; set; }
    }

    public class TeacherUpdateModel
    {
        public int Id { get; set; }

        public int Gender { get; set; }

        public DateTime? DateBirth { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "必須項目です")]
        [EmailAddress(ErrorMessage = "メールアドレスの形式が正しくありません")]
        public string Email { get; set; }

        public string Phone { get; set; }

        [Required(ErrorMessage = "必須項目です")]
        public string Name { get; set; }


        // ACOUNT TEACHER
        public IEnumerable<SelectItem> RolesList { get; set; }

        [Required(ErrorMessageResourceType = typeof(ManagerResource), ErrorMessageResourceName = nameof(ManagerResource.ERROR_NOT_NULL_REQUIRED))]
        [Display(ResourceType = typeof(Resources.ManagerResource), Name = nameof(ManagerResource.LB_USERNAME))]
       /* [EmailAddress(ErrorMessageResourceType = typeof(ManagerResource), ErrorMessageResourceName = nameof(ManagerResource.ERROR_EMAIL_INVALID))]*/
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ManagerResource), ErrorMessageResourceName = nameof(ManagerResource.ERROR_NOT_NULL_REQUIRED))]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.ManagerResource), Name = nameof(ManagerResource.LB_PASSWORD))]
        public string Password { get; set; }

        public string Role { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.ManagerResource), Name = nameof(ManagerResource.LB_CONFIRM_PASSWORD))]
        [Compare("Password", ErrorMessageResourceType = typeof(ManagerResource), ErrorMessageResourceName = nameof(ManagerResource.ERROR_CONFRIM_PASSWORD_NOT_MATCH))]
        public string ConfirmPassword { get; set; }


        public LockoutViewModel Lockout { get; set; }

    }
}