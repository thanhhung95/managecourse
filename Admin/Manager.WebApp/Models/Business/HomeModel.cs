﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Manager.WebApp.Resources;
using Manager.DataLayer.Entities;
using System;

namespace Manager.WebApp.Models
{
    public class ManageHomeModel : CommonPagingModel
    {
        public List<HomeUpdateModel> SearchResults { get; set; }

        public int TeacherId { get; set; }
    }

    public class HomeUpdateModel : CommonPagingModel
    {
        public int ClassId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public IdentityTeacher Teacher { get; set; }

        public IdentityClass Class { get; set; }
    }
}