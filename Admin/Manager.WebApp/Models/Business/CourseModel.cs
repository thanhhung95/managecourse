﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Manager.WebApp.Resources;
using Manager.DataLayer.Entities;
using System;

namespace Manager.WebApp.Models
{
    public class ManageCourseModel : CommonPagingModel
    {
        public List<IdentityCourse> SearchResults { get; set; }

        public List<IdentityCourseSub> CourseSubs { get; set; }

        public int? Status { get; set; }
    }

    public class CourseUpdateModel : CourseSubUpdateModel
    {

    }

    public class CourseSubUpdateModel
    {
        public int Id { get; set; }

        public int CourseId { get; set; }
        
        [Required(ErrorMessage = "コース名・サブコース名が必須です")]
        public string Name { get; set; }

        public string Description { get; set; }

        public int Status { get; set; }
    }
}