﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Manager.WebApp.Resources;
using Manager.DataLayer.Entities;
using System;
using Manager.WebApp.Helpers;

namespace Manager.WebApp.Models
{
    public class ManageClassModel : CommonPagingModel
    {
        public List<IdentityClass> SearchResults { get; set; }

        public int? Status { get; set; }
    }

    public class ClassDetail : CommonPagingModel
    {
        public IdentityClass Class { get; set; }

        public List<IdentityClassTeacher> ClassTeachers { get; set; }

        public IdentityCourse Course { get; set; }

        public IdentityCourseSub CourseSub { get; set; }

        public List<IdentityStudent> SearchResults { get; set; }

        public int? Status { get; set; }

        public bool IsPage { get; set; }
    }

    public class ClassStudentUpdateModel : CommonPagingModel
    {
        public int ClassId { get; set; }

        public int StudentId { get; set; }

        public string ListStudentId { get; set; }

        public int? Status { get; set; }

        public List<IdentityStudent> SearchResults { get; set; }

        public bool IsPage { get; set; }
    }

    public class ClassScheduleUpdateModel : ClassUpdateModel
    {
        public int ClassId { get; set; }

        public int TeacherId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Comment { get; set; }

        public string Note { get; set; }

        public string StartTimeStr { get; set; }

        public string EndTimeStr { get; set; }

        public IdentityTeacher Teacher { get; set; }

        public DateTime? DateSelect { get; set; }

        public List<IdentityAttendance> ListAttendances { get; set; }

        public List<IdentityStudent> SearchResults { get; set; }
    }

    public class ClassUpdateModel : CommonPagingModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int CourseId { get; set; }

        public int CourseSubId { get; set; }

        public TimeSpan? OpenTime { get; set; }

        public TimeSpan? CloseTime { get; set; }

        public int Status { get; set; }

        public List<IdentityClassTeacher> ClassTeachers { get; set; }

        public List<int> ListWeekdays { get; set; }

        public List<int> TeacherDuplicate { get; set; }
    }
}