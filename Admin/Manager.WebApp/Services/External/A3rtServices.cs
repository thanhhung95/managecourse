﻿using Manager.ShareLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Models;
using Manager.WebApp.Resources;
using Manager.WebApp.Settings;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using RestSharp;
using System.Configuration;

namespace Manager.WebApp.Services
{
    public class ApiA3rtProofreadingResponse
    {
        public string resultID { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public string inputSentence { get; set; }
        public string normalizedSentence { get; set; }
        public string checkedSentence { get; set; }
        public List<ApiA3rtProofreadingAlert> alerts { get; set; }
    }

    public class ApiA3rtProofreadingAlert
    {
        public int pos { get; set; }
        public string word { get; set; }
        public decimal? score { get; set; }
        public List<string> suggestions { get; set; }
    }

    public class A3rtServices
    {
        private static readonly ILog logger = LogProvider.For<A3rtServices>();

        public static async Task<ApiA3rtProofreadingResponse> SingleSentenceCheckAsync(string sentence)
        {
            var result = new ApiA3rtProofreadingResponse();
            try
            {               
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("apikey", ConfigurationManager.AppSettings["A3rt:ApiProofreadingKey"]);
                request.AddParameter("sentence", sentence);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var client = new RestClient(ConfigurationManager.AppSettings["A3rt:ApiProofreadingUrl"]);    
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.BadRequest)
                {
                    var responseString = response.Content;
                    result = JsonConvert.DeserializeObject<ApiA3rtProofreadingResponse>(responseString);
                }                

                await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed when SingleSentenceCheckAsync because: {0}", ex.ToString());
                logger.Error(strError);

                throw new CustomSystemException(ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT);
            }

            return result;
        }

        private static void HttpStatusCodeTrace(HttpResponseMessage response)
        {
            var statusCode = (int)response.StatusCode;
            if (statusCode != (int)HttpStatusCode.OK && statusCode != 1)
            {
                logger.Debug(string.Format("Return code: {0}, message: {1}", response.StatusCode, response.RequestMessage.RequestUri));
            }
        }
    }
}