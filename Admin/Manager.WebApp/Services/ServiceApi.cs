﻿//using Manager.ShareLibs;
//using Manager.SharedLibs.Logging;
//using Manager.WebApp.Resources;
//using Newtonsoft.Json;
//using System;
//using System.Net;
//using System.Net.Http;
//using System.Threading.Tasks;
//using System.Collections.Generic;
//using RestSharp;
//using System.Configuration;
//using Manager.DataLayer.Entities;
//using System.Text;
//using Manager.WebApp.Settings;
//using System.Net.Http.Headers;

//namespace Manager.WebApp.Services
//{
//    #region Response Api

//    public class ApiResRakutenMessageModel
//    {
//        public string messageType { get; set; }
//        public string messageCode { get; set; }
//        public string message { get; set; }
//    }

//    public class ApiResRakutenPaginationModel
//    {
//        public int totalRecordsAmount { get; set; }
//        public int totalPages { get; set; }
//        public int requestPage { get; set; }
//    }

//    public class ApiResRakutenOrderSearchModel
//    {
//        public List<ApiResRakutenMessageModel> MessageModelList { get; set; }
//        public List<string> orderNumberList { get; set; }
//        public ApiResRakutenMessageModel PaginationResponseModel { get; set; }
//    }

//    public class ApiResRakutenGetOrdersModel
//    {
//        public List<ApiResRakutenMessageModel> MessageModelList { get; set; }
//        //public List<IdentityRakutenOrder> OrderModelList { get; set; }
//    }

//    #endregion

//    #region Request Api

//    public class ApiRakutenSortModelList
//    {
//        public int sortColumn { get; set; }
//        public int sortDirection { get; set; }
//    }

//    public class ApiRakutenPaginationRequestModel
//    {
//        public int requestRecordsAmount { get; set; }
//        public int requestPage { get; set; }
//        public List<ApiRakutenSortModelList> SortModelList { get; set; }
//    }

//    public class ApiRakutenOrderSearchModel
//    {
//        public List<int> orderProgressList { get; set; }
//        public int dateType { get; set; }
//        public string startDatetime { get; set; }
//        public string endDatetime { get; set; }
//        public ApiRakutenPaginationRequestModel PaginationRequestModel { get; set; }
//    }

//    public class ApiRakutenGetOrdersModel
//    {
//        public List<string> orderNumberList { get; set; }
//        public int version { get; set; }
//    }

//    #endregion

//    public class RakutenServices
//    {
//        private static readonly ILog logger = LogProvider.For<RakutenServices>();

//        public static async Task<ApiResRakutenOrderSearchModel> OrderSearchAsync()
//        {
//            var result = new ApiResRakutenOrderSearchModel();
//            try
//            {
//                var requestObj = new ApiRakutenOrderSearchModel();
//                requestObj.orderProgressList = new List<int>();
//                requestObj.orderProgressList.Add(300);
//                requestObj.dateType = 1;
//                //requestObj.startDatetime = DateTime.UtcNow;
//                //requestObj.endDatetime = requestObj.startDatetime.AddDays(1);
//                requestObj.startDatetime = "2021-10-10T02:00:00+0900";
//                requestObj.endDatetime = "2021-10-11T02:00:00+0900";
//                requestObj.PaginationRequestModel = new ApiRakutenPaginationRequestModel();
//                requestObj.PaginationRequestModel.requestRecordsAmount = 30;
//                requestObj.PaginationRequestModel.requestPage = 1;
//                requestObj.PaginationRequestModel.SortModelList = new List<ApiRakutenSortModelList>();
//                requestObj.PaginationRequestModel.SortModelList.Add(new ApiRakutenSortModelList { sortColumn = 1, sortDirection = 1 });

//                var request = new RestRequest(Method.POST);
//                request.RequestFormat = DataFormat.Json;
//                request.AddJsonBody(requestObj);

//                request.AddHeader("Content-Type", "application/json; charset=utf-8");

//                request.RakutenAuthorization();

//                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

//                var client = new RestClient(ConfigurationManager.AppSettings["Rakuten:ApiOrderSearchUrl"]);
//                IRestResponse response = client.Execute(request);
//                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.BadRequest)
//                {
//                    var responseString = response.Content;
//                    result = JsonConvert.DeserializeObject<ApiResRakutenOrderSearchModel>(responseString);
//                }

//                await Task.FromResult(result);
//            }
//            catch (Exception ex)
//            {
//                var strError = string.Format("Failed when OrderSearchAsync because: {0}", ex.ToString());
//                logger.Error(strError);

//                throw new CustomSystemException(ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT);
//            }

//            return result;
//        }

//        public static async Task<ApiResRakutenGetOrdersModel> GetOrdersAsync(List<string> ordersNum)
//        {
//            var result = new ApiResRakutenGetOrdersModel();
//            try
//            {
//                var requestObj = new ApiRakutenGetOrdersModel();
//                requestObj.orderNumberList = ordersNum;
//                requestObj.version = 3;

//                var request = new RestRequest(Method.POST);
//                request.RequestFormat = DataFormat.Json;
//                request.AddJsonBody(requestObj);

//                request.AddHeader("Content-Type", "application/json; charset=utf-8");

//                request.RakutenAuthorization();

//                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

//                var client = new RestClient(ConfigurationManager.AppSettings["Rakuten:ApiOrderGetOrdersUrl"]);
//                IRestResponse response = client.Execute(request);
//                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.BadRequest)
//                {
//                    var responseString = response.Content;
//                    result = JsonConvert.DeserializeObject<ApiResRakutenGetOrdersModel>(responseString);
//                }

//                await Task.FromResult(result);
//            }
//            catch (Exception ex)
//            {
//                var strError = string.Format("Failed when GetOrdersAsync because: {0}", ex.ToString());
//                logger.Error(strError);

//                throw new CustomSystemException(ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT);
//            }

//            return result;
//        }

//        //public static async Task<ApiResRakutenGetOrdersModel> GetOrdersAsync(List<string> ordersNum)
//        //{
//        //    var result = new ApiResRakutenGetOrdersModel();
//        //    try
//        //    {
//        //        var requestObj = new ApiRakutenGetOrdersModel();
//        //        requestObj.orderNumberList = ordersNum;
//        //        requestObj.version = 3;

//        //        var request = new RestRequest(Method.POST);
//        //        request.RequestFormat = DataFormat.Json;
//        //        request.AddJsonBody(requestObj);

//        //        request.AddHeader("Content-Type", "application/json; charset=utf-8");

//        //        request.RakutenAuthorization();

//        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

//        //        var client = new RestClient(ConfigurationManager.AppSettings["Rakuten:ApiOrderGetOrdersUrl"]);
//        //        //IRestResponse response = client.Execute(request);
//        //        if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.BadRequest)
//        //        {
//        //            var responseString = response.Content;
//        //            result = JsonConvert.DeserializeObject<ApiResRakutenGetOrdersModel>(responseString);
//        //        }

//        //        await Task.FromResult(result);
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        var strError = string.Format("Failed when GetOrdersAsync because: {0}", ex.ToString());
//        //        logger.Error(strError);

//        //        throw new CustomSystemException(ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT);
//        //    }

//        //    return result;
//        //}

//        private static void HttpStatusCodeTrace(HttpResponseMessage response)
//        {
//            var statusCode = (int)response.StatusCode;
//            if (statusCode != (int)HttpStatusCode.OK && statusCode != 1)
//            {
//                logger.Debug(string.Format("Return code: {0}, message: {1}", response.StatusCode, response.RequestMessage.RequestUri));
//            }
//        }
//    }
//}