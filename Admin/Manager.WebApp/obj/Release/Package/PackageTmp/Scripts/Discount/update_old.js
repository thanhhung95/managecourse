﻿var customerEngine;
$.support.cors = true;

customerEngine = new Bloodhound({
    identify: function (o) { return o.materialid; },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name'),
    dupDetector: function (a, b) { return a.Id === b.Id; },
    remote: {
        url: '/Customer/GetSuggestion?query=%QUERY',
        wildcard: '%QUERY',
        cache: true,
        replace: function (url, uriEncodedQuery) {
            //var exceptIds = $("#ExceptMaterials").val();
            var exceptIds = "";
            return url.replace("%QUERY", uriEncodedQuery) + '&excepts=' + encodeURIComponent(exceptIds)
        },
    }
});

var CustomerControl = {
    CustomersRptTypeAhead: function (ct) {
        ct.find(".customer-suggestion-new").each(function () {
            var customerTemp = Handlebars.compile($("#customer-template").html());
            var el = $(this);
            el.removeClass("customer-suggestion-new");
            el.typeahead({
                highlight: true,
                minLength: 0,
                cache: false
            },
                {
                    limit: 10,
                    source: function (query, sync, async) {
                        if (query === '') {
                            //sync(makerEngine.get('a'));
                            //async([]);
                            customerEngine.search(query, sync, async);
                        }
                        else {
                            customerEngine.search(query, sync, async);
                        }
                    },
                    displayKey: 'Name',
                    templates: {
                        suggestion: customerTemp
                    }
                })
                .on('typeahead:selected', function (evt, item) {
                    /*console.log(item);*/

                    CustomerControl.CustomerSelectProcess(ct, item);
                });
        });
    },
    CustomerSelectProcess: function (ct, item) {
        CustomerControl.ClearParentsInfo(ct);
        if (item !== null) {
            ct.find(".customer-id").val(item.Id);
            ct.find(".customer-name").typeahead('val', item.Name);
            var parent = ct.find(".customer-parents");
            var parentContainer = $(".customer-parents-container");
            parentContainer.removeClass("hidden");
            parentContainer.areaLoading();

            var dt = { id: item.Id };
            $.aGet("/Customer/GetParents", dt, function (result) {
                if (result.returnList) {
                    if (result.returnList.length > 0) {
                        var idx = 0;
                        parentContainer.addClass("has-parents");

                        result.returnList.forEach(function (c) {
                            var prElem = $("<div>");
                            prElem.addClass("row");
                            if (idx > 0) {
                                prElem.addClass("mt10");
                            }

                            var prElemName = $("<div>");
                            prElemName.html(c.Name);
                            prElemName.addClass("col-md-8");

                            var prElemDiscount = $("<div>");
                            prElemDiscount.addClass("col-md-4");
                            //var inputDiscount = `<input type='text' name='Parents[${idx}].[DiscountValue]' class='form-control text-right' />`;
                            //var inputParentId = `<input type='hidden' name='Parents[${idx}].[Id]' class='form-control' value='${c.Id}' />`;
                            //var inputCustomerId = `<input type='hidden' name='Parents[${idx}].[CustomerId]' class='form-control' value='${result.customerId}' />`;

                            var inputDiscount = `<input type='text' name='[Parents[${idx}]].[DiscountValue]' class='form-control text-right' />`;
                            var inputParentId = `<input type='hidden' name='[Parents[${idx}]].[Id]' class='form-control' value='${c.Id}' />`;
                            var inputCustomerId = `<input type='hidden' name='[Parents[${idx}]].[CustomerId]' class='form-control' value='${result.customerId}' />`;
                            prElemDiscount.append(inputDiscount);
                            prElemDiscount.append(inputParentId);
                            prElemDiscount.append(inputCustomerId);


                            prElem.append(prElemName);
                            prElem.append(prElemDiscount);

                            parent.append(prElem);

                            idx++;
                        });
                    } else {
                        parentContainer.addClass("hidden");
                    }
                }

                parentContainer.areaLoading();
            }, "json", false);
        }
    },
    ClearParentsInfo: function (ct) {
        var parent = ct.find(".customer-parents");
        var parentContainer = $(".customer-parents-container");

        parent.html("");
        parentContainer.removeClass("has-parents");
        parentContainer.addClass("hidden");
    }
};

$("#customersRpt").repeater({
    initEmpty: !1,
    show: function () {
        $(this).show();

        CustomerControl.CustomersRptTypeAhead($(this));

        var idx = 0;
        $(".rpt-item-container").each(function () {
            $(this).attr("data-idx", idx);
            idx++;
        });
    },
    hide: function (e) {
        $(this).fadeOut(e);
        setTimeout(function () {
            $(this).remove();

            var idx = 0;
            $(".rpt-item-container").each(function () {
                $(this).attr("data-idx", idx);
                idx++;
            });
        }, 500);
    }
});

$(function () {
    $(".rpt-item-container").each(function () {
        var ct = $(this);
        CustomerControl.CustomersRptTypeAhead(ct);
    });    
});