﻿//const { hide } = require("@popperjs/core");

var customerEngine;
$.support.cors = true;

customerEngine = new Bloodhound({
    identify: function (o) { return o.materialid; },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name'),
    dupDetector: function (a, b) { return a.Id === b.Id; },
    remote: {
        url: '/Customer/GetSuggestion?query=%QUERY',
        wildcard: '%QUERY',
        cache: true,
        replace: function (url, uriEncodedQuery) {
            //var exceptIds = $("#ExceptMaterials").val();
            var exceptIds = "";
            return url.replace("%QUERY", uriEncodedQuery) + '&excepts=' + encodeURIComponent(exceptIds)
        },
    }
});

var CustomerControl = {
    CustomersRptTypeAhead: function (ct) {
        ct.find(".customer-suggestion-new").each(function () {
            var customerTemp = Handlebars.compile($("#customer-template").html());
            var el = $(this);
            el.removeClass("customer-suggestion-new");
            el.typeahead({
                highlight: true,
                minLength: 0,
                cache: false
            },
                {
                    limit: 10,
                    source: function (query, sync, async) {
                        if (query === '') {
                            //sync(makerEngine.get('a'));
                            //async([]);
                            customerEngine.search(query, sync, async);
                        }
                        else {
                            customerEngine.search(query, sync, async);
                        }
                    },
                    displayKey: 'Name',
                    templates: {
                        suggestion: customerTemp
                    }
                })
                .on('typeahead:selected', function (evt, item) {
                    /*console.log(item);*/

                    CustomerControl.CustomerSelectProcess(ct, item);
                });
        });
    },
    CustomerSelectProcess: function (ct, item) {
        CustomerControl.ClearParentsInfo(ct);
        if (item !== null) {
            ct.find(".customer-id").val(item.Id);
            ct.find(".customer-name").typeahead('val', item.Name);
            var parent = ct.find(".customer-parents");
            var parentContainer = $(".customer-parents-container");
            //parentContainer.removeClass("hidden");
            parentContainer.areaLoading();

            var dt = { id: item.Id };
            $.aGet("/Customer/GetParents", dt, function (result) {
                if (result.returnList) {
                    if (result.returnList.length > 0) {
                        var idx = 0;
                        parentContainer.addClass("has-parents");
                        ct.find(".customer-parents-container").removeClass("hidden");

                        result.returnList.forEach(function (c) {
                            var prElem = $("<div>");
                            prElem.addClass("row");
                            if (idx > 0) {
                                prElem.addClass("mt10");
                            }

                            var prElemName = $("<div>");
                            prElemName.html(c.Name);
                            prElemName.addClass("col-md-8");

                            var prElemDiscount = $("<div>");
                            prElemDiscount.addClass("col-md-4");
                            var inputDiscount = `<input type='text' name='Customers[0][Parents[${idx}]][DiscountValue]' data-name='DiscountValue' data-idx='${idx}' class='form-control text-right pr-item-prop' />`;
                            var inputParentId = `<input type='hidden' name='Customers[0][Parents[${idx}]][Id]' data-name='Id' data-idx='${idx}' class='form-control pr-item-prop' value='${c.Id}' />`;                            
                            prElemDiscount.append(inputDiscount);
                            prElemDiscount.append(inputParentId);

                            prElem.append(prElemName);
                            prElem.append(prElemDiscount);

                            parent.append(prElem);

                            idx++;

                            CustomerControl.RenameParentsElement();
                        });
                    } else {
                        ct.find(".customer-parents-container").remove();
                    }
                }

                parentContainer.areaLoading();
            }, "json", false);
        }
    },
    ClearParentsInfo: function (ct) {
        var parent = ct.find(".customer-parents");
        //var parentContainer = $(".customer-parents-container");
        var parentContainer = ct.find(".customer-parents-container");

        parent.html("");
        parentContainer.removeClass("has-parents");
        parentContainer.addClass("hidden");
    },
    RenameParentsElement: function () {
        $(".rpt-item-container").each(function () {
            var ct = $(this);
            var ctIdx = ct.data("idx");
            var parentsEl = ct.find(".customer-parents");
            if (parentsEl) {               
                parentsEl.find(".pr-item-prop").each(function () {
                    var it = $(this);
                    var itIdx = it.data("idx");
                    var itName = it.data("name");
                    it.attr("name", `Customers[${ctIdx}][Parents[${itIdx}]][${itName}]`);
                });
            }
        });
    },
    RemoveOldParentElement: function () {
        var el = $(".rpt-item-container").last();
        el.find(".customer-parents-container").find(".row").remove();
        el.find(".customer-parents-container").addClass("hidden");
    }
};

$("#customersRpt").repeater({
    initEmpty: !1,
    show: function () {
        $(this).show();

        CustomerControl.CustomersRptTypeAhead($(this));

        var idx = 0;
        $(".rpt-item-container").each(function () {
            $(this).attr("data-idx", idx);
            idx++;
        });
        CustomerControl.RemoveOldParentElement();
        setTimeout(function () {
            CustomerControl.RenameParentsElement();
        }, 300);
    },
    hide: function (e) {
        $(this).fadeOut(e);
        setTimeout(function () {
            $(this).remove();

            var idx = 0;
            $(".rpt-item-container").each(function () {
                $(this).attr("data-idx", idx);
                idx++;
            });

            CustomerControl.RenameParentsElement();
        }, 500);
    }
});

$(function () {
    $(".rpt-item-container").each(function () {
        var ct = $(this);
        CustomerControl.CustomersRptTypeAhead(ct);
    });
    setTimeout(function () {
        CustomerControl.RenameParentsElement();
    }, 300);
});