﻿var CustomerControl = {
    LevelChangeEvent: function () {
        var type = $("select.agency-type-select").val();
        var customerId = $("#CustomerId").val();
        if (type == 1) {
            $("#selectSuperior").val(0);
            $("#selectSuperior").attr("disabled", true);
            $("#selectSuperior").selectpicker("refresh");
        } else {
            setTimeout(function () {
                $.aGet(`/Customer/GetSuperiors?type=${type}&customerId=${customerId}`, {}, function (result) {
                    if (result.success) {
                        if (result.html) {
                            $("#selectSuperior").parent().remove();
                            $(".superior-select").append(result.html);
                            $("#selectSuperior").selectpicker();
                        }
                    }
                }, "json", false); 
            }, 300);               
        }      
    },

    UpdateInit: function () {
        var type = $("select.agency-type-select").val();
        var customerId = $("#CustomerId").val();
        if (type == 1) {
            $("#selectSuperior").val(0);
            $("#selectSuperior").attr("disabled", true);
            $("#selectSuperior").selectpicker("refresh");
        } else {
            var selectSuperiorVal = $("#selectSuperior").val();
            setTimeout(function () {
                $.aGet(`/Customer/GetSuperiors?type=${type}&customerId=${customerId}`, {}, function (result) {
                    if (result.success) {
                        if (result.html) {
                            $("#selectSuperior").parent().remove();
                            $(".superior-select").append(result.html);
                            $("#selectSuperior").val(selectSuperiorVal);
                            $("#selectSuperior").selectpicker("refresh");
                        }
                    }
                }, "json", false);
            }, 300);             
        }
    }
}

$(function () {
    $("select.agency-type-select").on("change", function () {
        CustomerControl.LevelChangeEvent();
    })

    CustomerControl.UpdateInit();
})