﻿using System;

using Owin;
using Microsoft.Owin;
using Autofac;
using System.Reflection;

using Manager.WebApp.Settings;
using Manager.WebApp.Helpers;

using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNet.SignalR.Client;

[assembly: OwinStartup(typeof(Manager.WebApp.Startup))]
namespace Manager.WebApp
{
    public partial class Startup
    {
        private static IContainer _container;
        public static IContainer AutofacContainer { get { return _container; } }

        public void Configuration(IAppBuilder app)
        {
            //Initialize autofac container
            InitAutofacContainer(app);

            ConfigureForOwinStartup(app);

            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateIssuerSigningKey = true,
                        //ValidIssuer = "http://mysite.com", //some string, normally web url,
                        //ValidAudience = "http://mysite.com",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SystemSettings.ApiSecretKey))
                    }
                });
        }

        private void InitAutofacContainer(IAppBuilder app)
        {
            ContainerBuilder builder = new ContainerBuilder();

            // Register all Autofact Modules (OWin, WebApi, WebMvc, ....)
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            // You can  register all dependencies here, normally we define in modules

            //then
            _container = builder.Build();


            //Middlewares: Enabling DI for OWIN middleware: 
            //should be the first middleware added to IAppBuilder

            // Register the Autofac middleware FIRST. This also adds
            // Autofac-injected middleware registered with the container.
            //app.UseAutofacMiddleware(_container);

            // ...then register your other middleware not registered
            // with Autofac.
        }

        //public sealed class SignalrConnection
        //{
        //    // Because Singleton's constructor is private, we must explicitly
        //    // give the Lazy<Singleton> a delegate for creating the Singleton.
        //    private static readonly Lazy<SignalrConnection> instanceHolder =
        //        new Lazy<SignalrConnection>(() => new SignalrConnection());

        //    HubConnection myCon = null;
        //    IHubProxy myHub = null;
        //    bool connected = false;
        //    public static string _hubName = string.Empty;
        //    private SignalrConnection()
        //    {
        //        InitSignalrConnection(_hubName);

        //        myCon.Closed += () =>
        //        {
        //            connected = false;
        //            while (!connected)
        //            {
        //                System.Threading.Thread.Sleep(1000);

        //                InitSignalrConnection(_hubName);

        //                myCon.Start().Wait();
        //                connected = true;
        //            }
        //        };

        //        myCon.Start().Wait();
        //    }

        //    private void InitSignalrConnection(string hubName = "ManagerHub")
        //    {
        //        var signalRServerUrl = MyCloudSettings.MyCloudServer;
        //        myCon = new HubConnection(signalRServerUrl);
        //        myHub = myCon.CreateHubProxy(hubName);
        //    }


        //    public HubConnection SinalrMyConnection { get { return myCon; } }
        //    public IHubProxy SignalrMyHub { get { return myHub; } }

        //    public static SignalrConnection Instance
        //    {
        //        get { return instanceHolder.Value; }
        //    }
        //}
    }
}