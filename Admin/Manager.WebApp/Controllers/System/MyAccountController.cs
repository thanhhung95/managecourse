﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Manager.WebApp.Models;
using Manager.DataLayer;
using Manager.WebApp.Helpers;
using System.Configuration;
using Manager.SharedLibs.Logging;
using System.Security.Claims;
using Manager.WebApp.Resources;
using Manager.WebApp.Services;
using Newtonsoft.Json;
using Manager.SharedLibs;
using Manager.WebApp.Settings;
using System.Text.RegularExpressions;
using System.Reflection;
using System.IO;
using Autofac;
using Manager.DataLayer.Stores;
using System.Net;

namespace Manager.WebApp.Controllers
{        
    public class MyAccountController : BaseAuthedController
    {
        IFrontEndSettingsService _frontSettingService;
        private readonly ILog logger = LogProvider.For<MyAccountController>();

        private readonly IIdentityStore _identityStore;
        private readonly IActivityStore _activityStore;

        public MyAccountController(IIdentityStore identityStore, IFrontEndSettingsService settingService)
        {
            _identityStore = identityStore;
            _frontSettingService = settingService;
        }

        public MyAccountController(ApplicationUserManager userManager, IIdentityStore identityStore, IFrontEndSettingsService settingService)
        {
            UserManager = userManager;
            _identityStore = identityStore;
            _frontSettingService = settingService;
        }


        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
      
        private SignInHelper _helper;

        private SignInHelper SignInHelper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new SignInHelper(UserManager, AuthenticationManager);
                }
                return _helper;
            }
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        #endregion

        //[AccessRoleChecker]
        public ActionResult Profile()
        {
            AccountDetailViewModel model = new AccountDetailViewModel();
            string currentUser = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUser))
                return RedirectToAction("Account", "Login");
            try
            {
                var userInfo = GetCurrentUser();
                var _userRoles = UserManager.GetRoles(currentUser);

                if (_userRoles != null)
                {
                    model.RolesList = _userRoles.ToList();
                }
                model.Id = userInfo.Id;
                model.UserName = userInfo.UserName;
                model.PhoneNumber = userInfo.PhoneNumber;
                model.Email = userInfo.Email;
                model.CreatedDateUtc = userInfo.CreatedDateUtc;
                model.Avatar = userInfo.Avatar;
                model.FullName = userInfo.FullName;
            }
            catch 
            {

            }            
             
            //Get newest activity
            try
            {
                int currentPage = 1;
                //Limit activity on once query.
                int pageSize = int.Parse(ConfigurationManager.AppSettings["Paging:PageSize"]);
                int total = 0;

                model.ActivityNews = _activityStore.GetActivityLogByUserId(currentUser, currentPage, pageSize);
                total = _activityStore.CountAllActivityLogByUserId(currentUser);

                model.ActivityPagingInfo = new PagingInfo { 
                    CurrentPage = currentPage,
                    //PageNo = (int)(total / pageSize),
                    PageNo = (total + pageSize - 1) / pageSize,
                    PageSize = pageSize,
                    Total = total
                };

                if (model.ActivityNews != null && model.ActivityNews.Count > 0)
                {
                    foreach (var record in model.ActivityNews)
                    {
                        //Calculate time
                        record.FriendlyRelativeTime = DateTimeHelper.GetFriendlyRelativeTime(record.ActivityDate);
                    }
                }
            }
            catch
            {
            }            

            return View(model);
        }

        [AllowAnonymous]
        public JsonResult GetActivityLogs(string page)
        {
            List<ActivityLog> list = new List<ActivityLog>();
            string currentUser = System.Web.HttpContext.Current.User.Identity.GetUserId();
            try
            {
                list = _activityStore.GetActivityLogByUserId(currentUser, Convert.ToInt32(page), AdminSettings.PageSize);
                if (list != null && list.Count > 0)
                {
                    foreach (var record in list)
                    {
                        //Calculate time
                        record.FriendlyRelativeTime = DateTimeHelper.GetFriendlyRelativeTime(record.ActivityDate);
                    }
                }
            }
            catch
            {
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        //Show change password form
        //[AccessRoleChecker]
        public ActionResult ChangePassword()
        {
            string currentUser = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUser))
                return RedirectToAction("Login","Account");

            AccountChangePasswordViewModel model = new AccountChangePasswordViewModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[AccessRoleChecker]
        public ActionResult ChangePassword(AccountChangePasswordViewModel model)
        {
            string currentUser = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUser))
                return RedirectToAction("Account", "Login");

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.OldPassword = Utility.Md5HashingData(model.OldPassword);
            model.NewPassword = Utility.Md5HashingData(model.NewPassword);

            //Change password
            var result = UserManager.ChangePassword(currentUser, model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                this.AddNotification(ManagerResource.LB_CHANGE_PWD_SUCCESS, NotificationType.SUCCESS);
                return RedirectToAction("Profile", "MyAccount");
            }
            
            AddErrors(result);
            return View();
        }

        public ActionResult UpdateProfile()
        {
            var model = new UpdateUserProfileModel();            
            try
            {
                if (Request.IsAuthenticated)
                {
                    model.UserId = User.Identity.GetUserId();
                    model.UserName = User.Identity.GetUserName();

                    var claims = (ClaimsIdentity)User.Identity;
                    model.FullName = claims.FindFirstValue(ClaimKeys.FullName);
                    model.Avatar = claims.FindFirstValue(ClaimKeys.Avatar);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification("System is busy now. Please try again later", NotificationType.ERROR);
                var strError = string.Format("Could not open page UpdateProfile because: {0}", ex.ToString());
                logger.Error(strError);

                return View(model);
            }

            return View(model);
        }

        [ActionName("UpdateProfile")]
        [HttpPost]
        public ActionResult UpdateProfile_Post(UpdateUserProfileModel model)
        {
            try
            {
                var currentIdentity = HttpContext.User.Identity as ClaimsIdentity;

                var avatarClaim = currentIdentity.FindFirst(ClaimKeys.Avatar);
                if(avatarClaim != null)
                {
                    currentIdentity.RemoveClaim(avatarClaim);
                    UserManager.RemoveClaim(model.UserId, avatarClaim);
                }

                currentIdentity.AddClaim(new Claim(ClaimKeys.Avatar, model.Avatar));
                UserManager.AddClaim(model.UserId, new Claim(ClaimKeys.Avatar, model.Avatar));

                var authenticationManager = System.Web.HttpContext.Current.GetOwinContext().Authentication;
                authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(currentIdentity), new AuthenticationProperties() { IsPersistent = true });                

                this.AddNotification("Update profile successfully", NotificationType.SUCCESS);
            }
            catch (Exception ex)
            {
                this.AddNotification("System is busy now. Please try again later", NotificationType.ERROR);
                var strError = string.Format("Could not UpdateProfile because: {0}", ex.ToString());
                logger.Error(strError);

                return View(model);
            }

            return View(model);
        }

        protected HttpPostedFile ConstructHttpPostedFile(byte[] data, string filename, string contentType)
        {
            // Get the System.Web assembly reference
            Assembly systemWebAssembly = typeof(HttpPostedFileBase).Assembly;
            // Get the types of the two internal types we need
            Type typeHttpRawUploadedContent = systemWebAssembly.GetType("System.Web.HttpRawUploadedContent");
            Type typeHttpInputStream = systemWebAssembly.GetType("System.Web.HttpInputStream");

            // Prepare the signatures of the constructors we want.
            Type[] uploadedParams = { typeof(int), typeof(int) };
            Type[] streamParams = { typeHttpRawUploadedContent, typeof(int), typeof(int) };
            Type[] parameters = { typeof(string), typeof(string), typeHttpInputStream };

            // Create an HttpRawUploadedContent instance
            object uploadedContent = typeHttpRawUploadedContent
              .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, uploadedParams, null)
              .Invoke(new object[] { data.Length, data.Length });

            // Call the AddBytes method
            typeHttpRawUploadedContent
              .GetMethod("AddBytes", BindingFlags.NonPublic | BindingFlags.Instance)
              .Invoke(uploadedContent, new object[] { data, 0, data.Length });

            // This is necessary if you will be using the returned content (ie to Save)
            typeHttpRawUploadedContent
              .GetMethod("DoneAddingBytes", BindingFlags.NonPublic | BindingFlags.Instance)
              .Invoke(uploadedContent, null);

            // Create an HttpInputStream instance
            object stream = (Stream)typeHttpInputStream
              .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, streamParams, null)
              .Invoke(new object[] { uploadedContent, 0, data.Length });

            // Create an HttpPostedFile instance
            HttpPostedFile postedFile = (HttpPostedFile)typeof(HttpPostedFile)
              .GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, parameters, null)
              .Invoke(new object[] { filename, contentType, stream });

            return postedFile;
        }

        [HttpPost]
        [VerifyLoggedInUser]
        public ActionResult UploadAvatar(FormCollection collection)
        {
            var isSuccess = false;
            var msg = string.Empty;
            var fileName = string.Empty;
            var contentType = "image/jpeg";
            try
            {
                var currentUser = GetCurrentUser();
                var staff_id = currentUser.StaffId;
                var agency_id = currentUser.ParentId > 0 ? currentUser.ParentId : currentUser.StaffId;
                var oldPath = currentUser.Avatar;

                var slimStrData = string.Empty;
                var changed = string.Empty;
                SlimObject slimObj = null;
                if (collection != null && collection["slim[]"] != null)
                    slimStrData = collection["slim[]"].ToString();

                if (!string.IsNullOrEmpty(slimStrData))
                {
                    slimObj = JsonConvert.DeserializeObject<SlimObject>(slimStrData);
                }

                if (slimObj == null)
                {
                    return Json(new { success = isSuccess, message = ManagerResource.COMMON_ERROR_DATA_INVALID }, JsonRequestBehavior.AllowGet);
                }

                if (collection["ChangingDetected"] != null)
                    changed = collection["ChangingDetected"].ToString();

                if (string.IsNullOrEmpty(changed))
                {
                    isSuccess = true;
                    msg = ManagerResource.LB_UPDATE_SUCCESS;

                    return Json(new { success = true, message = msg }, JsonRequestBehavior.AllowGet);
                }

                if (slimObj.input != null && slimObj.input != null)
                {
                    if (!string.IsNullOrEmpty(slimObj.input.name))
                    {
                        var ext = System.IO.Path.GetExtension(slimObj.input.name);
                        fileName = Utility.Md5HashingData(slimObj.input.name) + ext;
                    }
                    else
                        fileName = string.Format("{0}_{1}", staff_id, EpochTime.GetIntDate(DateTime.Now));

                    if (!string.IsNullOrEmpty(slimObj.input.type))
                        contentType = slimObj.input.type;
                }

                if (slimObj.output != null && !string.IsNullOrEmpty(slimObj.output.image))
                {
                    Regex r = new Regex("data:image/\\w+;base64,");
                    var imageBase64Str = r.Replace(slimObj.output.image, string.Empty);
                    var imgDataBytes = Convert.FromBase64String(imageBase64Str);
                    if (imgDataBytes != null)
                    {
                        var postedFile = ConstructHttpPostedFile(imgDataBytes, fileName, contentType);
                        if (postedFile != null)
                        {
                            var newPath = FileUploadHelper.UploadPostedFile(postedFile, string.Format("Avatars/{0}", currentUser.StaffId));
                            if (!string.IsNullOrEmpty(newPath))
                            {
                                currentUser.Avatar = newPath;

                                var store = GlobalContainer.IocContainer.Resolve<IStoreUser>();
                                store.UpdateAvatar(currentUser);

                                AccountHelper.ClearUserCache(currentUser);
                                try
                                {
                                    if (!string.IsNullOrEmpty(oldPath))
                                    {
                                        var delPath = Server.MapPath("~/Media") + "/" + oldPath;
                                        System.IO.File.Delete(delPath);
                                    }
                                        
                                }
                                catch (Exception ex)
                                {
                                    logger.ErrorFormat("Failed when Delete old image because: {0}", ex.ToString());

                                }

                                return Json(new { success = true, message = ManagerResource.LB_UPDATE_SUCCESS }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            logger.Error("Failed to get Upload image because: The image is null");
                        }
                    }
                }

                return Json(new { success = true, message = ManagerResource.LB_UPDATE_SUCCESS }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                var strError = string.Format("Failed when trying UploadAvatar because: {0}", ex.ToString());
                logger.Error(strError);
                msg = ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT;
            }

            return Json(new { success = isSuccess, message = msg }, JsonRequestBehavior.AllowGet);
        }
    }

    public class SlimInput
    {
        public string name { get; set; }
        public string type { get; set; }
        public string size { get; set; }
        public string width { get; set; }
        public string height { get; set; }
    }
    public class SlimOutput
    {
        public string width { get; set; }
        public string height { get; set; }
        public string image { get; set; }
    }
    public class SlimCrop
    {
        public string x { get; set; }
        public string y { get; set; }
        public string height { get; set; }
        public string width { get; set; }
        public string type { get; set; }
    }
    public class SlimSize
    {
        public string width { get; set; }
        public string height { get; set; }
    }

    public class SlimActions
    {
        public SlimCrop crop { get; set; }
        public SlimSize size { get; set; }
    }

    public class SlimObject
    {
        public object server { get; set; }
        public object meta { get; set; }
        public SlimInput input { get; set; }
        public SlimOutput output { get; set; }
        public SlimActions actions { get; set; }
    }
}