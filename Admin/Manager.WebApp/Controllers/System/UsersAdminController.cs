﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Manager.DataLayer;
using Manager.WebApp.Models;
using Manager.WebApp.Helpers;
using System.Configuration;
using System.Net;
using Manager.WebApp.Resources;
using Manager.SharedLibs.Logging;

using Manager.DataLayer.Repositories;
using Manager.SharedLibs;
using Manager.WebApp.Services;
using System.Collections.Generic;
using Newtonsoft.Json;
using SelectItem = System.Web.Mvc.SelectListItem;
using Manager.WebApp.Caching;

namespace Manager.WebApp.Controllers
{
    public class UsersAdminController : BaseAuthedController
    {
        private readonly IIdentityStore _identityStore;
        private readonly ILog logger = LogProvider.For<UsersAdminController>();
        public UsersAdminController(IIdentityStore identityStore)
        {
            _identityStore = identityStore;

            //Clear cache
            CachingHelpers.ClearUserCache();

            UserCookieManager.ClearCookie(UserCookieManager.GetVersionToken());
        }

        public UsersAdminController(ApplicationUserManager userManager, ApplicationRoleManager roleManager, IIdentityStore identityStore)
        {
            UserManager = userManager;
            RoleManager = roleManager;

            _identityStore = identityStore;
            //Clear cache
            CachingHelpers.ClearUserCache();

        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /UsersAdmin/
        [AccessRoleChecker]
        public ActionResult Index(UserViewModel model)
        {
            model = GetDefaultFilterModel(model);
            try
            {
                var agencyId = GetCurrentAgencyId();
                var roles = new RoleRepository().GetRoleByAgencyId(agencyId);

                if (roles.HasData())
                {
                    model.RoleList = roles.Select(x => new SelectItem()
                    {
                        Text = x.Name,
                        Value = x.Id
                    });
                }

                var isLocked = Convert.ToBoolean(model.IsLocked);

                model.SearchResult = _identityStore.FilterUserList(model.Keyword, agencyId, model.RoleId, isLocked, model.CurrentPage, model.PageSize);
                model.Total = _identityStore.CountAll(model.Email, model.RoleId, isLocked, agencyId);

                model.PageNo = (int)(model.Total / model.PageSize);

                if (model.SearchResult != null && model.SearchResult.Count > 0)
                {
                    foreach (var record in model.SearchResult)
                    {
                        var _userRoles = new RoleRepository().GetRoleByUserId(record.Id);                        
                        if (_userRoles.HasData())
                        {
                            record.Roles = _userRoles.Select(x=>x.Name).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Could not display UserAdmin page because: {0}", ex.ToString());
            }

            return View(model);
        }

        // GET: /Users/Details/5
        [AccessRoleChecker]
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var detailsModel = new UserDetailsViewModel();
            try
            {
                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return RedirectToErrorPage();
                }

                ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);
                detailsModel.User = user;
                detailsModel.Lockout = new LockoutViewModel();
                var isLocked = await UserManager.IsLockedOutAsync(user.Id);
                detailsModel.Lockout.Status = isLocked ? LockoutStatus.Locked : LockoutStatus.Unlocked;

                var userInfo = _identityStore.GetUserByID(user.Id);

                if (detailsModel.Lockout.Status == LockoutStatus.Locked)
                {
                    detailsModel.Lockout.LockoutEndDate = (await UserManager.GetLockoutEndDateAsync(user.Id)).DateTime;
                }

                var claims = await UserManager.GetClaimsAsync(user.Id);
                if (userInfo != null)
                {
                    detailsModel.FullName = userInfo.FullName;
                }

                detailsModel.Claims = claims.ToList();
                detailsModel.Email = Request["Email"];
                detailsModel.RoleId = Request["RoleId"];
                detailsModel.SearchExec = Request["SearchExec"];
                detailsModel.Page = Request["Page"];
                detailsModel.IsLocked = Convert.ToInt32(Request["IsLocked"]);
            }
            catch
            {
                return RedirectToErrorPage();
            }

            return View(detailsModel);
        }

        //
        // GET: /Users/Create
        [AccessRoleChecker(AgencyRequired = true)]
        public ActionResult Create()
        {
            var model = new RegisterViewModel();
            model.StaffId = GetCurrentStaffId();
            //model.Companies = new List<IdentityCompany>();
            try
            {
                //Get the list of Roles
                // ViewBag.RoleId = new SelectList(RoleManager.Roles.ToList(), "Name", "Name");
                model.RolesList = new RoleRepository(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString).GetRoleByUserId(GetCurrentAgencyId()).OrderBy(m => m.Name).ToList().Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Name
                });
            }
            catch (Exception ex)
            {

                throw;
            }

            return View(model);
        }


        //
        // POST: /Users/Create
        [HttpPost]
        [AccessRoleChecker]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                userViewModel.Password = Utility.Md5HashingData(userViewModel.Password);
                int parentId = GetCurrentAgencyId();
                if (parentId == 0)
                {
                    GetCurrentStaffId();
                }
                var user = new ApplicationUser { UserName = userViewModel.UserName, Email = userViewModel.UserName, FullName = userViewModel.FullName, EmailConfirmed = true, ParentId = parentId };

                if (!string.IsNullOrEmpty(user.UserName))
                {
                    var existedUser = await UserManager.FindByNameAsync(user.UserName);
                    if (existedUser != null)
                    {
                        if (existedUser != null)
                        {
                            this.AddNotification(ManagerResource.ERROR_ACCOUNT_DUPLICATED, NotificationType.ERROR);
                            userViewModel.RolesList = new RoleRepository(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString).GetRoleByUserId(GetCurrentAgencyId()).OrderBy(m => m.Name).ToList().Select(x => new SelectListItem()
                            {
                                Text = x.Name,
                                Value = x.Name
                            });
                            return View();
                        }
                    }
                }

                var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);

                //Clear cache
                CachingHelpers.ClearUserCache();

                UserCookieManager.ClearCookie(UserCookieManager.GetVersionToken());

                //Add User to the selected Roles 
                if (adminresult.Succeeded)
                {
                    _identityStore.UpdateProfile(new Manager.DataLayer.IdentityUser { FullName = user.FullName, Id = user.Id });

                    var userDetail = _identityStore.GetUserByID(user.Id);
                    //if (userDetail != null)
                    //{
                    //    var apiModel = new ApiCompanyUpdateCompanyModel
                    //    {
                    //        agency_id = userDetail.StaffId,
                    //        ListCompanyId = userViewModel.selectCompany,
                    //        agency_parent_id = GetCurrentAgencyId()
                    //    };
                    //    var result = AgencyCompanyServices.UpdateCompanyAsync(apiModel).Result;
                    //}

                    if (!string.IsNullOrEmpty(userViewModel.Role))
                    {
                        selectedRole = new string[] { userViewModel.Role };
                        var result = await UserManager.AddUserToRolesAsync(user.Id, selectedRole);
                        if (!result.Succeeded)
                        {
                            //ModelState.AddModelError("", result.Errors.First());
                            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                            this.AddNotification(result.Errors.First(), NotificationType.ERROR);

                            userViewModel.RolesList = new RoleRepository(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString).GetRoleByUserId(GetCurrentAgencyId()).OrderBy(m => m.Name).ToList().Select(x => new SelectListItem()
                            {
                                Text = x.Name,
                                Value = x.Name
                            });
                            return View();
                        }
                    }
                }
                else
                {
                    //ModelState.AddModelError("", adminresult.Errors.First());

                    this.AddNotification(adminresult.Errors.First(), NotificationType.ERROR);
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    userViewModel.RolesList = new RoleRepository(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString).GetRoleByUserId(GetCurrentAgencyId()).OrderBy(m => m.Name).ToList().Select(x => new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Name
                    });

                    return View();

                }
                //return RedirectToAction("Edit/" + user.Id);
                this.AddNotification(ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);

                return RedirectToAction("Index");
            }
            //ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        public async Task<ActionResult> Lock(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            //var helper = new IdentityModelHelper(UserManager, RoleManager);
            //var model = await helper.GetUserDetailsViewModel(id);
            try
            {
                var result = await UserManager.LockUserAccount(id, 1000 * 1000);
                if (!result.Succeeded)
                {
                    AddErrors(result.Errors);
                }
                else
                {
                    //Clear cache
                    CachingHelpers.ClearUserCache();
                    CachingHelpers.ClearUserCache(id);

                    UserCookieManager.ClearCookie(UserCookieManager.GetVersionToken());

                    //model = await helper.GetUserDetailsViewModel(id);
                    return RedirectToAction("Details/" + id, "UsersAdmin");
                }
            }
            catch (Exception ex)
            {
                AddError(ex);
            }
            //ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);
            return View();
        }

        public async Task<ActionResult> Unlock(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            //var helper = new IdentityModelHelper(UserManager, RoleManager);
            //var model = await helper.GetUserDetailsViewModel(id);
            try
            {
                var result = await UserManager.UnlockUserAccount(id);
                if (!result.Succeeded)
                {
                    AddErrors(result.Errors);
                }
                else
                {
                    //Clear cache
                    CachingHelpers.ClearUserCache();
                    CachingHelpers.ClearUserCache(id);

                    return RedirectToAction("Details/" + id, "UsersAdmin");
                }
            }
            catch (Exception ex)
            {
                AddError(ex);
            }
            //ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);
            return View();
        }

        //
        // GET: /Users/Edit/1
        [AccessRoleChecker]
        public async Task<ActionResult> Edit(string id)
        {
            var model = new EditUserViewModel();
            
            try
            {
                var agencyId = GetCurrentAgencyId();               
                if (string.IsNullOrEmpty(id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User id was not provided");
                }

                var allRoles = new RoleRepository().GetRoleByAgencyId(agencyId);


                var applicationUser = new RpsUser().GetById(id);
                if (applicationUser == null)
                {
                    return RedirectToErrorPage();
                }

                model.Lockout = new LockoutViewModel();
                var isLocked = applicationUser.LockoutEnabled;
                model.Lockout.Status = isLocked ? LockoutStatus.Locked : LockoutStatus.Unlocked;
                model.IsActived = !isLocked;
                if (model.Lockout.Status == LockoutStatus.Locked)
                {
                    model.Lockout.LockoutEndDate = (await UserManager.GetLockoutEndDateAsync(applicationUser.Id)).DateTime;
                }               

                model.Id = applicationUser.Id;
                model.UserName = applicationUser.UserName;
                model.FullName = applicationUser.FullName;

                var userRoles = new RoleRepository().GetRoleByUserId(applicationUser.Id);
                var hasRoles = userRoles.HasData();

                model.RolesList = new List<SelectItem>();
                if (allRoles.HasData())
                {                    
                    foreach (var r in allRoles)
                    {
                        var item = new SelectListItem();
                        item.Text = r.Name;
                        item.Value = r.Id;
                        if (hasRoles)
                        {
                            item.Selected = userRoles.Exists(x => x.Id == r.Id);
                            if(item.Selected)
                                model.Role = r.Id;
                        }

                        model.RolesList.Add(item);
                    }
                }

                model.SEmail = Request["Email"];
                model.SRoleId = Request["RoleId"];
                model.SearchExec = Request["SearchExec"];
                model.Page = Request["Page"];
                model.SIsLocked = Convert.ToInt32(Request["IsLocked"]);

                return View(model);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Could not display EditUser page because: {0}", ex.ToString());

                return RedirectToErrorPage();
            }
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AccessRoleChecker]
        public async Task<ActionResult> Edit([Bind(Include = "UserName,Id,FullName,selectCompany,IsActived,Role")] EditUserViewModel editUser, params string[] selectedRole)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new RpsUser().GetById(editUser.Id);
                    if (user == null)
                    {
                        return HttpNotFound();
                    }

                    user.UserName = editUser.UserName;
                    user.FullName = editUser.FullName;

                    user.StaffId = GetCurrentStaffId();
                    user.ParentId = GetCurrentAgencyId();
                    if (user.ParentId == 0) user.ParentId = GetCurrentStaffId();

                    if (!string.IsNullOrEmpty(user.UserName))
                    {
                        var existedUser = await UserManager.FindByNameAsync(user.UserName);
                        if (existedUser != null)
                        {
                            if (editUser.Id != existedUser.Id)
                            {
                                this.AddNotification(ManagerResource.ERROR_ACCOUNT_DUPLICATED, NotificationType.ERROR);
                                return RedirectToAction("Edit/" + editUser.Id);
                            }
                        }
                    }
                    if (!editUser.IsActived && !user.LockoutEnabled)
                    {
                        user.LockoutEndDateUtc = DateTime.UtcNow;
                        user.LockoutEnabled = true;
                        //var resultLock = await UserManager.LockUserAccount(editUser.Id, 1000 * 1000);
                    }
                    if (editUser.IsActived && user.LockoutEnabled)
                    {
                        user.LockoutEndDateUtc = DateTime.UtcNow;
                        user.LockoutEnabled = false;
                        //var unLock = await UserManager.UnlockUserAccount(editUser.Id);
                    }

                    user.Status = editUser.IsActived ? 1 : 0;

                    //Update user info
                    new RpsUser().Update(user);

                    var userRoles = new RoleRepository().GetRoleByUserId(editUser.Id);
                    var rolesSelected = new List<IdentityRole>();

                    if (!string.IsNullOrEmpty(editUser.Role))
                    {
                        selectedRole = new string[] { editUser.Role };
                        if (selectedRole.HasData())
                        {
                            foreach (var item in selectedRole)
                            {
                                var r = new IdentityRole();
                                r.Id = item;

                                rolesSelected.Add(r);
                            }

                            if (rolesSelected.HasData())
                            {
                                //Add to roles
                                var roleResult = new RpsUser().AddToRoles(user.Id, rolesSelected);
                            }
                        }
                    }

                    //Clear cache
                    //CachingHelpers.ClearUserCache();
                    MenuHelper.ClearUserMenuCache(editUser.Id);
                    CachingHelpers.ClearUserCache(editUser.Id);
                    PermissionHelper.ClearPermissionsCache(editUser.Id);
                    UserCookieManager.ClearCookie(UserCookieManager.GetVersionToken());

                    this.AddNotification(ManagerResource.LB_UPDATE_SUCCESS, NotificationType.SUCCESS);
                    //return RedirectToAction("Edit/" + editUser.Id);

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Could not Update User info because: {0}", ex.ToString()));
            }

            this.AddNotificationModelStateErrors(ModelState);
            return RedirectToAction("Edit/" + editUser.Id);
        }

        //Show popup confirm delete        
        public ActionResult DeleteUser(string id)
        {
            if (id == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserViewModel record = new UserViewModel();
            record.UserInfoViewModel = new Manager.DataLayer.IdentityUser();
            record.UserInfoViewModel.Id = id;

            return PartialView("_DeleteUserInfo", record);
        }

        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        [AccessRoleChecker]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                if (id == null)
                {
                    return Json(new { success = false, message = ManagerResource.LB_ERROR_OCCURED });
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return Json(new { success = false, message = ManagerResource.LB_ERROR_OCCURED });
                }

                var result = await UserManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    //Clear cache
                    CachingHelpers.ClearUserCache();
                    CachingHelpers.ClearUserCache(id);

                    UserCookieManager.ClearCookie(UserCookieManager.GetVersionToken());

                    //return Json(new { success = true });
                    return Json(new { success = true, message = ManagerResource.LB_DELETE_SUCCESS, clientcallback = "location.reload();" });
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Failed when delete user because: {0}", ex.ToString()));
            }

            return Json(new { success = false, message = ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT });
        }

        //Show popup confirm reset password        
        public ActionResult ResetPassword(string id)
        {
            if (id == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserViewModel record = new UserViewModel();
            record.UserInfoViewModel = new Manager.DataLayer.IdentityUser();
            record.UserInfoViewModel.Id = id;

            return PartialView("_ConfirmResetPwd", record);
        }

        [HttpPost, ActionName("ResetPassword")]
        [ValidateAntiForgeryToken]
        [AccessRoleChecker]
        public async Task<ActionResult> ResetPwd(string id)
        {
            if (id == null)
            {
                return Json(new { success = false, message = ManagerResource.LB_ERROR_OCCURED });
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return Json(new { success = false, message = ManagerResource.LB_ERROR_OCCURED });
            }

            try
            {
                var removePassword = UserManager.RemovePassword(id);
                string defaultPassword = ConfigurationManager.AppSettings["System:UserDefaultPassword"];
                if (removePassword.Succeeded)
                {
                    user.PasswordHash = Utility.Md5HashingData(UserManager.PasswordHasher.HashPassword(defaultPassword));
                    var result = await UserManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return Json(new { success = true, message = ManagerResource.LB_PASSWORD_RESET_SUCCESS + ": " + defaultPassword });
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Reset password failed, reason: {0}", ex.Message));
            }

            return Json(new { success = false, message = ManagerResource.COMMON_ERROR_EXTERNALSERVICE_TIMEOUT });
        }
    }
}