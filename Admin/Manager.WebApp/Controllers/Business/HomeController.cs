﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Helpers;
using Manager.WebApp.Models;
using Manager.WebApp.Resources;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Manager.WebApp.Controllers
{
    public class HomeController : BaseAuthedController
    {
        private readonly IStoreClassSchedule _mainStore;

        private readonly ILog logger = LogProvider.For<HomeController>();

        public HomeController(IStoreClassSchedule mainStore)
        {
            _mainStore = mainStore;
        }

        /// <summary>
        /// Display Home 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AccessRoleChecker]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get Schedule by Filter
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetSchedule(ManageHomeModel model)
        {
            var strError = string.Empty;
            JsonResult result = new JsonResult();

            var filter = new IdentityClassSchedule();
            filter.TeacherId = model.TeacherId != 0 ? model.TeacherId : 0;

            try
            {
                var SearchResults = new List<HomeUpdateModel>();
                var listData = _mainStore.GetListByFilter(filter);

                if (listData.HasData())
                {
                    foreach (var item in listData)
                    {
                        var info = HelperClassSchedule.GetBaseInfo(item.Id);
                        if (info != null)
                        {
                            var schedule = info.MappingObject<HomeUpdateModel>();
                            var infoTeacher = HelperTeacher.GetBaseInfo(info.TeacherId);
                            var infoClass = HelperClass.GetBaseInfo(info.ClassId);

                            schedule.Teacher = (infoTeacher != null && infoTeacher.Id > 0) ? infoTeacher : null;
                            schedule.Class = (infoClass != null && infoClass.Id > 0) ? infoClass : null;

                            SearchResults.Add(schedule);
                        }
                    }
                }
                // Processing.  
                result = this.Json(SearchResults, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get schedule because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            // Return info.  
            return result;
        }

        /// <summary>
        /// Display popup Detail 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult DetailClassDaySchedule(ClassScheduleUpdateModel model)
        {
            var strError = string.Empty;

            var filter = new IdentityStudent
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var info = HelperClassSchedule.GetBaseInfo(model.Id);
                var _storeStudent = GlobalContainer.IocContainer.Resolve<IStoreStudent>();

                if (info != null && info.Id > 0)
                {
                    model = info.MappingObject<ClassScheduleUpdateModel>();

                    if (info.TeacherId != null && info.TeacherId > 0)
                    {
                        model.Teacher = HelperTeacher.GetBaseInfo(info.TeacherId);
                    }

                    var infoClass = HelperClass.GetBaseInfo(info.ClassId);

                    if (infoClass != null && infoClass.Id > 0)
                    {
                        model.Name = infoClass.Name;
                    }

                    var listStudentId = _storeStudent.GetListStudentByClassSchedule(filter, 1, 1000, info.ClassId, info.Id);

                    if (listStudentId.HasData())
                    {
                        model.SearchResults = listStudentId;
                        model.TotalCount = listStudentId[0].TotalCount;
                    }
                }

                return PartialView("_PopupDetails", model);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get DetailClassDaySchedule because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }
    }
}