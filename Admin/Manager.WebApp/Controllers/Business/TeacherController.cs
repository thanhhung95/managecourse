﻿using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Helpers;
using Manager.WebApp.Models;
using Manager.WebApp.Resources;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Manager.DataLayer.Repositories;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Manager.WebApp.Caching;
using Manager.DataLayer;
using Microsoft.AspNet.Identity.Owin;

namespace Manager.WebApp.Controllers.Business
{
    public class TeacherController : BaseAuthedController
    {
        private readonly IStoreTeacher _mainStore;
        private readonly IIdentityStore _identityStore;

        private readonly ILog logger = LogProvider.For<TeacherController>();

        public TeacherController(IStoreTeacher mainStore, IIdentityStore identityStore)
        {
            _mainStore = mainStore;

            _identityStore = identityStore;
        }

        public TeacherController(ApplicationUserManager userManager, ApplicationRoleManager roleManager, IIdentityStore identityStore)
        {
            UserManager = userManager;
            RoleManager = roleManager;

            _identityStore = identityStore;
            //Clear cache
            CachingHelpers.ClearUserCache();

        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        /// <summary>
        /// Display list teachers by paging
        /// </summary>
        /// <returns></returns>
        [AccessRoleChecker]
        public ActionResult Index(ManageTeacherModel model)
        {
            int currentPage = 1;
            int pageSize = (model.PageSize != 0 ? model.PageSize : SystemSettings.DefaultPageSize);

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityTeacher
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {

                var listData = _mainStore.GetByPage(filter, currentPage, pageSize);

                if (listData.HasData())
                {
                    model.SearchResults = new List<IdentityTeacher>();

                    foreach (var item in listData)
                    {
                        // Check cache
                        var info = HelperTeacher.GetBaseInfo(item.Id);
                        if (info != null)
                            model.SearchResults.Add(info);
                    }

                    model.TotalCount = listData[0].TotalCount;
                    model.CurrentPage = currentPage;
                    model.PageSize = pageSize;
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get index because: " + ex.ToString());

                return View(model);
            }

            return View(model);
        }

        /// <summary>
        /// Display form create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var model = new TeacherUpdateModel();

            return View(model);
        }

        /// <summary>
        /// Create Teacher
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(TeacherUpdateModel model)
        {
            var newId = 0;
            try
            {
                //Extract info
                var info = ExtractUpdateData(model);

                //random color
                var random = new Random();
                var color = String.Format("#{0:X6}", random.Next(0x1000000));

                var listTeacher = HelperTeacher.GetList();

                // check Duplicate
                var duplicateColor = listTeacher.Where(x => x.Color == color).FirstOrDefault();

                if (duplicateColor != null && duplicateColor.Id > 0)
                {
                    info.Color = String.Format("#{0:X6}", random.Next(0x1000000));
                }

                info.Color = color;

                // Insert Teacher
                newId = _mainStore.Insert(info);

                //Clear Cache List
                HelperTeacher.ClearCache(0);

                if (newId > 0)
                {
                    this.AddNotification(ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Teacher request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display Form Edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AccessRoleChecker]
        public ActionResult Edit(int id)
        {
            /*var model = new TeacherUpdateModel();
            try
            {
                var infoTeacher = HelperTeacher.GetBaseInfo(id);


                var agencyId = GetCurrentAgencyId();
                if (string.IsNullOrEmpty(infoTeacher.UserId))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User id was not provided");
                }

                var allRoles = new RoleRepository().GetRoleByAgencyId(agencyId);


                var applicationUser = new RpsUser().GetById(id);
                if (applicationUser == null)
                {
                    return RedirectToErrorPage();
                }

                model.Lockout = new LockoutViewModel();
                var isLocked = applicationUser.LockoutEnabled;
                model.Lockout.Status = isLocked ? LockoutStatus.Locked : LockoutStatus.Unlocked;
                model.IsActived = !isLocked;
                if (model.Lockout.Status == LockoutStatus.Locked)
                {
                    model.Lockout.LockoutEndDate = (await UserManager.GetLockoutEndDateAsync(applicationUser.Id)).DateTime;
                }

                model.UserName = applicationUser.UserName;

                var userRoles = new RoleRepository().GetRoleByUserId(applicationUser.Id);
                var hasRoles = userRoles.HasData();

                model.RolesList = new List<SelectItem>();
                if (allRoles.HasData())
                {
                    foreach (var r in allRoles)
                    {
                        var item = new SelectListItem();
                        item.Text = r.Name;
                        item.Value = r.Id;
                        if (hasRoles)
                        {
                            item.Selected = userRoles.Exists(x => x.Id == r.Id);
                            if (item.Selected)
                                model.Role = r.Id;
                        }

                        model.RolesList.Add(item);
                    }
                }

                model.SEmail = Request["Email"];
                model.SRoleId = Request["RoleId"];
                model.SearchExec = Request["SearchExec"];
                model.Page = Request["Page"];
                model.SIsLocked = Convert.ToInt32(Request["IsLocked"]);

                return View(model);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Could not display EditUser page because: {0}", ex.ToString());

                return RedirectToErrorPage();
            }*/

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                // Check cache 

                var info = HelperTeacher.GetBaseInfo(id);

                if (info == null)
                    return RedirectToErrorPage();
                //Render to view model
                var model = ParseUpdateDataForm(info);

                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed get Edit Teacher because: " + ex.ToString());
            }

            return View(new TeacherUpdateModel());
        }

        /// <summary>
        /// Edit Teacher
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(TeacherUpdateModel model)
        {
            try
            {
                //Extract data
                var info = ExtractUpdateData(model);

                // Update Teacher
                var isSuccess = _mainStore.Update(info);

                //Clear cache
                HelperTeacher.ClearCache(model.Id);

                if (isSuccess)
                {
                    this.AddNotification("更新完了！", NotificationType.SUCCESS);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Teacher request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display Popup Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            return PartialView("_PopupDelete", id);
        }

        /// <summary>
        /// Delete Teacher
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            var strError = string.Empty;
            try
            {
                // Delete Teacher
                _mainStore.Delete(id);

                //Clear cache
                HelperTeacher.ClearCache(id);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed for Delete Teacher request: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = "削除しました。", title = "完了！", clientcallback = "location.reload();" });
        }

        #region Helpers

        private TeacherUpdateModel ParseUpdateDataForm(IdentityTeacher data)
        {
            var model = data.MappingObject<TeacherUpdateModel>();
            return model;
        }

        private IdentityTeacher ExtractUpdateData(TeacherUpdateModel model)
        {
            var info        = model.MappingObject<IdentityTeacher>();
            info.Name       = info.Name.ToStringNormally();
            info.Address    = info.Address.ToStringNormally();
            info.Email      = info.Email.ToStringNormally();
            return info;
        }

        #endregion
    }
}