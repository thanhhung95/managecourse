﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Manager.WebApp.Helpers;
using Manager.WebApp.Settings;
using Manager.WebApp.Resources;
using Manager.SharedLibs.Logging;
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.WebApp.Models;

namespace Manager.WebApp.Controllers
{
    public class CurrencyController : BaseAuthedController
    {
        private readonly IStoreCurrency _mainStore;
        private readonly ILog logger = LogProvider.For<CurrencyController>();

        public CurrencyController(IStoreCurrency mainStore)
        {
            _mainStore = mainStore;
        }

        [AccessRoleChecker]
        public ActionResult Index(ManageCurrencyModel model)
        {
            int currentPage = 1;
            int pageSize = SystemSettings.DefaultPageSize;

            if (string.IsNullOrEmpty(model.SearchExec))
            {
                model.SearchExec = "Y";
                if (!ModelState.IsValid)
                {
                    ModelState.Clear();
                }
            }

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityCurrency
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                model.SearchResults = _mainStore.GetByPage(filter, currentPage, SystemSettings.DefaultPageSize);
                if (model.SearchResults != null && model.SearchResults.Count > 0)
                {
                    model.TotalCount = model.SearchResults[0].TotalCount;
                    model.CurrentPage = currentPage;
                    model.PageSize = pageSize;
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get data because: " + ex.ToString());

                return View(model);
            }

            return View(model);
        }

        //[AccessRoleChecker]
        public ActionResult Create()
        {
            var createModel = new CurrencyUpdateModel();
            createModel.Status = (int)EnumStatus.Activated;
            return View(createModel);
        }

        [HttpPost]
        //[AccessRoleChecker]
        public ActionResult Create(CurrencyUpdateModel model)
        {
            var newId = 0;
            if (!ModelState.IsValid)
            {
                string messages = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage + x.Exception));
                this.AddNotification(messages, NotificationType.ERROR);
                return View(model);
            }

            try
            {
                //Extract info
                var info = ExtractUpdateData(model);

                newId = _mainStore.Insert(info);

                //Clear cache
                HelperCurrency.ClearCache();
                if (newId > 0)
                {
                    this.AddNotification(ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Currency request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        //[AccessRoleChecker]
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                //Begin db transaction
                var info = _mainStore.GetById(id);

                if (info == null)
                    return RedirectToErrorPage();

                //Render to view model
                var editModel = ParseUpdateDataForm(info);

                return View(editModel);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Currency request: " + ex.ToString());
            }

            return View(new CurrencyUpdateModel());
        }

        [HttpPost]
        public ActionResult Edit(CurrencyUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                string messages = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage + x.Exception));
                this.AddNotification(messages, NotificationType.ERROR);
                return View(model);
            }

            try
            {
                //Extract data
                var info = ExtractUpdateData(model);

                var isSuccess = _mainStore.Update(info);

                //Clear cache
                HelperCurrency.ClearCache();

                if (isSuccess)
                {
                    this.AddNotification(ManagerResource.LB_UPDATE_SUCCESS, NotificationType.SUCCESS);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Currency request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        //Show popup confirm delete        
        ////[AccessRoleChecker]
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView("_PopupDelete", id);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[AccessRoleChecker]
        public ActionResult Delete_Confirm(int id)
        {
            var strError = string.Empty;
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            try
            {
                _mainStore.Delete(id);

                //Clear cache
                HelperCurrency.ClearCache();
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get Delete Currency because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = ManagerResource.LB_DELETE_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback="location.reload();" });
        }

        #region Helpers

        private CurrencyUpdateModel ParseUpdateDataForm(IdentityCurrency data)
        {
            var info = data.MappingObject<CurrencyUpdateModel>();
            return info;
        }

        private IdentityCurrency ExtractUpdateData(CurrencyUpdateModel model)
        {
            var info = model.MappingObject<IdentityCurrency>();
            info.Name = info.Name.ToStringNormally();
            info.Code = info.Code.ToStringNormally();
            return info;
        }

        #endregion

    }
}