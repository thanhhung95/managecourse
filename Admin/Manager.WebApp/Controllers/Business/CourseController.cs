﻿using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Helpers;
using Manager.WebApp.Models;
using Manager.WebApp.Resources;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Manager.WebApp.Controllers.Business
{
    public class CourseController : BaseAuthedController
    {
        private readonly IStoreCourse _mainStore;
        private readonly IStoreCourseSub _mainStoreCourseSub;
        private readonly ILog logger = LogProvider.For<CourseController>();

        public CourseController(
            IStoreCourse mainStore, 
            IStoreCourseSub mainStoreCourseSub
            )
        {
            _mainStore = mainStore;
            _mainStoreCourseSub = mainStoreCourseSub;
        }

        /// <summary>
        /// Display list Course by paging
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AccessRoleChecker]
        public ActionResult Index(ManageCourseModel model)
        {
            int currentPage = 1;
            int pageSize = (model.PageSize == 0 ? SystemSettings.DefaultPageSize : model.PageSize);

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityCourse
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var listData = _mainStore.GetByPage(filter, currentPage, pageSize);

                if (listData.HasData())
                {
                    model.SearchResults = new List<IdentityCourse>();

                    foreach (var item in listData)
                    {
                        //Check Cache
                        var info = HelperCourse.GetBaseInfo(item.Id);

                        if (info != null)
                        {             
                            model.SearchResults.Add(info);
                        }
                    }
                    model.TotalCount = listData[0].TotalCount;
                    model.CurrentPage = currentPage;
                    model.PageSize = pageSize;
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get data because: " + ex.ToString());

                return View(model);
            }

            return View(model);
        }

        /// <summary>
        /// Display form create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var model = new CourseUpdateModel();

            model.Status = (int)EnumStatus.Activated;

            return View(model);
        }

        /// <summary>
        /// Create Course
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AccessRoleChecker]
        public ActionResult Create(CourseUpdateModel model)
        {
            try
            {
                var newId= 0;

                //Mapping 
                var info = ExtractUpdateData(model);

                //Insert Course
                newId = _mainStore.Insert(info);

                //Clear cache 
                HelperCourse.ClearCache(0);

                if (newId > 0)
                {
                    this.AddNotification("コース追加完了！", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Course request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display form Edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                //Check Cache
                var infoCourse = HelperCourse.GetBaseInfo(id);

                //Mapping 
                var model = ParseUpdateDataForm(infoCourse);

                if (infoCourse == null)
                {
                    return RedirectToErrorPage();
                }

                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Course request: " + ex.ToString());
            }

            return View(new CourseUpdateModel());
        }

        /// <summary>
        /// Edit Course
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(CourseUpdateModel model)
        {
            try
            {
                //Mapping
                var info = ExtractUpdateData(model);

                //Update Course
                var isSuccess = _mainStore.Update(info);

                //Clear cache
                HelperCourse.ClearCache(model.Id);

                if (isSuccess)
                {
                    this.AddNotification("更新完了！", NotificationType.SUCCESS);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Course request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display PopupDelete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            return PartialView("_PopupDelete", id);
        }

        /// <summary>
        /// Delete Course
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            var strError = string.Empty;
            try
            {   //delete Course
                _mainStore.Delete(id);

                //Clear cache
                HelperCourse.ClearCache(id);

            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get Delete Currency because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = "削除しました。", title = "完了！", clientcallback = "location.reload();" });
        }

        /// <summary>
        /// Get CourseSub
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetSub(int id)
        {
            var model = new ManageCourseModel();
            try
            {
                model.CourseSubs = new List<IdentityCourseSub>();

                var infoCourse = HelperCourse.GetBaseInfo(id);

                if(infoCourse != null && infoCourse.CourseSubs.Count > 0)
                {
                    foreach (var item in infoCourse.CourseSubs)
                    {
                        var info = HelperCourseSub.GetBaseInfo(item.Id);

                        model.CourseSubs.Add(info);
                    }
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get data because: " + ex.ToString());

                return View(model);
            }

            return PartialView("Sub", model);
        }


        /// <summary>
        /// Display form create courseSub
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CreateSub(int id)
        {
            var model = new CourseUpdateModel();

            model.CourseId  = id;
            model.Status    = (int)EnumStatus.Activated;

            return View(model);
        }

        /// <summary>
        /// Create CourseSub
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateSub(CourseUpdateModel model)
        {
            var returnModel = new ManageCourseModel();
            try
            {
                var newId = 0;

                //Mapping 
                var info = ExtractCourseSubUpdateData(model);

                //Insert Course
                newId = _mainStoreCourseSub.Insert(info);

                //Clear Cache
                HelperCourse.ClearCache(model.CourseId);

                if (newId > 0)
                {
                    returnModel.Id = model.Id;
                    this.AddNotification("サブコース追加完了！", NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Course request: " + ex.ToString());

                return View(model);
            }           

            return RedirectToAction("Index", "Course", returnModel);
        }

        /// <summary>
        /// Display form Edit CourseSub
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditSub(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var model = new CourseUpdateModel();

                //Check Cache
                var info = HelperCourseSub.GetBaseInfo(id);

                //Mapping 
                model = info.MappingObject<CourseUpdateModel>();

                if (info == null)
                {
                    return RedirectToErrorPage();
                }

                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Course request: " + ex.ToString());
            }

            return View(new CourseUpdateModel());
        }

        /// <summary>
        /// Edit Course
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditSub(CourseUpdateModel model)
        {
            var returnModel = new ManageCourseModel();
            try
            {
                //Mapping
                var info = ExtractCourseSubUpdateData(model);

                //Update Course
                var isSuccess = _mainStoreCourseSub.Update(info);

                //Clear cache
                HelperCourseSub.ClearCache(model.Id);

                if (isSuccess)
                {
                    returnModel.Id = info.CourseId;
                    this.AddNotification("更新完了！", NotificationType.SUCCESS);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Course request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index", "Course", returnModel);
        }

        /// <summary>
        /// Display PopupDelete CourseSub
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteSub(int id)
        {
            return PartialView("_PopupDelete", id);
        }

        /// <summary>
        /// Delete CourseSub
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("DeleteSub")]
        public ActionResult DeleteSubConfirm(int id)
        {
            var strError = string.Empty;
            try
            {
                var info = HelperCourseSub.GetBaseInfo(id);

                //Clear cache Course
                HelperCourse.ClearCache(info.CourseId);

                //delete CourseSub
                _mainStoreCourseSub.Delete(id);

                //Clear cache CourseSub
                HelperCourseSub.ClearCache(id);
            
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get Delete Currency because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = "削除しました。", title = "完了！", clientcallback = "location.reload();" });
        }
        #region Helpers

        private CourseUpdateModel ParseUpdateDataForm(IdentityCourse data)
        {
            var model = data.MappingObject<CourseUpdateModel>();
            return model;
        }

        private IdentityCourse ExtractUpdateData(CourseUpdateModel model)
        {
            var info = model.MappingObject<IdentityCourse>();
            info.Name = info.Name.ToStringNormally();
            info.Description = info.Description.ToStringNormally();

            return info;
        }
        private IdentityCourseSub ExtractCourseSubUpdateData(CourseUpdateModel model)
        {
            var info = model.MappingObject<IdentityCourseSub>();
            info.Name = info.Name.ToStringNormally();
            info.Description = info.Description.ToStringNormally();

            return info;
        }
        #endregion
    }
}