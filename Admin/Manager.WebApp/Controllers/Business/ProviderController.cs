﻿using Manager.DataLayer.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Settings;
using Manager.WebApp.Models;
using Manager.SharedLibs;
using Manager.DataLayer.Entities;
using Manager.WebApp.Helpers;

namespace Manager.WebApp.Controllers.Business
{
    public class ProviderController : BaseAuthedController
    {
        private readonly IStoreProvider _mainStore;
        private readonly ILog logger = LogProvider.For<ProviderController>();

        public ProviderController(IStoreProvider mainStore)
        {
            _mainStore = mainStore;
        }

        [AccessRoleChecker]
        // GET: Provider
        public ActionResult Index(ManageProviderModel model)
        {
            int currentPage = 1;
            int pageSize = SystemSettings.DefaultPageSize;

            /*if (string.IsNullOrEmpty(model.SearchExec))
            {
                model.SearchExec = "Y";
                if (!ModelState.IsValid)
                {
                    ModelState.Clear();
                }
            }

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }*/

            var filter = new IdentityProvider
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status


            };

            try
            {
                model.SearchResults = _mainStore.GetByPage(filter, currentPage, SystemSettings.DefaultPageSize);
                if (model.SearchResults != null && model.SearchResults.Count > 0)
                {
                    model.TotalCount = model.SearchResults[0].TotalCount;
                    model.CurrentPage = currentPage;
                    model.PageSize = pageSize;
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get data because: " + ex.ToString());

                return View(model);
            }

            return View(model);
        }
        //[AccessRoleChecker]
        public ActionResult Create()
        {
            var createModel = new ProviderUpdateModel();
            createModel.Status = (int)EnumStatus.Activated;
            return View(createModel);
        }

        [HttpPost]
        //[AccessRoleChecker]
        public ActionResult Create(ProviderUpdateModel model)
        {
            var newId = 0;
            if (!ModelState.IsValid)
            {
                string messages = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage + x.Exception));
                this.AddNotification(messages, NotificationType.ERROR);
                return View(model);
            }

            try
            {
                //Extract info
                var info = ExtractUpdateData(model);

                newId = _mainStore.Insert(info);

                //Clear cache
                HelperCurrency.ClearCache();
                if (newId > 0)
                {
                    this.AddNotification(Resources.ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Currency request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        #region Helpers

        private ProviderUpdateModel ParseUpdateDataForm(IdentityProvider data)
        {
            var info = data.MappingObject<ProviderUpdateModel>();
            return info;
        }

        private IdentityProvider ExtractUpdateData(ProviderUpdateModel model)
        {
            var info = model.MappingObject<IdentityProvider>();
            info.Name = info.Name.ToStringNormally();
            info.Code = info.Code.ToStringNormally();
            return info;
        }

        #endregion

    }
}