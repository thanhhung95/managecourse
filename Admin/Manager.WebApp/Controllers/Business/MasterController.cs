﻿using System.Web.Mvc;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Resources;
using System.Globalization;
using System.Threading;

namespace Manager.WebApp.Controllers
{
    public class MasterController : BaseAuthedController
    {
        private readonly ILog logger = LogProvider.For<MasterController>();

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PreventCrossOrigin]
        [PreventSpam(DelayRequest = 1)]
        public JsonResult GetResources()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(_currentLanguage);
            var resources = ResourceSerialiser.ToJson(typeof(ManagerResource), _currentLanguage);

            return Json(resources, JsonRequestBehavior.AllowGet);
        }
    }
}
