﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Helpers;
using Manager.WebApp.Models;
using Manager.WebApp.Resources;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Manager.WebApp.Controllers.Business
{
    public class ClassController : BaseAuthedController
    {
        private readonly IStoreClass _mainStore;
        private readonly ILog logger = LogProvider.For<ClassController>();

        public ClassController(IStoreClass mainStore)
        {
            _mainStore = mainStore;
        }

        /// <summary>
        /// Display list Class by paging
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AccessRoleChecker]
        public ActionResult Index(ManageClassModel model)
        {
            int currentPage = 1;
            int pageSize = (model.PageSize != 0 ? model.PageSize : SystemSettings.DefaultPageSize);

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityClass
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var listData = _mainStore.GetByPage(filter, currentPage, pageSize);

                if (listData.HasData())
                {
                    model.SearchResults = new List<IdentityClass>();

                    foreach (var item in listData)
                    {
                        //Check cache
                        var info = HelperClass.GetBaseInfo(item.Id);

                        if (info != null && info.Id > 0 )
                        {
                            info.Course = HelperCourse.GetBaseInfo(info.CourseId);
                            info.CourseSub = HelperCourseSub.GetBaseInfo(info.CourseSubId);

                            model.SearchResults.Add(info);
                        }
                    }

                    model.TotalCount = listData[0].TotalCount;
                    model.CurrentPage = currentPage;
                    model.PageSize = pageSize;
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get list class by paging: " + ex.ToString());

                return View(model);
            }

            return View(model);
        }

        /// <summary>
        /// Display form create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var model = new ClassUpdateModel();

            model.Status = (int)EnumStatus.Activated;

            return View(model);
        }

        /// <summary>
        /// Create Class
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ClassUpdateModel model)
        {
            var newId = 0;
            try
            {
                var _storeClassTeacher = GlobalContainer.IocContainer.Resolve<IStoreClassTeacher>();
                var _storeClassSchedule = GlobalContainer.IocContainer.Resolve<IStoreClassSchedule>();

                model.TeacherDuplicate = new List<int>();
                List<IdentityClassSchedule> listData = new List<IdentityClassSchedule>();

                var isValid = true;

                if (model.ListWeekdays == null)
                {
                    this.AddNotification("Ngay khong duoc de trong", NotificationType.ERROR);
                    return View(model);
                }

                if (model.ClassTeachers.Count > 0 && model.OpenTime.HasValue && model.CloseTime.HasValue)
                {
                    DateTime startDate = model.StartDate.HasValue ? (DateTime)model.StartDate : new DateTime();
                    DateTime endDate = model.EndDate.HasValue ? (DateTime)model.EndDate : new DateTime();

                    List<DateTime> dates = GetDatesBetweenNew(startDate, endDate, model.ListWeekdays);

                    if (dates.Count > 0 && model.ClassTeachers.Count > 0)
                    {
                        foreach (DateTime date in dates)
                        {
                            foreach (var mdClassTeacher in model.ClassTeachers)
                            {
                                if (date >= mdClassTeacher.StartTeachingDate && date <= mdClassTeacher.EndTeachingDate)
                                {
                                    var idenSchedule = new IdentityClassSchedule();

                                    idenSchedule.TeacherId = mdClassTeacher.TeacherId;
                                    idenSchedule.StartTime = date.Add(model.OpenTime.Value);
                                    idenSchedule.EndTime = date.Add(model.CloseTime.Value);

                                    // Check Duplicate 
                                    var resultDuplicate = _storeClassSchedule.DuplicateTeaching(idenSchedule);

                                    if (resultDuplicate != null && resultDuplicate.Id > 0)
                                    {
                                        model.TeacherDuplicate.Add(resultDuplicate.TeacherId);

                                        isValid = false;
                                    }

                                    listData.Add(idenSchedule);
                                }
                            }
                        }
                    }
                }

                if (isValid)
                {
                    //Extract data
                    var info = ExtractUpdateData(model);

                    string weekDays = string.Empty;

                    if (model.ListWeekdays != null && model.ListWeekdays.Count > 0)
                    {
                        //convert list to string
                        weekDays = string.Join(",", model.ListWeekdays);

                        info.Weekdays = weekDays;
                    }

                    //Insert Class
                    newId = _mainStore.Insert(info);

                    //Clear cache
                    HelperClass.ClearCache(model.Id);
                }
                if (newId > 0 )
                {
                    if (model.ClassTeachers != null && model.ClassTeachers.Count > 0)
                    {
                        foreach (var item in model.ClassTeachers)
                        {
                            //Insert Teacher
                            item.ClassId = newId;
                            _storeClassTeacher.Insert(item);
                        }
                    }

                    if (listData != null && listData.Count > 0)
                    {
                        foreach (var item in listData)
                        {
                            item.ClassId = newId;
                            //Insert Schedule 
                            _storeClassSchedule.Insert(item);
                        }
                    }

                    this.AddNotification(ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification("trung lich giang day", NotificationType.ERROR);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Class request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display Form Edit Class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var model = new ClassUpdateModel();
                
                // Check cache
                var info = HelperClass.GetBaseInfo(id);

                if (info == null)
                    return RedirectToErrorPage();

                if(info != null && info.Id > 0)
                {
                    foreach (var classTeacher in info.ClassTeachers)
                    {
                        var infoClassTeacher = HelperClassTeacher.GetBaseInfo(classTeacher.Id);
                        classTeacher.ClassId = infoClassTeacher.ClassId;
                        classTeacher.TeacherId = infoClassTeacher.TeacherId;
                        classTeacher.StartTeachingDate = infoClassTeacher.StartTeachingDate;
                        classTeacher.EndTeachingDate = infoClassTeacher.EndTeachingDate;
                        classTeacher.CodeColor = infoClassTeacher.CodeColor;

                        if (infoClassTeacher != null && infoClassTeacher.Id > 0)
                        {
                            var infoTeacher = HelperTeacher.GetBaseInfo(classTeacher.TeacherId);
                            if (infoTeacher != null && infoTeacher.Id > 0)
                            {
                                classTeacher.Teacher = infoTeacher;
                            }
                        }
                    }
                }

                //Render to view model
                model = ParseUpdateDataForm(info);

                if (info.Weekdays != null && info.Weekdays.Length > 0)
                {

                    List<string> result = info.Weekdays.Split(',').ToList();

                    if (result != null)
                    {
                        model.ListWeekdays = result.Select(s => int.Parse(s)).ToList();
                    }
                }

                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get Edit Class because: " + ex.ToString());
            }

            return View(new ClassUpdateModel());
        }

        /// <summary>
        /// Edit Class
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ClassUpdateModel model)
        {
            try
            {
                var _storeClassTeacher = GlobalContainer.IocContainer.Resolve<IStoreClassTeacher>();
                var _storeClassSchedule = GlobalContainer.IocContainer.Resolve<IStoreClassSchedule>();
                model.TeacherDuplicate = new List<int>();
                List<IdentityClassSchedule> listData = new List<IdentityClassSchedule>();

                var isValid = true;

                if (model.ListWeekdays == null)
                {
                    this.AddNotification("Ngay khong duoc de trong", NotificationType.ERROR);
                    return View(model);
                }

                if (model.ClassTeachers.Count > 0 && model.OpenTime.HasValue && model.CloseTime.HasValue)
                {
                    DateTime startDate = model.StartDate.HasValue ? (DateTime)model.StartDate : new DateTime();
                    DateTime endDate = model.EndDate.HasValue ? (DateTime)model.EndDate : new DateTime();

                    List<DateTime> dates = GetDatesBetweenNew(startDate, endDate, model.ListWeekdays);

                    if (dates.Count > 0 && model.ClassTeachers.Count > 0)
                    {
                        foreach (DateTime date in dates)
                        {
                            foreach (var mdClassTeacher in model.ClassTeachers)
                            {
                                if (date >= mdClassTeacher.StartTeachingDate && date <= mdClassTeacher.EndTeachingDate)
                                {
                                    var idenSchedule = new IdentityClassSchedule();

                                    idenSchedule.ClassId = model.Id;
                                    idenSchedule.TeacherId = mdClassTeacher.TeacherId;
                                    idenSchedule.StartTime = date.Add(model.OpenTime.Value);
                                    idenSchedule.EndTime = date.Add(model.CloseTime.Value);

                                    // Check Duplicate 
                                    var resultDuplicate = _storeClassSchedule.DuplicateTeaching(idenSchedule);

                                    if (resultDuplicate != null && resultDuplicate.Id > 0)
                                    {
                                        model.TeacherDuplicate.Add(resultDuplicate.TeacherId);

                                        isValid = false;
                                    }

                                    listData.Add(idenSchedule);
                                }
                            }
                        }
                    }
                }


                if (isValid)
                {
                    //Extract data
                    var info = ExtractUpdateData(model);

                    string weekDays = string.Empty;

                    if (model.ListWeekdays != null && model.ListWeekdays.Count > 0)
                    {
                        weekDays = string.Join(",", model.ListWeekdays);

                        info.Weekdays = weekDays;
                    }

                    // Update Class
                    var isSuccess = _mainStore.Update(info);

                    if (model.ClassTeachers != null && model.ClassTeachers.Count > 0)
                    {
                        foreach (var item in model.ClassTeachers)
                        {
                            if (item.Id > 0)
                            {
                                //Update Teacher
                                _storeClassTeacher.Update(item);
                                HelperClassTeacher.ClearCache(item.Id);
                            }
                            if (item.Id == 0)
                            {
                                //Insert Teacher
                                item.ClassId = model.Id;
                                _storeClassTeacher.Insert(item);
                            }
                        }
                    }

                    if (listData != null && listData.Count > 0)
                    {
                        // Delete Schedule by Class
                        _storeClassSchedule.DeleteByClass(model.Id);

                        foreach (var item in listData)
                        {
                            //Insert Schedule 
                            _storeClassSchedule.Insert(item);
                        }
                    }
                    
                    //Clear cache
                    HelperClass.ClearCache(model.Id);

                    if (isSuccess)
                    {
                        this.AddNotification(ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);
                    }
                }
                else
                {
                    this.AddNotification("trung lich giang day", NotificationType.ERROR);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Class request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Detail", "Class", new ClassDetail() { Id = model.Id});
        }

        /// <summary>
        /// Display Popup Delete Class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return PartialView("_PopupDelete", id);
        }

        /// <summary>
        /// Delete Class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            var strError = string.Empty;
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                // Delete Class
                _mainStore.Delete(id);

                //Clear cache
                HelperClass.ClearCache(id);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed for Delete Class request: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = ManagerResource.LB_DELETE_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback = "location.reload();" });
        }

        /// <summary>
        /// Get List CourseSub by CourseId 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult GetSubByCourse(int id)
        {
            var strError = string.Empty;
            var ListCourseSub = new List<IdentityCourseSub>();

            if (id == 0)
            {
                return Json(new { success = false, message = strError });
            }
            try
            {
                //check Cache
                var infoCourse = HelperCourse.GetBaseInfo(id);

                if (infoCourse != null)
                {
                    foreach (var record in infoCourse.CourseSubs)
                    {
                        var infoSub = new IdentityCourseSub();

                        //check Cache
                        infoSub = HelperCourseSub.GetBaseInfo(record.Id);

                        if (infoSub != null)
                        {
                            ListCourseSub.Add(infoSub);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get CourseSub by CourseId because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, data = ListCourseSub }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Display Detail Class
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Detail(ClassDetail model)
        {
            try
            {
                var infoClass = HelperClass.GetBaseInfo(model.Id);

                if (infoClass != null)
                {
                    model.Class = infoClass;

                    var infoCourse = HelperCourse.GetBaseInfo(infoClass.CourseId);

                    if (infoCourse != null && infoCourse.Id > 0)
                    {
                        model.Course = infoCourse;
                    }

                    var infoCourseSub = HelperCourseSub.GetBaseInfo(infoClass.CourseSubId);

                    if (infoCourseSub != null && infoCourseSub.Id > 0)
                    {
                        model.CourseSub = infoCourseSub;
                    }

                    if (infoClass.ClassTeachers.HasData() && infoClass.ClassTeachers.Count > 0)
                    {
                        model.ClassTeachers = new List<IdentityClassTeacher>();

                        foreach ( var classTeacher in infoClass.ClassTeachers)
                        {
                            var infoClassTeacher = HelperClassTeacher.GetBaseInfo(classTeacher.Id);
                            classTeacher.ClassId = infoClassTeacher.ClassId;
                            classTeacher.TeacherId = infoClassTeacher.TeacherId;
                            classTeacher.StartTeachingDate = infoClassTeacher.StartTeachingDate;
                            classTeacher.EndTeachingDate = infoClassTeacher.EndTeachingDate;
                            classTeacher.CodeColor = infoClassTeacher.CodeColor;

                            if (infoClassTeacher != null && infoClassTeacher.Id > 0)
                            {
                                var infoTeacher = HelperTeacher.GetBaseInfo(classTeacher.TeacherId);

                                if (infoTeacher != null && infoTeacher.Id > 0)
                                {
                                    classTeacher.Teacher = infoTeacher;
                                }
                            }

                            model.ClassTeachers.Add(classTeacher);
                        }
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed get Detail because: " + ex.ToString());
            }

            return View(new ClassDetail());
        }

        /// <summary>
        /// Get Class Student by paging
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult GetClassStudentsByPaging(ClassStudentUpdateModel model)
        {
            var _storeStudent = GlobalContainer.IocContainer.Resolve<IStoreStudent>();
            int currentPage = 1;
            int pageSize = (model.PageSize != 0 ? model.PageSize : 10);

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityStudent
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var InfoClass = HelperClass.GetBaseInfo(model.ClassId);

                if (InfoClass != null)
                {
                    model.ClassId = InfoClass.Id;

                    var listStudentId = _storeStudent.GetListStudentInClass(filter, currentPage, pageSize, model.ClassId);

                    if (listStudentId.HasData())
                    {
                        model.SearchResults = new List<IdentityStudent>();

                        foreach (var item in listStudentId)
                        {
                            //check Cache
                            var r = HelperStudent.GetBaseInfo(item.Id);
                            if (r != null)
                            {
                                model.SearchResults.Add(r);
                            }
                        }
                        model.TotalCount = listStudentId[0].TotalCount;
                        model.CurrentPage = currentPage;
                        model.PageSize = pageSize;
                    }
                }

                return PartialView("_ClassStudents", model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for get list student not in class request: " + ex.ToString());
            }

            return View(new ClassStudentUpdateModel());
        }

        /// <summary>
        /// Display popup get list student by paging
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult GetListStudentsByPage(ClassStudentUpdateModel model)
        {
            var _storeStudent = GlobalContainer.IocContainer.Resolve<IStoreStudent>();

            int currentPage = 1;
            int pageSize = (model.PageSize != 0 ? model.PageSize : 10);

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityStudent
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var InfoClass = HelperClass.GetBaseInfo(model.ClassId);

                if (InfoClass != null)
                {
                    model.ClassId = InfoClass.Id;

                    var listStudentId = _storeStudent.GetListStudentNotInClass( filter, currentPage, pageSize, model.ClassId);

                    if (listStudentId.HasData())
                    {
                        model.SearchResults = new List<IdentityStudent>();

                        foreach (var item in listStudentId)
                        {
                            //check Cache
                            var r = HelperStudent.GetBaseInfo(item.Id);
                            if (r != null)
                            {
                                model.SearchResults.Add(r);
                            }
                        }
                        model.TotalCount = listStudentId[0].TotalCount;
                        model.CurrentPage = currentPage;
                        model.PageSize = pageSize;
                    }
                }

                if (model.IsPage == true)
                {
                    return PartialView("_ListStudentsContent", model);
                }

                return PartialView("_PopupListStudents", model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for get list student not in class request: " + ex.ToString());
            }

            return View(new ClassStudentUpdateModel());
        }

        /// <summary>
        /// Insert Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertStudent(ClassStudentUpdateModel model)
        {
            var strError = string.Empty;
            var newId = 0; 
            try
            {
                var _storeClassStudent = GlobalContainer.IocContainer.Resolve<IStoreClassStudent>();

                if (!string.IsNullOrEmpty(model.ListStudentId))
                {
                     newId = _storeClassStudent.InsertList(model.ListStudentId, model.ClassId);
                }

                //Clear Cache 
                HelperClass.ClearCache(model.ClassId);

                if (newId > 0 )
                {
                    
                    return Json(new { success = true, message = ManagerResource.LB_INSERT_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback = "GetClassStudentsByAjax();" });
                }
                else
                {
                    return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION, clientcallback = "GetClassStudentsByAjax();" });
                }
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed for insert Student because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }

        /// <summary>
        /// Display Popup delete Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteStudent(ClassStudentUpdateModel model)
        {
            return PartialView("_PopupDeleteStudent", model);
        }

        /// <summary>
        /// Delete Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ActionName("DeleteStudent")]
        public ActionResult DeleteStudentConfirm(ClassStudentUpdateModel model)
        {
            var strError = string.Empty;
            try
            {
                var _storeClassStudent = GlobalContainer.IocContainer.Resolve<IStoreClassStudent>();

                if (model.ListStudentId != null && model.ClassId > 0)
                {
                    //delete student in class
                    _storeClassStudent.DeleteList(model.ListStudentId, model.ClassId);

                    //clear Cache
                    HelperClass.ClearCache(model.ClassId);
                    return Json(new { success = true, message = ManagerResource.LB_DELETE_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback = "GetClassStudentsByAjax();" });
                }
                else
                {
                    return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION });
                }
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get Delete Currency because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }

        /// <summary>
        /// Edit Day in class schedule
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditClassDaySchedule(ClassScheduleUpdateModel model)
        {
            var strError = string.Empty;
            bool isSuccess = true;
            try
            {
                if (model.ClassId > 0  && model.TeacherId > 0 && model.StartDate.HasValue && model.EndDate.HasValue && model.OpenTime.HasValue && model.CloseTime.HasValue)
                {
                    var _storeClassSchedule = GlobalContainer.IocContainer.Resolve<IStoreClassSchedule>();
                    var info = new IdentityClassSchedule();

                    info.Id = model.Id;
                    info.TeacherId = model.TeacherId;
                    info.ClassId = model.ClassId;

                    DateTime StartDate = model.StartDate.HasValue ? (DateTime)model.StartDate : new DateTime();
                    DateTime EndDate = model.StartDate.HasValue ? (DateTime)model.EndDate : new DateTime();

                    info.StartTime = StartDate.Add(model.OpenTime.Value);
                    info.EndTime = EndDate.Add(model.CloseTime.Value);

                    if(model.Comment != null && model.Comment.Length > 0)
                    {
                        info.Comment = model.Comment.ToStringNormally();
                    }

                    if (model.Note != null && model.Note.Length > 0)
                    {
                        info.Note = model.Note.ToStringNormally();
                    }

                    isSuccess = _storeClassSchedule.Update(info);

                    if (model.ListAttendances.HasData())
                    {
                        var _storeAttendance = GlobalContainer.IocContainer.Resolve<IStoreAttendance>();

                        foreach (var attendance in model.ListAttendances)
                        {
                            attendance.Comment = attendance.Comment.ToStringNormally();
                            attendance.Note = attendance.Note.ToStringNormally();

                            _storeAttendance.Update(attendance);
                        }
                    }

                    //Clear cache
                    HelperClassSchedule.ClearCache(model.Id);

                    if (isSuccess)
                    {
                        return Json(new { success = true, message = ManagerResource.LB_UPDATE_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback = "RefreshEvents();" });
                    }
                    else
                    {
                        return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION, clientcallback = "RefreshEvents();" });
                    }
                }
                return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION, clientcallback = "RefreshEvents();" });
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed Edit class day schedule because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }

        /// <summary>
        /// Get ClassSchedules 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetClassSchedules(int id)
        {
            var strError = string.Empty;
            JsonResult result = new JsonResult();

            try
            {
                var listClassSchedule = new List<ClassScheduleUpdateModel>();

                var infoClass = HelperClass.GetBaseInfo(id);

                if (infoClass != null)
                {
                    foreach (var item in infoClass.ClassSchedule)
                    {
                        
                        var classSchedule = new ClassScheduleUpdateModel();

                        var infoSchedule = HelperClassSchedule.GetBaseInfo(item.Id);
                       
                        if(infoSchedule != null && infoSchedule.TeacherId > 0)
                        {
                            var infoTeacher = HelperTeacher.GetBaseInfo(infoSchedule.TeacherId);

                            classSchedule.Teacher = infoTeacher;
                        }
                        else
                        {
                            classSchedule.Teacher = new IdentityTeacher();
                        }
                        classSchedule.Id = infoSchedule.Id;
                        classSchedule.ClassId = infoSchedule.ClassId;

                        classSchedule.StartTimeStr = infoSchedule.StartTime.HasValue ? EpochTime.GetIntDate(infoSchedule.StartTime.Value).ToString() : "";
                        classSchedule.EndTimeStr = infoSchedule.EndTime.HasValue ? EpochTime.GetIntDate(infoSchedule.EndTime.Value).ToString() : "";

                        listClassSchedule.Add(classSchedule);
                    }
                }

                // Processing.  
                result = this.Json(listClassSchedule, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get Class Schedule because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            // Return info.  
            return result;
        }


        /// <summary>
        /// create Day to Schedule
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ActionName("CreateDay")]
        public ActionResult CreateDayConfirm(ClassScheduleUpdateModel model)
        {
            var strError = string.Empty;
            try
            {
                var isValid = new List<bool>();
                int teacherSelect = 0;
                var _storeClassSchedule = GlobalContainer.IocContainer.Resolve<IStoreClassSchedule>();

                if (model.ClassId <= 0 || !model.DateSelect.HasValue)
                {
                    return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION, clientcallback = "RefreshEvents();" });
                }

                var infoClass = HelperClass.GetBaseInfo(model.ClassId);

                if (infoClass != null && infoClass.Id > 0)
                {
                    foreach(var classTeacher in infoClass.ClassTeachers)
                    {
                         var infoClassTeacher = HelperClassTeacher.GetBaseInfo(classTeacher.Id);
                         if(infoClassTeacher != null && infoClassTeacher.Id > 0)
                        {
                            if (model.DateSelect < infoClassTeacher.StartTeachingDate || model.DateSelect > infoClassTeacher.EndTeachingDate)
                            {
                                isValid.Add(false);
                            }
                            else
                            {
                                isValid.Add(true);
                                teacherSelect = infoClassTeacher.TeacherId;
                            }
                        }
                    }
                    if (isValid.Contains(true) && teacherSelect > 0)
                    {
                        var dataInsert = new IdentityClassSchedule();

                        dataInsert.ClassId = model.ClassId;
                        dataInsert.TeacherId = teacherSelect;
                        dataInsert.StartTime = model.DateSelect.Value.Add(infoClass.OpenTime.Value);
                        dataInsert.EndTime = model.DateSelect.Value.Add(infoClass.CloseTime.Value);

                        var newId = _storeClassSchedule.Insert(dataInsert);
                        //Clear Cache
                        HelperClass.ClearCache(model.ClassId);

                        if (newId > 0)
                        {
                            return Json(new { success = true, message = ManagerResource.LB_INSERT_SUCCESS, title = ManagerResource.LB_NOTIFICATION });
                        }
                        else
                        {
                            return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION });
                        }
                    }
                    else
                    {
                        return Json(new { success = false, message = "Ngày không hợp lệ", title = ManagerResource.LB_NOTIFICATION });
                    }
                }
                else
                {
                    return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION });
                }
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to create Day because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }

        /// <summary>
        /// Display popup Edit day in schedule
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult EditDay(ClassScheduleUpdateModel model)
        {
            var strError = string.Empty;

            var filter = new IdentityStudent
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var info = HelperClassSchedule.GetBaseInfo(model.Id);
                var _storeStudent = GlobalContainer.IocContainer.Resolve<IStoreStudent>();

                if(info != null && info.Id > 0)
                {
                    model = info.MappingObject<ClassScheduleUpdateModel>();
                }

                if (info.TeacherId > 0)
                {
                    model.TeacherId = info.TeacherId;
                }

                var infoClass = HelperClass.GetBaseInfo(info.ClassId);

                if (infoClass != null && infoClass.Id > 0)
                {
                    model.Name = infoClass.Name;
                }

                var listStudentId = _storeStudent.GetListStudentByClassSchedule(filter, 1, 1000, info.ClassId, info.Id);

                if (listStudentId.HasData())
                {
                    model.SearchResults = listStudentId;
                    model.TotalCount = listStudentId[0].TotalCount;
                }

                return PartialView("_PopupEditDay", model);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed to get Edit Day because: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }

        /// <summary>
        /// Display Popup Delete Day
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteDay(int id)
        {
            if (id <= 0)
            {
                return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION });
            }
            return PartialView("_PopupDeleteDay", id);
        }

        /// <summary>
        /// Delete Class
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("DeleteDay")]
        public ActionResult DeleteDayConfirm(int id)
        {
            var strError = string.Empty;
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var _storeClassSchedule = GlobalContainer.IocContainer.Resolve<IStoreClassSchedule>();

                //Delete ClassTeacher
                _storeClassSchedule.Delete(id);

                //Clear cache
                HelperClassSchedule.ClearCache(id);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed for Delete Class request: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = ManagerResource.LB_DELETE_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback = "RefreshEvents();" });
        }


        #region Helpers

        public List<DateTime> GetDatesBetweenNew(DateTime start, DateTime end, List<int> weekdays)
        {
            bool allDays = weekdays == null || !weekdays.Any();

            var dates = Enumerable.Range(0, 1 + end.Subtract(start).Days)
                                  .Select(offset => start.AddDays(offset))
                                  .Where(d => allDays || weekdays.Contains((int)(DayOfWeek)d.DayOfWeek))
                                  .ToList();
            return dates;
        }

        private ClassUpdateModel ParseUpdateDataForm(IdentityClass data)
        {
            var model = data.MappingObject<ClassUpdateModel>();
            return model;
        }

        private IdentityClass ExtractUpdateData(ClassUpdateModel model)
        {
            var info = model.MappingObject<IdentityClass>();
            info.Name = info.Name.ToStringNormally();
            return info;
        }

        #endregion
    }
}