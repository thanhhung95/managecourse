﻿using Autofac;
using Manager.DataLayer.Entities;
using Manager.DataLayer.Stores;
using Manager.SharedLibs;
using Manager.SharedLibs.Logging;
using Manager.WebApp.Helpers;
using Manager.WebApp.Models;
using Manager.WebApp.Resources;
using Manager.WebApp.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Manager.WebApp.Controllers.Business
{
    public class StudentController : BaseAuthedController
    {
        private readonly IStoreStudent _mainStore;
        private readonly ILog logger = LogProvider.For<StudentController>();

        public StudentController(IStoreStudent mainStore)
        {
            _mainStore = mainStore;
        }

        /// <summary>
        /// Display list student by paging
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AccessRoleChecker]
        public ActionResult Index(ManageStudentModel model)
        {
            int currentPage = 1;
            int pageSize = (model.PageSize != 0 ? model.PageSize : SystemSettings.DefaultPageSize);

            if (Request["Page"] != null)
            {
                currentPage = Utils.ConvertToInt32(Request["Page"], 1);
            }

            var filter = new IdentityStudent
            {
                Keyword = !string.IsNullOrEmpty(model.Keyword) ? model.Keyword.ToStringNormally() : null,
                Status = model.Status == null ? -1 : (int)model.Status
            };

            try
            {
                var listData = _mainStore.GetByPage(filter, currentPage, pageSize);

                if (listData.HasData())
                {
                    model.SearchResults = new List<IdentityStudent>();

                    foreach (var item in listData)
                    {
                        //Check cache
                        var info = HelperStudent.GetBaseInfo(item.Id);

                        if (info != null)
                            model.SearchResults.Add(info);
                    }

                    model.TotalCount = listData[0].TotalCount;
                    model.CurrentPage = currentPage;
                    model.PageSize = pageSize;
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed to get data because: " + ex.ToString());

                return View(model);
            }

            return View(model);
        }

        /// <summary>
        /// Display form create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var model = new StudentUpdateModel();
            return View(model);
        }

        /// <summary>
        /// Create Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(StudentUpdateModel model)
        {
            var newId = 0;
            try
            {
                //Extract info
                var info = ExtractUpdateData(model);

                //Insert Student
                newId = _mainStore.Insert(info);
                    
                if (newId > 0)
                {
                    this.AddNotification(ManagerResource.LB_INSERT_SUCCESS, NotificationType.SUCCESS);
                }
                else
                {
                    this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Create Student request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display Form Edit 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                // Check cache
                var info = HelperStudent.GetBaseInfo(id);

                if (info == null)
                    return RedirectToErrorPage();

                //Render to view model
                var model = ParseUpdateDataForm(info);

                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed get Edit Student because: " + ex.ToString());
            }

            return View(new StudentUpdateModel());
        }

        /// <summary>
        /// Update Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(StudentUpdateModel model)
        {
            try
            {
                //Extract data
                var info = ExtractUpdateData(model);

                // update student
                var isSuccess = _mainStore.Update(info);

                //Clear cache
                HelperStudent.ClearCache(model.Id);

                if (isSuccess)
                {
                    this.AddNotification(ManagerResource.LB_UPDATE_SUCCESS, NotificationType.SUCCESS);
                }
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed for Edit Student request: " + ex.ToString());

                return View(model);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Display Details Student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                var _storeClassStudent = GlobalContainer.IocContainer.Resolve<IStoreClassStudent>();

                // Check cache
                var info = HelperStudent.GetBaseInfo(id);

                if (info == null)
                    return RedirectToErrorPage();

                //Render to view model
                var model = ParseUpdateDataForm(info);

                model.ListClass = new List<IdentityClass>();

                var listClass = _storeClassStudent.GetListClassByStudent(id);

                if (listClass.HasData())
                {
                    foreach (var record  in listClass)
                    {
                        var infoClass = HelperClass.GetBaseInfo(record.ClassId);

                        if(infoClass != null && infoClass.Id > 0)
                        {
                            model.ListClass.Add(infoClass);
                        }
                    }
                    model.TotalCount = listClass[0].TotalCount;
                }

                return View(model);
            }
            catch (Exception ex)
            {
                this.AddNotification(NotifSettings.Error_SystemBusy, NotificationType.ERROR);

                logger.Error("Failed get Details student because: " + ex.ToString());
            }

            return View(new StudentUpdateModel());
        }

        /// <summary>
        /// Get Status of Student
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult GetStatusStudent(AttendanceModel model)
        {
            try
            {
                var _storeAttendance = GlobalContainer.IocContainer.Resolve<IStoreAttendance>();
                int status = 0;

                status = model.Status != null ? model.Status.Value : -1;

                if (model.ClassId > 0 && model.StudentId > 0) {

                    var infoClass = HelperClass.GetBaseInfo(model.ClassId);

                    if(infoClass != null && infoClass.Id > 0)
                    {
                        model.Class = infoClass;
                    }

                    var ListAttendance = _storeAttendance.GetListByStudent(model.ClassId, model.StudentId, status);

                    if (ListAttendance.HasData())
                    {
                        model.ListAttendance = ListAttendance;
                        model.TotalCount = ListAttendance[0].TotalCount;
                    }

                    if(model.IsFilter)
                    {
                        return PartialView("_StatusStudentContent", model);
                    }

                    return PartialView("_PopupStatusStudent", model);
                }
                return Json(new { success = false, message = ManagerResource.LB_SYSTEM_BUSY, title = ManagerResource.LB_NOTIFICATION });
            }
            catch (Exception ex)
            {
                var strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed for Get Status of Student request: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }
        }

        /// <summary>
        /// Display Popup Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            return PartialView("_PopupDelete", id);
        }

        /// <summary>
        /// Delete Student
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            var strError = string.Empty;
            try
            {
                // Delete Student
                _mainStore.Delete(id);

                //Clear cache
                HelperStudent.ClearCache(id);
            }
            catch (Exception ex)
            {
                strError = ManagerResource.LB_SYSTEM_BUSY;

                logger.Error("Failed for Delete Student request: " + ex.ToString());

                return Json(new { success = false, message = strError });
            }

            return Json(new { success = true, message = ManagerResource.LB_DELETE_SUCCESS, title = ManagerResource.LB_NOTIFICATION, clientcallback = "location.reload();" });
        }

        #region Helpers

        private StudentUpdateModel ParseUpdateDataForm(IdentityStudent data)
        {
            var model = data.MappingObject<StudentUpdateModel>();
            return model;
        }

        private IdentityStudent ExtractUpdateData(StudentUpdateModel model)
        {
            var info = model.MappingObject<IdentityStudent>();
            info.Name = info.Name.ToStringNormally();
            info.Address = info.Address.ToStringNormally();
            info.Email = info.Email.ToStringNormally();
            return info;
        }

        #endregion
    }
}