﻿using Autofac;
using Manager.DataLayer;
using Manager.DataLayer.Stores;

namespace Manager.WebApp.DependencyInjection
{
    public class ManagerModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //For system
            builder.RegisterType<IdentityStore>().As<IIdentityStore>();
            builder.RegisterType<StoreUser>().As<IStoreUser>();

            //For business
            builder.RegisterType<StoreCurrency>().As<IStoreCurrency>();
            builder.RegisterType<StoreUnit>().As<IStoreUnit>();
            builder.RegisterType<StoreProvider>().As<IStoreProvider>();
            builder.RegisterType<StoreStudent>().As<IStoreStudent>();
            builder.RegisterType<StoreCourse>().As<IStoreCourse>();
            builder.RegisterType<StoreCourseSub>().As<IStoreCourseSub>();
            builder.RegisterType<StoreTeacher>().As<IStoreTeacher>();
            builder.RegisterType<StoreClass>().As<IStoreClass>();
            builder.RegisterType<StoreClassTeacher>().As<IStoreClassTeacher>();
            builder.RegisterType<StoreClassStudent>().As<IStoreClassStudent>();
            builder.RegisterType<StoreClassSchedule>().As<IStoreClassSchedule>();
            builder.RegisterType<StoreDaysOfWeek>().As<IStoreDaysOfWeek>();
            builder.RegisterType<StoreAttendance>().As<IStoreAttendance>();
        }
    }
}